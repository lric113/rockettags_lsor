from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import logout_then_login
from django.conf import settings


# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^t/(.+)$', 'transition.views.qrcode'),
                       url(r'^testtag/$', 'transition.views.test_tag'),
                       url(r'^registertag/(.*)$', 'transition.views.register_tag'),
                       url(r'^foundtag/(.*)$', 'transition.views.found_tag'),
                       url(r'^myrockettags$', 'transition.views.myrockettags'),
                       url(r'^user_logout/$', 'transition.views.user_logout'),
                       url(r'^login$', 'transition.views.login_'),
                       url(r'^$', 'transition.views.home'),
                       url(r'^googlecb48235008046c8e.html/$', 'transition.views.google_seo'),
                       url(r'^reset/$', 'transition.views.reset'),
                       url(r'^check/$', 'transition.views.check'),
                       url(r'^trainingvideo/$', 'transition.views.trainingvideo'),
                       url(r'^promo$', 'transition.views.redirect_to_promo_offer'),
                       url(r'^promo/offer$', 'transition.views.promo'),
                       url(r'^promo-thankyou$', 'transition.views.promo_thankyou'),
                       url(r'^cam01$', 'transition.views.cam01'),
                       url(r'^bulk-test$', 'transition.views.bulk_test'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^corp_promo', 'transition.views.corp_promo'),
                       url(r'^corp_solutions', 'transition.views.corp_solutions'),
                       url(r'^sheetapp', 'sheetapp.views.main'),
                       url(r'^corpmanage', 'sheetapp.views.corpmanage'),
                       url(r'^updatetag', 'sheetapp.views.updatetag'),
                       url(r'^downloadmytags', 'sheetapp.views.downloadmytags'),
                       url(r'^emailmyeditedtags', 'sheetapp.views.emailmyeditedtags'),
                       url(r'^updatecustomfields', 'sheetapp.views.updatecustomfields'),
                       url(r'^renamefields', 'sheetapp.views.renamefields'),
                       url(r'^staffconsole', 'sheetapp.views.staffconsole'),
                       url(r'^createcorporateaccount', 'sheetapp.views.createcorporateaccount'),
                       url(r'^uploadtags', 'sheetapp.views.uploadtags'),
                       url(r'^landing', 'transition.views.facebooklanding'),
                       )

urlpatterns += patterns('',
                        url(r'^captcha/', include('captcha.urls')),
                        )

"""
url(r'^testcaptcha/$', 'transition.views.test_captcha'),
url(r'^reset/$', 'transition.views.reset'),
url(r'^gettags/$', 'transition.views.get_tags'),
url(r'^user/$', 'transition.views.userinfo'),
url(r'^test_menu/$', direct_to_template, {'template': 'transition/test_menu.html'} ), 
"""
