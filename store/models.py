from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from paypal.standard.ipn.signals import payment_was_successful, payment_was_flagged
import uuid
import time
from django.conf import settings
#from rockettags.main.models import create_log_entry

table_codes = { 'ordermaster' : '001',
                'paymentmaster' : '002',
                'subscription' : '003',
              }

def create_backoffice_number(table_name, table_id) :

    now = datetime.now()
    date_string = now.strftime('%d-%m-%Y-%H-%M-%S')
    return '%s-%d-%s' % (table_codes[table_name], table_id, date_string)

                
def create_backoffice_log_entry(category, message, subcategory=None, tablename='BACKOFFICE') :

    if category is None or len(category) == 0 :
        return None

    if subcategory is None :
        subcategory = ''

    if tablename is None :
        tablename = ''

#    new_entry = SiteLog(tablename=tablename, category=category, subcategory=subcategory, message=message)
#    new_entry.save()

#    return new_entry

    message_list = message.rsplit('\n')
    new_message = ':'.join(message_list)

    new_entry = '%s|%s|%s|%s|%s|' % (str(datetime.datetime.now()), tablename, category, subcategory, new_message)
    f = open('/var/log/rockettags/site.log', 'a')
    try :
        f.write('%s\n' % new_entry)
    finally :
        f.close()

    return new_entry

# Create your models here.

class ProductType(models.Model):
    
    type_name = models.CharField(null=False, blank=False, max_length=64)
    category = models.CharField(null=False, blank=False, max_length=64)
    description = models.CharField(null=False, blank=False, max_length=255)

    def __unicode__(self) :
        return u'%s' % self.type_name
    

class Product(models.Model):
    
    product_code = models.CharField(null=True, blank=True, max_length=64)
    product_name = models.CharField(null=True, blank=True, max_length=128)
    product_type = models.ForeignKey(ProductType, null=True, blank=True, related_name='products', on_delete=models.SET_NULL)
    time_to_live = models.PositiveIntegerField(default=90)
    subscription_period = models.PositiveIntegerField(default=90)
    start_date = models.DateTimeField(default=datetime.now())
    expiry_date = models.DateTimeField(null=True, blank=True)
    status = models.PositiveIntegerField(default=0)

    def __unicode__(self) :
        return u'%s' % self.product_code


# !!!! PRICE is no longer used in this system !!!!
class Price(models.Model) :
    product = models.OneToOneField(Product)
    price = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    creation_date = models.DateTimeField(default=datetime.now())
    update_date = models.DateTimeField(null=True, blank=True)

def get_product_details(code) :

    product_details = {}
    product = None
    try :
        product = Product.objects.get(product_code=code)
    except :
        product = None

    if product is None : return None

    product_details['product_code'] = code
    product_details['product_name'] = product.product_name
    product_details['product_type'] = product.product_type.type_name
    product_details['product_start_date'] = product.start_date
    product_details['product_ttl'] = product.time_to_live
    product_details['product_expiry_date'] = product.expiry_date
    product_details['product_subscription_period'] = product.subscription_period

    price = None
    try :
        price = product.productprices.all()[0]
    except :
        price = None
        
    if not price is None :
        product_details['product_price'] = price.price
        product_details['product_price_currency'] = price.currency

    return product_details
    
    


class Stickerbook(models.Model):
    
    pack_id = models.CharField(null=True, blank=True, max_length=128)
    product = models.ForeignKey(Product, null=True, blank=True, related_name='stickerbooks', on_delete=models.SET_NULL)
    preview_dropin = models.CharField(null=True, blank=True, max_length=255)
    finished_dropin = models.CharField(null=True, blank=True, max_length=255)
    description = models.CharField(null=False, blank=False, max_length=255)
    creation_date = models.DateTimeField(default=datetime.now())
    shipped_date = models.DateTimeField(null=True, blank=True)
    received_date = models.DateTimeField(null=True, blank=True)

# !!!! ORDER IS no longer used in this system !!!!
class Order(models.Model) :
    # order code is the caller reference for the Amazon Payment
    order_code = models.CharField(null=False, blank=False, max_length=64)
    user = models.ForeignKey(User, null=True, blank=True, related_name='orders', on_delete=models.SET_NULL)    
    product = models.ForeignKey(Product, null=True, blank=True, related_name='orders', on_delete=models.SET_NULL)
    num_items = models.PositiveIntegerField(default=0)
    total_amount = models.PositiveIntegerField(default=0)
    creation_date = models.DateTimeField(default=datetime.now())
    shipping_date = models.DateTimeField(null=True, blank=True)
    status = models.PositiveIntegerField(default=0)

#class OrderDetails(models.Model):
#    order = models.OneToOneField(Order)
#    tcversion = models.CharField(null=True, blank=True, max_length=8)
#    # other fields

# Add a Subscription model
# This gets created upon a successful AmazonPayment for an Order
# Used when renewing a subscription
# has fields
#
# order linking to the terms and conditions version that applied
# amazon recurring token id
# the length of the subscription
# the creation/starting date
# last renewal date
# expiry date
# extension (by PROMO or COMPLIMENTARY)
# whether active
#
# A customer can have a history of multiple subscriptions but only one active one
#


# !!!! PAYMENT is no longer used in this system !!!!
class Payment(models.Model) :

    payment_code = models.CharField(null=False, blank=False, max_length=64)
    order = models.ForeignKey(Order, null=True, blank=True, related_name='orders', on_delete=models.SET_NULL)    
    creation_date = models.DateTimeField(default=datetime.now())
    completion_date = models.DateTimeField(null=True, blank=True)
    amazon_token = models.CharField(null=False, blank=False, max_length=128)
    status = models.PositiveIntegerField(default=0)
    
# Will need an extra table to hold the Amazon Specific Details from the CBUI
#
# There can be multiple AmazonPayment attempts per order but only one successful one
# which is the most recent successful one
#
#class AmazonPayment(models.Model) :
#
#   order = models.OneToOneField(Order)
#   active = models.PositiveIntegerField(default=0)
#   tokenID = models.CharField(null=False, blank=False, max_length=128)
#   status = models.CharField(null=True, blank=True, max_length=16)
#   certificateUrl = models.CharField(null=True, blank=True, max_length=256)
#   expiry = models.CharField(null=True, blank=True, max_length=32)
    # caller reference is the order code
#   callerReference = models.CharField(null=True, blank=True, max_length=64)
#   signatureVersion = models.CharField(null=True, blank=True, max_length=8)
#   signatureMethod = models.CharField(null=True, blank=True, max_length=32)
#   signature = models.CharField(null=True, blank=True, max_length=256)
#   creation_date = models.DateTimeField(default=datetime.now())
#   payment_date = models.DateTimeField(null=True, blank=True)


class ProductPrice(models.Model) :
    
    product = models.ForeignKey(Product, null=True, blank=True, related_name='productprices', on_delete=models.SET_NULL)

    price_type = models.CharField(max_length=32, null=True, blank=True)
    price_description = models.CharField(max_length=255, null=True, blank=True)
    
    price = models.DecimalField(max_digits=64, decimal_places=2, null=True, blank=True)
    currency = models.CharField(max_length=32, null=True, blank=True)

    creation_date = models.DateTimeField(default=datetime.now())

    extra1 = models.CharField(max_length=128, null=True, blank=True)
    extra2 = models.CharField(max_length=64, null=True, blank=True)
    extra3 = models.CharField(max_length=32, null=True, blank=True)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self) :
        return u'%s' % self.product.product_code


class OrderMaster(models.Model) :

    order_number = models.CharField(max_length=128, null=True, blank=True)
    
    user_id = models.CharField(max_length=128, null=True, blank=True)

    total_amount = models.PositiveIntegerField(default=0)
    total_cost = models.DecimalField(max_digits=64, decimal_places=2, null=True, blank=True)
    
    creation_date = models.DateTimeField(default=datetime.now())
    shipping_date = models.DateTimeField(null=True, blank=True)
    shipping_status = models.PositiveIntegerField(default=0)
    tc_version = models.CharField(max_length=32, null=True, blank=True)

    description = models.CharField(max_length=255, null=True, blank=True)

    extra1 = models.CharField(max_length=128, null=True, blank=True) # used to hold the main product type for this order
    extra2 = models.CharField(max_length=64, null=True, blank=True)
    extra3 = models.CharField(max_length=32, null=True, blank=True)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self) :
        return u'%s' % self.order_number

    @classmethod
    def exists(cls, order_number) :
        try :
            existing_order = cls.objects.get(order_number=order_number)
        except :
            existing_order = None
        if existing_order is not None :
            return True        
        return False

    
class OrderDetails(models.Model) :

    ordermaster = models.ForeignKey(OrderMaster, null=True, blank=True, related_name='orderdetails', on_delete=models.SET_NULL)
    
    product = models.ForeignKey(Product, null=True, blank=True, related_name='orderdetails', on_delete=models.SET_NULL)
    
    amount = models.PositiveIntegerField(default=0)
    cost = models.DecimalField(max_digits=64, decimal_places=2, null=True, blank=True)

    description = models.CharField(max_length=255, null=True, blank=True)
    
    extra1 = models.CharField(max_length=128, null=True, blank=True)
    extra2 = models.CharField(max_length=64, null=True, blank=True)
    extra3 = models.CharField(max_length=32, null=True, blank=True)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self) :
        return u'%s' % self.id
    

def get_order_details(order_number) :

    details = {}

    order = OrderMaster.objects.get(order_number=order_number)
    if order is None :
        return None

    details['order_number'] = order.order_number
    details['total_amount'] = order.total_amount
    details['total_cost'] = order.total_cost
    details['main_product_type'] = order.extra1
    details['tc_version'] = order.tc_version
    details['shipping_status'] = order.shipping_status
    details['shipping_date'] = order.shipping_date
    details['creation_date'] = order.creation_date
    
    return details

def get_num_order_line_items(order_number) :

    details = {}

    order = OrderMaster.objects.get(order_number=order_number)
    if order is None :
        return None

    orderdetails = order.orderdetails
    if orderdetails is None :
        return None

    num_items = 0
    try :
         num_items = len(orderdetails.all())
    except :
         num_items = 0

    return num_items


def get_order_line_item(order_number, line_number) :

    details = {}

    order = OrderMaster.objects.get(order_number=order_number)
    if order is None :
        return None

    orderdetails = order.orderdetails
    if orderdetails is None :
        return None

    try :
        line_item = orderdetails.all()[line_number]
    except :
        line_item = None

    if line_item is None :
        return None

    details['product_code'] = line_item.product.product_code
    details['product_type'] = line_item.product.product_type.type_name
    details['product_name'] = line_item.product.product_name
    details['product_subscription_period'] = line_item.product.subscription_period
    details['product_start_date'] = line_item.product.start_date
    details['product_time_to_live'] = line_item.product.time_to_live

    details['amount'] = line_item.amount
    details['cost'] = line_item.cost

    return details    


def create_order_number(order_id) :
    return create_backoffice_number('ordermaster', order_id)
    
def create_single_product_order(user_id, product_code, amount, main_product_type=None) :

    try :
        the_product = Product.objects.get(product_code=product_code)
    except :
        the_product = None
    if the_product is None :
        return None

    product_details = get_product_details(product_code)
    if product_details is None :
        return None    

    cost = '0.00'
    if 'product_price' in product_details :
        cost = product_details['product_price']
    else :
        cost = '0.00'
    
    new_order = OrderMaster()

    new_order.user_id = user_id
    new_order.save()
    
    order_number = create_order_number(new_order.id)
    new_order.order_number = order_number
    new_order.save()

    new_order_details = OrderDetails()
    new_order_details.product = the_product
    new_order_details.amount = amount
    new_order_details.cost = cost
    new_order_details.save()

    new_order.orderdetails.add(new_order_details)
    new_order.total_amount = amount
    new_order.total_cost = cost

    if not main_product_type is None :
        new_order.extra1 = main_product_type

    new_order.tc_version = settings.TC_VERSION

    new_order.save()

    return new_order

def create_multi_product_order(user_id, product_codes, amounts, main_product_type=None) :

    if product_codes is None or len(product_codes) == 0:
        return None
    if amounts is None or len(amounts) == 0:
        return None
    if len(product_codes) != len(amounts) :
        return None

    # Do a quick test of the first product to see that it is OK
    try :
        the_product = Product.objects.get(product_code=product_codes[0])
    except :
        the_product = None
    if the_product is None :
        return None

    product_details = get_product_details(product_codes[0])
    if product_details is None :
        return None    

    new_order = OrderMaster()

    new_order.user_id = user_id
    new_order.save()
    
    order_number = create_order_number(new_order.id)
    new_order.order_number = order_number
    new_order.save()

    total_amount = 0
    total_cost = 0.00

    for i in range(0, len(product_codes)) :
        
        product_code = product_codes[i]
        amount = amounts[i]
    
        try :
            the_product = Product.objects.get(product_code=product_code)
        except :
            the_product = None
        if the_product is None :
            break

        product_details = get_product_details(product_code)
        if product_details is None :
            break

        cost = '0.00'
        if 'product_price' in product_details :
            cost = product_details['product_price']
        else :
            cost = '0.00'
    
        new_order_details = OrderDetails()
        new_order_details.product = the_product
        new_order_details.amount = amount
        new_order_details.cost = cost
        new_order_details.save()

        total_amount = total_amount + int(amount)
        total_cost = total_cost + float(cost)

        new_order.orderdetails.add(new_order_details)
        new_order.save()

    new_order.total_amount = str(total_amount)
    new_order.total_cost = str(total_cost)

    if not main_product_type is None :
        new_order.extra1 = main_product_type

    new_order.tc_version = settings.TC_VERSION
    
    new_order.save()

    return new_order

def update_order_shipping_status(order_number, status) :

    try :
        order = OrderMaster.objects.get(order_number=order_number)
    except :
        order = None

    if order is None :
        return False

    order.shipping_status = status

    order.save()


class PaymentMaster(models.Model) :

    invoice_number = models.CharField(max_length=128, null=True, blank=True)
    txn_id = models.CharField(max_length=255, null=True, blank=True)

    user_id = models.CharField(max_length=128, null=True, blank=True)

    ordermaster = models.ForeignKey(OrderMaster, null=True, blank=True, related_name='paymentmasters', on_delete=models.SET_NULL)
    
    billingdetails = models.CharField(max_length=255, null=True, blank=True)
    
    creation_date = models.DateTimeField(default=datetime.now())
    completion_date = models.DateTimeField(null=True, blank=True)
    
    extra1 = models.CharField(max_length=128, null=True, blank=True) # payment has been processed or not
    extra2 = models.CharField(max_length=64, null=True, blank=True) # agreement to the terms and conditions
    extra3 = models.CharField(max_length=32, null=True, blank=True) # id of the last payment details added to this payment master
    status = models.PositiveIntegerField(default=0) # values 1- Pending, 2 - Completed, 3 - Refund, 0 - Cancelled or not Started
    update_date = models.DateTimeField(null=True, blank=True) # updated each time a successful payment details occurs

    def __unicode__(self) :
        return u'%s' % self.invoice_number

    @classmethod
    def exists(cls, invoice_number) :
        try :
            existing_payment = cls.objects.get(invoice_number=invoice_number)
        except :
            existing_payment = None
        if existing_payment is not None :
            return True        
        return False

def get_payment_details(invoice_number) :

    details = {}

    try :
        payment = PaymentMaster.objects.get(invoice_number=invoice_number)
    except :
        payment = None
        
    if payment is None :
        return None

    if payment.ordermaster is None :
        return None

    details['invoice_number'] = payment.invoice_number
    details['user_id'] = payment.user_id
    details['order_number'] = payment.ordermaster.order_number
    details['txn_id'] = payment.txn_id
    details['creation_date'] = payment.creation_date
    details['completion_date'] = payment.completion_date
    details['update_date'] = payment.update_date
    details['status'] = payment.status

    if not payment.extra3 is None :
        last_payment_id = int(payment.extra3)
        if last_payment_id >= 0 :
            details['last_payment_id'] = last_payment_id
        else :
            details['last_payment_id'] = None
    else :
        details['last_payment_id'] = None

    if not payment.extra2 is None and len(payment.extra2) > 0:
        if payment.extra2 == 'True' :
            details['tc_acceptance'] = True
        elif payment.extra2 == 'False' :
            details['tc_acceptance'] = False
        else :
            details['tc_acceptance'] = False
    else :
        details['tc_acceptance'] = False
        
    return details    

def create_invoice_number(paymentmaster_id) :
    return create_backoffice_number('paymentmaster', paymentmaster_id)

def create_payment(order_number) :

    try :
        the_ordermaster = OrderMaster.objects.get(order_number=order_number)
    except :
        the_ordermaster = None
    if the_ordermaster is None :
        return None

    new_paymentmaster = PaymentMaster()
    new_paymentmaster.user_id = the_ordermaster.user_id
    new_paymentmaster.ordermaster = the_ordermaster
    new_paymentmaster.save()
    
    invoice_number = create_invoice_number(new_paymentmaster.id)
    new_paymentmaster.invoice_number = invoice_number
    new_paymentmaster.extra2 = 'False' # the t&c's are not accepted by default
    new_paymentmaster.save()

    return new_paymentmaster

def set_payment_tc_accept(invoice_number) :

    try :
        the_paymentmaster = PaymentMaster.objects.get(invoice_number=invoice_number)
    except :
        the_paymentmaster = None

    if the_paymentmaster is None :
        return False

    the_paymentmaster.extra2 = 'True'

    the_paymentmaster.save()

    return True

def set_payment_status(invoice_number, new_status) :

    try :
        the_paymentmaster = PaymentMaster.objects.get(invoice_number=invoice_number)
    except :
        the_paymentmaster = None

    if the_paymentmaster is None :
        return False

    the_paymentmaster.status = new_status

    the_paymentmaster.save()

    return True

def set_payment_processed(invoice_number) :

    try :
        the_paymentmaster = PaymentMaster.objects.get(invoice_number=invoice_number)
    except :
        the_paymentmaster = None

    if the_paymentmaster is None :
        return False

    the_paymentmaster.extra1 = 'True'

    the_paymentmaster.save()

    return True

def get_payment_processed(invoice_number) :

    try :
        the_paymentmaster = PaymentMaster.objects.get(invoice_number=invoice_number)
    except :
        the_paymentmaster = None

    if the_paymentmaster is None :
        return None

    is_processed = the_paymentmaster.extra1
    if not is_processed is None and len(is_processed) > 0 and is_processed == 'True':
        return True
    else:
        return False


class PaymentDetails(models.Model) :
    
    paymentmaster = models.ForeignKey(PaymentMaster, null=True, blank=True, related_name='paymentdetails', on_delete=models.SET_NULL)
    creation_date = models.DateTimeField(default=datetime.now())
    completion_date = models.DateTimeField(null=True, blank=True)

    ipn_log_id = models.CharField(max_length=128, null=True, blank=True)
    txn_id = models.CharField(max_length=255, null=True, blank=True)
    txn_type = models.CharField(max_length=255, null=True, blank=True)

    invoice = models.CharField(max_length=128, null=True, blank=True)

    payment_status = models.CharField(max_length=64, null=True, blank=True)
    payment_type = models.CharField(max_length=64, null=True, blank=True)
    pending_reason = models.CharField(max_length=64, null=True, blank=True)        

    business = models.CharField(max_length=255, null=True, blank=True)
    charset = models.CharField(max_length=128, null=True, blank=True)
    custom = models.CharField(max_length=255, null=True, blank=True)
    ipn_track_id = models.CharField(max_length=128, null=True, blank=True)
    notify_version = models.CharField(max_length=128, null=True, blank=True)
    parent_txn_id = models.CharField(max_length=255, null=True, blank=True)
    receipt_id = models.CharField(max_length=255, null=True, blank=True)
    receiver_email = models.CharField(max_length=255, null=True, blank=True)
    receiver_id = models.CharField(max_length=255, null=True, blank=True)
    resend = models.CharField(max_length=1024, null=True, blank=True)
    residence_country = models.CharField(max_length=16, null=True, blank=True)
    test_ipn = models.CharField(max_length=8, null=True, blank=True)
    verify_sign = models.CharField(max_length=255, null=True, blank=True)
	
    address_country = models.CharField(max_length=64, null=True, blank=True)
    address_city = models.CharField(max_length=64, null=True, blank=True)
    address_country_code = models.CharField(max_length=8, null=True, blank=True)
    address_name = models.CharField(max_length=128, null=True, blank=True)
    address_state = models.CharField(max_length=64, null=True, blank=True)
    address_status = models.CharField(max_length=32, null=True, blank=True)
    address_street = models.CharField(max_length=255, null=True, blank=True)
    address_zip = models.CharField(max_length=32, null=True, blank=True)
    contact_phone = models.CharField(max_length=64, null=True, blank=True)
    first_name = models.CharField(max_length=64, null=True, blank=True)
    last_name = models.CharField(max_length=64, null=True, blank=True)
    payer_business_name = models.CharField(max_length=128, null=True, blank=True)
    payer_email = models.CharField(max_length=128, null=True, blank=True)
    payer_id = models.CharField(max_length=32, null=True, blank=True)

    auth_amount = models.CharField(max_length=64, null=True, blank=True)
    auth_exp = models.CharField(max_length=32, null=True, blank=True)
    auth_id = models.CharField(max_length=32, null=True, blank=True)
    auth_status = models.CharField(max_length=64, null=True, blank=True)
    echeck_time_processed = models.CharField(max_length=255, null=True, blank=True)
    exchange_rate = models.CharField(max_length=32, null=True, blank=True)
    fraud_managment_pending_filters_x = models.CharField(max_length=255, null=True, blank=True)
    item_name = models.CharField(max_length=128, null=True, blank=True)
    item_number = models.CharField(max_length=128, null=True, blank=True)
    mc_currency = models.CharField(max_length=32, null=True, blank=True)
    mc_fee = models.CharField(max_length=64, null=True, blank=True)
    mc_gross = models.CharField(max_length=64, null=True, blank=True)
    mc_handling = models.CharField(max_length=64, null=True, blank=True)
    mc_shipping = models.CharField(max_length=64, null=True, blank=True)
    memo = models.CharField(max_length=255, null=True, blank=True)
    num_cart_items = models.CharField(max_length=32, null=True, blank=True)
    payer_status = models.CharField(max_length=32, null=True, blank=True)
    payment_date = models.CharField(max_length=64, null=True, blank=True)
    payment_fee = models.CharField(max_length=64, null=True, blank=True)
    
    payment_gross = models.CharField(max_length=64, null=True, blank=True)
    protection_eligibility = models.CharField(max_length=64, null=True, blank=True)
    quantity = models.CharField(max_length=64, null=True, blank=True)
    reason_code = models.CharField(max_length=64, null=True, blank=True)
    remaining_settle = models.CharField(max_length=64, null=True, blank=True)
    settle_amount = models.CharField(max_length=64, null=True, blank=True)
    settle_currency = models.CharField(max_length=32, null=True, blank=True)
    shipping = models.CharField(max_length=64, null=True, blank=True)
    shipping_method = models.CharField(max_length=255, null=True, blank=True)
    tax = models.CharField(max_length=64, null=True, blank=True)
    transaction_entity = models.CharField(max_length=255, null=True, blank=True)
    
    amount1 = models.CharField(max_length=64, null=True, blank=True)
    amount2 = models.CharField(max_length=64, null=True, blank=True)
    amount3 = models.CharField(max_length=64, null=True, blank=True)
    mc_amount1 = models.CharField(max_length=64, null=True, blank=True)
    mc_amount2 = models.CharField(max_length=64, null=True, blank=True)
    mc_amount3 = models.CharField(max_length=64, null=True, blank=True)
    password = models.CharField(max_length=64, null=True, blank=True)
    period1 = models.CharField(max_length=64, null=True, blank=True)
    period2 = models.CharField(max_length=64, null=True, blank=True)
    period3 = models.CharField(max_length=64, null=True, blank=True)
    reattempt = models.CharField(max_length=16, null=True, blank=True)
    recur_times = models.CharField(max_length=32, null=True, blank=True)
    recurring = models.CharField(max_length=8, null=True, blank=True)
    retry_at = models.CharField(max_length=64, null=True, blank=True)
    subscr_date = models.CharField(max_length=64, null=True, blank=True)    
    subscr_effective = models.CharField(max_length=64, null=True, blank=True)    
    subscr_id = models.CharField(max_length=32, null=True, blank=True)    
    username = models.CharField(max_length=64, null=True, blank=True)    

    case_creation_date = models.CharField(max_length=64, null=True, blank=True)    
    case_id = models.CharField(max_length=32, null=True, blank=True)    
    case_type = models.CharField(max_length=32, null=True, blank=True)    

    currency_code = models.CharField(max_length=32, null=True, blank=True)    
    handling_amount = models.CharField(max_length=64, null=True, blank=True)    
    transaction_subject = models.CharField(max_length=255, null=True, blank=True)    
       
    ipaddress = models.CharField(max_length=128, null=True, blank=True)    
    flag = models.CharField(max_length=8, null=True, blank=True)    
    flag_code = models.CharField(max_length=16, null=True, blank=True)    
    flag_info = models.CharField(max_length=255, null=True, blank=True)    
    query = models.CharField(max_length=1024, null=True, blank=True)    
    response = models.CharField(max_length=1024, null=True, blank=True)    
    created_at = models.CharField(max_length=64, null=True, blank=True)    
    updated_at = models.CharField(max_length=64, null=True, blank=True)    
    from_view = models.CharField(max_length=16, null=True, blank=True)    

    extra1 = models.CharField(max_length=128, null=True, blank=True)
    extra2 = models.CharField(max_length=64, null=True, blank=True)
    extra3 = models.CharField(max_length=32, null=True, blank=True)
    extra4 = models.CharField(max_length=255, null=True, blank=True)
    extra5 = models.CharField(max_length=255, null=True, blank=True)
    extra6 = models.CharField(max_length=255, null=True, blank=True)

    status = models.PositiveIntegerField(default=0)
    status1 = models.PositiveIntegerField(default=0)
    status2 = models.PositiveIntegerField(default=0)
    
    update_date = models.DateTimeField(null=True, blank=True)
    extra_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self) :
        return u'%s' % self.id


def create_paymentdetails_from_ipn(ipn_obj, successful=True) :

    invoice_number = ipn_obj.invoice
    txn_id = ipn_obj.txn_id
    txn_type = ipn_obj.txn_type

    real_invoice_number = invoice_number
    code = None
    invoice_number_elements = invoice_number.split("_")
    if len(invoice_number_elements) > 1 :
        real_invoice_number = invoice_number_elements[0]
        code = invoice_number_elements[1]
    
    try :
        the_paymentmaster = PaymentMaster.objects.get(invoice_number=real_invoice_number)
    except :
        the_paymentmaster = None

    if the_paymentmaster is None :
        #create_backoffice_log_entry(category='IPN_ERROR', message='IPN object (%d) of type %s with transaction id %s has no valid invoice number (%s)' % (ipn_obj.id, txn_type, txn_id, invoice_number), tablename='PAYMENTMASTER')
        return None

    new_details = PaymentDetails()
    new_details.invoice = invoice_number
    new_details.txn_type = txn_type
    new_details.txn_id = txn_id
    new_details.ipn_log_id = ipn_obj.id

    new_details.save()
    
    new_details.payment_status = ipn_obj.payment_status
    new_details.payment_type = ipn_obj.payment_type
    new_details.pending_reason = ipn_obj.pending_reason

    new_details.save()

    new_details.business = str(ipn_obj.business)
    new_details.charset = str(ipn_obj.charset)
    new_details.custom = str(ipn_obj.custom)
    
    new_details.notify_version = str(ipn_obj.notify_version)
    new_details.parent_txn_id = str(ipn_obj.parent_txn_id)
    new_details.receipt_id = str(ipn_obj.receipt_id)
    new_details.receiver_email = str(ipn_obj.receiver_email)
    new_details.receiver_id = str(ipn_obj.receiver_id)
    new_details.residence_country = str(ipn_obj.residence_country)
    new_details.test_ipn = str(ipn_obj.test_ipn)
    new_details.verify_sign = str(ipn_obj.verify_sign)

    new_details.save()

    new_details.address_country = str(ipn_obj.address_country)
    new_details.address_city = str(ipn_obj.address_city)
    new_details.address_country_code = str(ipn_obj.address_country_code)
    new_details.address_name = str(ipn_obj.address_name)
    new_details.address_state = str(ipn_obj.address_state)
    new_details.address_status = str(ipn_obj.address_status)
    new_details.address_street = str(ipn_obj.address_street)
    new_details.address_zip = str(ipn_obj.address_zip)
    new_details.contact_phone = str(ipn_obj.contact_phone)
    new_details.first_name = str(ipn_obj.first_name)
    new_details.last_name = str(ipn_obj.last_name)
    new_details.payer_business_name = str(ipn_obj.payer_business_name)
    new_details.payer_email = str(ipn_obj.payer_email)
    new_details.payer_id = str(ipn_obj.payer_id)

    new_details.save()

    new_details.auth_amount = str(ipn_obj.auth_amount)
    new_details.auth_exp = str(ipn_obj.auth_exp)
    new_details.auth_id = str(ipn_obj.auth_id)
    new_details.auth_status = str(ipn_obj.auth_status)
    new_details.exchange_rate = str(ipn_obj.exchange_rate)

    new_details.save()
       
    new_details.item_name = str(ipn_obj.item_name)
    new_details.item_number = str(ipn_obj.item_number)
    new_details.mc_currency = str(ipn_obj.mc_currency)
    new_details.mc_fee = str(ipn_obj.mc_fee)
    new_details.mc_gross = str(ipn_obj.mc_gross)
    new_details.mc_handling = str(ipn_obj.mc_handling)
    new_details.mc_shipping = str(ipn_obj.mc_shipping)
    new_details.memo = str(ipn_obj.memo)
    new_details.num_cart_items = str(ipn_obj.num_cart_items)
    new_details.payer_status = str(ipn_obj.payer_status)
    new_details.payment_date = str(ipn_obj.payment_date)

    new_details.save()

    new_details.payment_gross = str(ipn_obj.payment_gross)
    new_details.protection_eligibility = str(ipn_obj.protection_eligibility)
    new_details.quantity = str(ipn_obj.quantity)
    new_details.reason_code = str(ipn_obj.reason_code)
    new_details.remaining_settle = str(ipn_obj.remaining_settle)
    new_details.settle_amount = str(ipn_obj.settle_amount)
    new_details.settle_currency = str(ipn_obj.settle_currency)
    new_details.shipping = str(ipn_obj.shipping)
    new_details.shipping_method = str(ipn_obj.shipping_method)
    new_details.tax = str(ipn_obj.tax)
    new_details.transaction_entity = str(ipn_obj.transaction_entity)

    new_details.save()

    new_details.amount1 = str(ipn_obj.amount1)
    new_details.amount2 = str(ipn_obj.amount2)
    new_details.amount3 = str(ipn_obj.amount3)
    new_details.mc_amount1 = str(ipn_obj.mc_amount1)
    new_details.mc_amount2 = str(ipn_obj.mc_amount2)
    new_details.mc_amount3 = str(ipn_obj.mc_amount3)
    new_details.password = str(ipn_obj.password)
    new_details.period1 = str(ipn_obj.period1)
    new_details.period2 = str(ipn_obj.period2)
    new_details.period3 = str(ipn_obj.period3)
    new_details.reattempt = str(ipn_obj.reattempt)
    new_details.recur_times = str(ipn_obj.recur_times)
    new_details.recurring = str(ipn_obj.recurring)
    new_details.retry_at = str(ipn_obj.retry_at)
    new_details.subscr_date = str(ipn_obj.subscr_date)
    new_details.subscr_effective = str(ipn_obj.subscr_effective)
    new_details.subscr_id = str(ipn_obj.subscr_id)
    new_details.username = str(ipn_obj.username)
        
    new_details.save()

    new_details.case_creation_date = str(ipn_obj.case_creation_date)
    new_details.case_id = str(ipn_obj.case_id)
    new_details.case_type = str(ipn_obj.case_type)

    new_details.save()

    new_details.currency_code = str(ipn_obj.currency_code)
    new_details.handling_amount = str(ipn_obj.handling_amount)
    new_details.transaction_subject = str(ipn_obj.transaction_subject)

    new_details.save()

    new_details.ipaddress = str(ipn_obj.ipaddress)
    new_details.flag = str(ipn_obj.flag)
    new_details.flag_code = str(ipn_obj.flag_code)
    new_details.flag_info = str(ipn_obj.flag_info)
    new_details.query = str(ipn_obj.query)
    new_details.response = str(ipn_obj.response)
    new_details.created_at = str(ipn_obj.created_at)
    new_details.updated_at = str(ipn_obj.updated_at)
    new_details.from_view = str(ipn_obj.from_view)

    new_details.save()

    the_paymentmaster.paymentdetails.add(new_details)

    if successful :
        the_paymentmaster.extra3 = new_details.id

        # In future need to handle situation when the txn_id can change
        # look at parent_txn_id instead        
        the_paymentmaster.txn_id = new_details.txn_id

        if new_details.payment_status == 'Pending' :
            the_paymentmaster.status = 1
        elif new_details.payment_status == 'Completed' :
            the_paymentmaster.status = 2
            the_paymentmaster.completion_date = datetime.now()

        the_paymentmaster.update_date = datetime.now()
        
    the_paymentmaster.save()

    #create_backoffice_log_entry('PROCESS_PAYMENT_IPN', 'Creating PaymentDetails for IPN %s message with txn_id %s, invoice number %s and IPN_OBJ id %s' % (new_details.payment_status, new_details.txn_id, new_details.invoice, new_details.ipn_log_id))

    return new_details

    
def get_extended_payment_details(paymentdetails_id) :

    details = {}

    try :
        paymentdetails_obj = PaymentDetails.objects.get(id=paymentdetails_id)
    except :
        paymentdetails_obj = None
        
    if paymentdetails_obj is None :
        return None

    details['invoice_number'] = paymentdetails_obj.paymentmaster.invoice_number
    details['creation_date'] = paymentdetails_obj.creation_date
    details['completion_date'] = paymentdetails_obj.completion_date

    details['ipn_log_id'] = paymentdetails_obj.ipn_log_id
    details['txn_id'] = paymentdetails_obj.txn_id
    details['txn_type'] = paymentdetails_obj.txn_type

    details['invoice'] = paymentdetails_obj.invoice

    details['payment_status'] = paymentdetails_obj.payment_status
    details['payment_type'] = paymentdetails_obj.payment_type
    details['pending_reason'] = paymentdetails_obj.pending_reason

    details['business'] = paymentdetails_obj.business
    details['charset'] = paymentdetails_obj.charset
    details['custom'] = paymentdetails_obj.custom
    details['notify_version'] = paymentdetails_obj.notify_version
    details['parent_txn_id'] = paymentdetails_obj.parent_txn_id
    details['receipt_id'] = paymentdetails_obj.receipt_id
    details['receiver_email'] = paymentdetails_obj.receiver_email
    details['receiver_id'] = paymentdetails_obj.receiver_id
    details['residence_country'] = paymentdetails_obj.residence_country
    details['test_ipn'] = paymentdetails_obj.test_ipn
    details['verify_sign'] = paymentdetails_obj.verify_sign
	
    details['address_country'] = paymentdetails_obj.address_country
    details['address_city'] = paymentdetails_obj.address_city
    details['address_country_code'] = paymentdetails_obj.address_country_code
    details['address_name'] = paymentdetails_obj.address_name
    details['address_state'] = paymentdetails_obj.address_state
    details['address_status'] = paymentdetails_obj.address_status
    details['address_street'] = paymentdetails_obj.address_street
    details['address_zip'] = paymentdetails_obj.address_zip
    details['contact_phone'] = paymentdetails_obj.contact_phone
    details['first_name'] = paymentdetails_obj.first_name
    details['last_name'] = paymentdetails_obj.last_name
    details['payer_business_name'] = paymentdetails_obj.payer_business_name
    details['payer_email'] = paymentdetails_obj.payer_email
    details['payer_id'] = paymentdetails_obj.payer_id

    details['auth_amount'] = paymentdetails_obj.auth_amount
    details['auth_exp'] = paymentdetails_obj.auth_exp
    details['auth_id'] = paymentdetails_obj.auth_id
    details['auth_status'] = paymentdetails_obj.auth_status
    details['exchange_rate'] = paymentdetails_obj.exchange_rate
    details['item_name'] = paymentdetails_obj.item_name
    details['item_number'] = paymentdetails_obj.item_number
    details['mc_currency'] = paymentdetails_obj.mc_currency
    details['mc_fee'] = paymentdetails_obj.mc_fee
    details['mc_gross'] = paymentdetails_obj.mc_gross
    details['mc_handling'] = paymentdetails_obj.mc_handling
    details['mc_shipping'] = paymentdetails_obj.mc_shipping
    details['memo'] = paymentdetails_obj.memo
    details['num_cart_items'] = paymentdetails_obj.num_cart_items
    details['payer_status'] = paymentdetails_obj.payer_status
    details['payment_date'] = paymentdetails_obj.payment_date
    
    details['payment_gross'] = paymentdetails_obj.payment_gross
    details['protection_eligibility'] = paymentdetails_obj.protection_eligibility
    details['quantity'] = paymentdetails_obj.quantity
    details['reason_code'] = paymentdetails_obj.reason_code
    details['remaining_settle'] = paymentdetails_obj.remaining_settle
    details['settle_amount'] = paymentdetails_obj.settle_amount
    details['settle_currency'] = paymentdetails_obj.settle_currency
    details['shipping'] = paymentdetails_obj.shipping
    details['shipping_method'] = paymentdetails_obj.shipping_method
    details['tax'] = paymentdetails_obj.tax
    details['transaction_entity'] = paymentdetails_obj.transaction_entity
    
    details['amount1'] = paymentdetails_obj.amount1
    details['amount2'] = paymentdetails_obj.amount2
    details['amount3'] = paymentdetails_obj.amount3
    details['mc_amount1'] = paymentdetails_obj.mc_amount1
    details['mc_amount2'] = paymentdetails_obj.mc_amount2
    details['mc_amount3'] = paymentdetails_obj.mc_amount3
    details['password'] = paymentdetails_obj.password
    details['period1'] = paymentdetails_obj.period1
    details['period2'] = paymentdetails_obj.period2
    details['period3'] = paymentdetails_obj.period3
    details['reattempt'] = paymentdetails_obj.reattempt
    details['recur_times'] = paymentdetails_obj.recur_times
    details['recurring'] = paymentdetails_obj.recurring
    details['retry_at'] = paymentdetails_obj.retry_at
    details['subscr_date'] = paymentdetails_obj.subscr_date
    details['subscr_effective'] = paymentdetails_obj.subscr_effective
    details['subscr_id'] = paymentdetails_obj.subscr_id
    details['username'] = paymentdetails_obj.username

    details['case_creation_date'] = paymentdetails_obj.case_creation_date
    details['case_id'] = paymentdetails_obj.case_id
    details['case_type'] = paymentdetails_obj.case_type

    details['currency_code'] = paymentdetails_obj.currency_code
    details['handling_amount'] = paymentdetails_obj.handling_amount
    details['transaction_subject'] = paymentdetails_obj.transaction_subject
       
    details['ipaddress'] = paymentdetails_obj.ipaddress
    details['flag'] = paymentdetails_obj.flag
    details['flag_code'] = paymentdetails_obj.flag_code
    details['flag_info'] = paymentdetails_obj.flag_info
    details['query'] = paymentdetails_obj.query
    details['response'] = paymentdetails_obj.response
    details['created_at'] = paymentdetails_obj.created_at
    details['updated_at'] = paymentdetails_obj.updated_at
    details['from_view'] = paymentdetails_obj.from_view

    details['extra1'] = paymentdetails_obj.extra1
    details['extra2'] = paymentdetails_obj.extra2
    details['extra3'] = paymentdetails_obj.extra3
    details['extra4'] = paymentdetails_obj.extra4
    details['extra5'] = paymentdetails_obj.extra5
    details['extra6'] = paymentdetails_obj.extra6

    details['status'] = paymentdetails_obj.status
    details['status1'] = paymentdetails_obj.status1
    details['status2'] = paymentdetails_obj.status2
    
    details['update_date'] = paymentdetails_obj.update_date
    details['extra_date'] = paymentdetails_obj.extra_date

    return details



class Subscription(models.Model) :
    
    user_id = models.CharField(max_length=128, null=True, blank=True)

    payment = models.OneToOneField(PaymentMaster, null=True, blank=True, related_name='subscription', on_delete=models.SET_NULL)
    product = models.ForeignKey(Product, null=True, blank=True, related_name='subscriptions', on_delete=models.SET_NULL)

    autorenew = models.PositiveIntegerField(default=0)
    period = models.PositiveIntegerField(default=0)

    creation_date = models.DateTimeField(default=datetime.now())
    expiry_date = models.DateTimeField(null=True, blank=True)

    extra1 = models.CharField(max_length=128, null=True, blank=True) # reserved for future subscription id
    extra2 = models.CharField(max_length=64, null=True, blank=True) # reserved for annotation about latest change
    extra3 = models.CharField(max_length=32, null=True, blank=True) # 25022013 reserved for whether or not T&Cs in version 2.0 have been accepted (when subscription happens without a payment)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self) :
        return u'%s' % self.user_id

def create_subscription_number(order_id) :
    return create_backoffice_number('subscription', order_id)

# 25022013 extended to use field extra3 to store version of the T&Cs that have been accepted for v2.0 of RocketTags
def create_subscription(user_id, invoice_number, subscription_period, autorenew=False, accepted_tcs_version=None) :

    try :
        subscription = Subscription.objects.get(user_id=user_id)
    except :
        subscription = None
        
    if not subscription is None :
        return None

    # 29062012 an invoice number need not be provided
    # complimentary subscriptions will have no invoice number
    # but what if they then purchase a subscription!!
    thepayment = None
    theproduct = None
    if not invoice_number is None :
        payment_details = get_payment_details(invoice_number)
        if payment_details is None :
            return None

        primary_product_details = get_order_line_item(payment_details['order_number'], 0)
        if primary_product_details is None :
            return None

        try :
            thepayment = PaymentMaster.objects.get(invoice_number=invoice_number)
        except :
            thepayment = None
        if thepayment is None :
            return None

        try :
            theproduct = Product.objects.get(product_code=primary_product_details['product_code'])
        except :
            theproduct = None
        if theproduct is None :
            return None    

    subscription = Subscription(user_id=user_id)

    subscription.payment = thepayment
    subscription.product = theproduct

    subscription.autorenew = autorenew
    subscription.period = subscription_period

    subscription.expiry_date = datetime.now() + timedelta(days=subscription.period)

    subscription.save()

    subscription.extra1 = create_subscription_number(subscription.id)

    if not accepted_tcs_version is None :
        subscription.extra3 = accepted_tcs_version

    subscription.update_date = datetime.now()    
    subscription.save()

    return subscription

# 25022013 extended to use field extra3 to store version of the T&Cs that have been accepted for v2.0 of RocketTags
def update_subscription(user_id, invoice_number, product_code, subscription_period, status, annotate_text=None, accepted_tcs_version=None) :

    try :
        subscription = Subscription.objects.get(user_id=user_id)
    except:
        subscription = None

    if subscription is None :
        return False

    thepayment = None
    theproduct = None
    if not invoice_number is None :
        payment_details = get_payment_details(invoice_number)
        if payment_details is None :
            return False

        primary_product_details = get_order_line_item(payment_details['order_number'], 0)
        if primary_product_details is None :
            return False

        try :
            thepayment = PaymentMaster.objects.get(invoice_number=invoice_number)
        except :
            thepayment = None
        if thepayment is None :
            return False

        try :
            theproduct = Product.objects.get(product_code=primary_product_details['product_code'])
        except :
            theproduct = None
        if theproduct is None :
            return False    

    if not thepayment is None and not theproduct is None :
        subscription.payment = thepayment
        subscription.product = theproduct
        
    if not product_code is None and len(product_code) > 0 :
        try :
            product = Product.objects.get(product_code=product_code)
        except :
            product = None
        if not product is None :
            subscription.product = product

    subscription.status = status

    if not subscription_period is None and subscription_period > 0 :
        remaining_period = 0 # How do we work out the remaining period?
        if datetime.now() < subscription.expiry_date :
            remaining_delta = subscription.expiry_date - datetime.now()
            remaining_period = remaining_delta.days + 1

        subscription_period = remaining_period + subscription_period
        subscription.period = subscription_period

        subscription.expiry_date = datetime.now() + timedelta(days=subscription_period)

    subscription.update_date = datetime.now()

    if not annotate_text is None and len(annotate_text) > 0 :
        subscription.extra2 = str(annotate_text)

    if not accepted_tcs_version is None :
        subscription.extra3 = accepted_tcs_version


    subscription.save()

    return True


def get_subscription_details(user_id) :

    details = {}

    try :
        subscription = Subscription.objects.get(user_id=user_id)
    except :
        subscription = None
    if subscription is None :
        details['subscription_exists'] = False
        return details

    details['subscription_exists'] = True

    details['user_id'] = subscription.user_id
    details['autorenew'] = subscription.autorenew
    details['period'] = subscription.period
    details['expiry_date'] = subscription.expiry_date
    details['subscription_number'] = subscription.extra1
    details['promo_code'] = subscription.extra2
    # 25022013 new field for whether T&Cs have been accepted
    details['accepted_tcs_version'] = subscription.extra3

    if not subscription.payment is None :
        details['invoice_number'] = subscription.payment.invoice_number
    else :
        details['invoice_number'] = None
    
    if not subscription.product is None :
        details['product_code'] = subscription.product.product_code
    else :
        details['product_code'] = None
    
    return details
    




def payment_was_successful_handler(sender, **kwargs):
    ipn_obj = sender
    payment_details = create_paymentdetails_from_ipn(ipn_obj, successful=True)

payment_was_successful.connect(payment_was_successful_handler)

def payment_was_flagged_handler(sender, **kwargs):
    ipn_obj = sender
    payment_details = create_paymentdetails_from_ipn(ipn_obj, successful=False)

payment_was_flagged.connect(payment_was_flagged_handler)
