from django.contrib import auth
from django.contrib.auth.models import User
from django.conf import settings
from rockettags.store import models
from boto.fps.connection import *
from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.ipn.signals import payment_was_successful


# Constant Values
ORDER_SHIPPING_STATUS_CREATED = 0
ORDER_SHIPPING_STATUS_READY = 1
ORDER_SHIPPING_STATUS_SHIPPED = 2

SUBSCRIBED_STATUS_NOT_SUBSCRIBED = 0
SUBSCRIBED_STATUS_WAITING = 1
SUBSCRIBED_STATUS_FULLY_SUBSCRIBED = 2

PAYMENT_STATUS_PENDING = 1
PAYMENT_STATUS_COMPLETED = 2
PAYMENT_STATUS_REFUND = 3
PAYMENT_STATUS_CANCELLED = 0
PAYMENT_STATUS_NEVER_INITIATED = 0


def get_products(product_types) :

    product_codes = {}
    product_prices = {}

    all_products = models.Product.objects.all()
    if all_products is None :
        return (product_codes, product_prices)

    for product in all_products :
        if product.product_type.type_name in product_types :
            product_codes[product.product_code] = product.product_code
            product_details = get_product_details(product.product_code)
            if not product_details is None :
                if 'product_price' in product_details :
                    product_prices[product.product_code] = product_details['product_price']
                else :
                    product_prices[product.product_code] = "0.00"

    return (product_codes, product_prices)

def obtain_full_product_code(product_code, num_stickers) :

    return "%s_%s" % (product_code, str(num_stickers))


def get_product_details(product_code) :

    details = models.get_product_details(product_code)

    return details

def get_product_name(product_code) :

    details = get_product_details(product_code)
    if details is None :
        return None

    return details['product_name']


def get_order_details(order_number) :

    details = models.get_order_details(order_number)

    return details

def get_num_order_line_items(order_number):
    return models.get_num_order_line_items(order_number)

def get_order_line_item(order_number, line_number) :

    details = models.get_order_line_item(order_number, line_number)

    return details    
    
def get_payment_details(invoice_number) :

    details = models.get_payment_details(invoice_number)

    return details    

def get_single_product_details_from_order(order_number) :

    order_line_details = get_order_line_item(order_number, 0)
    if order_line_details is None :
        return None

    product_code = order_line_details['product_code']
    
    details = get_product_details(product_code)

    return details


def get_combined_product_name_from_order(order_number, order_line_numbers) :

    product_name = ''
    if order_line_numbers is None or len(order_line_numbers) == 0 or len(order_line_numbers) > 2:
        return None

    if len(order_line_numbers) == 1:
        order_line_details = get_order_line_item(order_number, order_line_numbers[0])
        if order_line_details is None :
            return None

        product_code = order_line_details['product_code']    
        details = get_product_details(product_code)
        if details is None:
            return None
        return details['product_name']

    elif len(order_line_numbers) == 2:
        order_line_details1 = get_order_line_item(order_number, order_line_numbers[0])
        if order_line_details1 is None :
            return None
        product_code1 = order_line_details1['product_code']    
        details1 = get_product_details(product_code1)
        if details1 is None:
            return None
        product_name1 = details1['product_name']

        order_line_details2 = get_order_line_item(order_number, order_line_numbers[1])
        if order_line_details2 is None :
            return None
        product_code2 = order_line_details2['product_code']    
        details2 = get_product_details(product_code2)
        if details2 is None:
            return None
        product_name2 = details2['product_name']
        
        product_name = '%s with %s' % (product_name1, product_name2)

        return product_name
    

def generate_amazon_button(order_number, store_fields) :

    order_details = get_order_details(order_number)

    conn = FPSConnection(aws_access_key_id=settings.FPS_AWS_ACCESS_KEY, aws_secret_access_key=settings.FPS_AWS_SECRET_KEY)

    # 26052012 Caller reference consists of 2 parts
    # the order number and a promo field
    # 11062012 pass the tag code through if present alternate to the promo code
    tag_code = store_fields['tag_code']
    promo_code = store_fields['promo_code']
    if len(tag_code) > 0 :
        callerReference = '%s_%s' % (order_number, tag_code)
    elif len(promo_code) > 0 :
        callerReference = '%s_%s' % (order_number, promo_code)
    else :
        callerReference = '%s' % (order_number)

    response = conn.make_url(returnURL="http://23.21.227.115/store/payment_confirmation/", paymentReason="A Test Purchase of 1 Years Subscription", pipelineName="SingleUse", transactionAmount="2", callerReference=callerReference)    

    return response

def generate_paypal_button(invoice_number, store_fields) :

    invoice = ''
    product_name = None
    order_amount = 1
    order_cost = '0.00'
    
    invoice_details = get_payment_details(invoice_number)
    if not invoice_details is None :
        order_details = get_order_details(invoice_details['order_number'])
        if not order_details is None :
            order_amount = order_details['total_amount']
            order_cost = order_details['total_cost']
            #product_details = get_single_product_details_from_order(order_details['order_number'])
            #if not product_details is None :
            #    product_name = product_details['product_name']
            num_order_items = get_num_order_line_items(order_details['order_number'])
            if num_order_items == 0:
                return None
            elif num_order_items == 1:
                product_name = get_combined_product_name_from_order(order_details['order_number'], [0])
            elif num_order_items == 2:
                product_name = get_combined_product_name_from_order(order_details['order_number'], [0, 1])
                
    if product_name is None :
        return None
        
    # 26052012 Caller reference consists of 2 parts
    # the order number and a promo field
    # 11062012 pass the tag code through if present alternate to the promo code
    tag_code = store_fields['tag_code']
    promo_code = store_fields['promo_code']
    
    if len(tag_code) > 0 :
        invoice = '%s_%s' % (invoice_number, tag_code)
    elif len(promo_code) > 0 :
        invoice = '%s_%s' % (invoice_number, promo_code)
    else :
        invoice = '%s' % (invoice_number)

    # What you want the button to do.
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "quantity" : order_amount,
        "amount": order_cost,
        "item_name": product_name,
        "invoice": invoice,
        "notify_url": "https://www.rockettags.com/s/pp/x0tnxz/",
        "return_url": "https://www.rockettags.com/pp-return/",
        "cancel_return": "https://www.rockettags.com/pp-cancel/",

    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)

    return form

def get_amazon_fields(get_fields) :

    amazon_fields = {}

    status = ''
    signatureVersion = ''
    tokenID = ''
    certificateUrl  = ''
    expiry  = ''
    signatureMethod = ''
    callerReference = ''
    signature = ''

    if 'status' in get_fields :
        status = get_fields['status']

    if 'signatureVersion' in get_fields :
        signatureVersion = get_fields['signatureVersion']

    if 'tokenID' in get_fields :
        tokenID = get_fields['tokenID']
        
    if 'certificateUrl' in get_fields :
        certificateUrl = get_fields['certificateUrl']
    
    if 'expiry' in get_fields :
        expiry = get_fields['expiry']

    if 'signatureMethod' in get_fields :
        signatureMethod = get_fields['signatureMethod']

    if 'callerReference' in get_fields :
        callerReference = get_fields['callerReference']

    if 'signature' in get_fields :
        signature = get_fields['signature']

    amazon_fields['amazon_status'] = status
    amazon_fields['amazon_signatureVersion'] = signatureVersion
    amazon_fields['amazon_tokenID'] = tokenID
    amazon_fields['amazon_certificateUrl'] = certificateUrl
    amazon_fields['amazon_expiry'] = expiry
    amazon_fields['amazon_signatureMethod'] = signatureMethod
    amazon_fields['amazon_callerReference'] = callerReference
    amazon_fields['amazon_signature'] = signature
    
    return amazon_fields


def amazon_succeeded(amazon_fields) :

    # check amazon_fields['amazon_status']
    
    return True

def create_amazon_payment(order_details, amazon_details) :
    payment_details = {}

    return payment_details


def create_single_product_order(user_id, amount, product_code, main_product_type=None) :

    order = models.create_single_product_order(user_id, product_code, amount, main_product_type)    
    if order is None :
        return None

    return order.order_number

def create_multi_product_order(user_id, amounts, product_codes, main_product_type=None) :

    order = models.create_multi_product_order(user_id, product_codes, amounts, main_product_type)    
    if order is None :
        return None

    return order.order_number
    
def update_order_shipping_status(order_number, status) :

    return models.update_order_shipping_status(order_number, status)

def create_payment(order_number) :

    payment = models.create_payment(order_number)
    if payment is None :
        return None

    return payment.invoice_number

def set_payment_tc_accept(invoice_number) :

    return models.set_payment_tc_accept(invoice_number)

def set_payment_status(invoice_number, new_status) :

    return models.set_payment_status(invoice_number, new_status)

def set_payment_processed(invoice_number) :
    return models.set_payment_processed(invoice_number)

def get_payment_processed(invoice_number) :
    return models.get_payment_processed(invoice_number)

def get_extended_payment_details(paymentdetails_id) :

    return models.get_extended_payment_details(paymentdetails_id)

def get_last_extended_payment_details(invoice_number) :

    details = get_payment_details(invoice_number)
    if details is None :
        return None

    last_payment_id = details['last_payment_id']
    if last_payment_id is None :
        return None

    extended_details = get_extended_payment_details(last_payment_id)

    return extended_details

# 25022013 extended to use field extra3 to store version of the T&Cs that have been accepted for v2.0 of RocketTags
def create_subscription(user_id, invoice_number, subscription_period, autorenew=False, accepted_tcs_version=None) :

    subscription = models.create_subscription(user_id, invoice_number, subscription_period, autorenew)

    if subscription is None :
        return None

    details = get_subscription_details(user_id)
    if details is None or not details['subscription_exists'] :
        return None
    
    return details['subscription_number']

def get_subscription_details(user_id) :

    details = models.get_subscription_details(user_id)

    return details

# 25022013 extended to use field extra3 to store version of the T&Cs that have been accepted for v2.0 of RocketTags
def update_subscription(user_id, invoice_number, product_code, subscription_period, status, annotate_text=None, accepted_tcs_version=None) :

    return models.update_subscription(user_id, invoice_number, product_code, subscription_period, status, annotate_text)

