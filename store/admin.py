from models import ProductType, Product, ProductPrice, OrderMaster, OrderDetails, PaymentMaster, PaymentDetails, Subscription, Stickerbook
from django.contrib import admin

class ProductTypeAdmin(admin.ModelAdmin):
    # ...
    list_display = ('type_name', 'category')

class ProductAdmin(admin.ModelAdmin):
    # ...
    list_display = ('product_name', 'product_type')

class StickerbookAdmin(admin.ModelAdmin) :
    # ...
    list_display = ('description', 'pack_id')
    
admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductPrice)
admin.site.register(Stickerbook, StickerbookAdmin)
admin.site.register(OrderMaster)
admin.site.register(OrderDetails)
admin.site.register(PaymentMaster)
admin.site.register(PaymentDetails)
admin.site.register(Subscription)
