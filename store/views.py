from django.template import Context, loader, RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.conf import settings
from rockettags.lists import page_codes
from rockettags.main.screenelements import get_bottom_items, get_main_top_menu, get_central_fragment
from rockettags.main.screenelements import get_query_string_fields
from rockettags.main.models import update_profile, test_if_subscribed, get_user, is_subscribed, get_subscribed_status, add_address_details, get_address_details
from rockettags.main.models import create_log_entry
from rockettags.tags.models import primary_tag, get_tag_type, get_tag, tag_exists
from rockettags.links import links
from rockettags.fragments import query_string
from django.views.decorators.csrf import csrf_exempt
import datetime
from rockettags.main import mailers

from rockettags.store import backoffice
from rockettags.main.dropdowns import get_countries, get_states, get_cities, get_country_name, get_state_name

#import uuid

@csrf_exempt
def returnview(request):

   txn_id = None
   if request.method == 'POST' :
        txn_id = request.POST['txn_id']
   elif request.method == 'GET':
        if 'txn_id' in request.GET : txn_id = request.GET['txn_id']

   invoice_number = None
   ext_invoice_number = None
   code = None
   if request.method == 'POST' :
        ext_invoice_number = request.POST['invoice']
   elif request.method == 'GET':
        if 'invoice_number' in request.GET : invoice_number = request.GET['invoice_number']

   if invoice_number is None :
      ext_invoice_elements = ext_invoice_number.split('_')
      invoice_number = ext_invoice_elements[0]
      code = None
      if len(ext_invoice_elements) > 1 :
          code = ext_invoice_elements[1]

   # Main handler for the Process Payment workflow
   process_details = process_payment(request, invoice_number, code)
   if not process_details['process_success'] :
      create_log_entry('PROCESS_PAYMENT_ERROR', process_details['process_error_message'])

   details = backoffice.get_last_extended_payment_details(invoice_number)

   if not details is None :
      #if not details['custom'] is None and len(details['custom']) > 0 and details['custom'] == 'True' :
      if request.user.is_authenticated() and not request.user.is_anonymous() :
         logged_in = True
      else :
         logged_in = False

      output = ""
      if process_details['process_success'] :    
      
         try :

            if not details is None :

               output = '%sFirstname: %s\n' % (output, details['first_name'])
               output = '%sLastname: %s\n' % (output, details['last_name'])
               output = '%sUser ID: %s\n' % (output, process_details['user_id'])
               output = '%slogged_in: %s\n\n' % (output, logged_in)
               
               output = '%stxn_id: %s\n' % (output, details['txn_id'])
               output = '%spayment status: %s\n' % (output, details['payment_status'])
               output = '%spending reason: %s\n' % (output, details['pending_reason'])

               if 'process_main_product_type' in process_details :
                  output = '%smain product_type: %s\n' % (output, process_details['process_main_product_type'])
               if 'process_product_subscription_period' in process_details :
                  output = '%sproduct subscription period: %s\n' % (output, process_details['process_product_subscription_period'])
               if 'process_num_days' in process_details :
                  output = '%sNumber of promotional days: %s\n' % (output, process_details['process_num_days'])
               if 'process_user_subscribed_status' in process_details :
                  output = '%sUser subscribed status: %s\n' % (output, process_details['process_user_subscribed_status'])
               if 'process_product_type' in process_details :
                  output = '%sMain order line item product type: %s\n' % (output, process_details['process_product_type'])
               
                              
            else :
               output = "empty1"
               
         except:
              output = "empty2"

      else :
         output = process_details['process_error_message']
         
   else :
      output = "Your payment hasn't come through yet"
      logged_in = False

      
   #t = loader.get_template('store/return.html')

   #c = RequestContext(request, {'output' : output, 'txn_id' : txn_id, 'invoice_number' : invoice_number, })

   #return HttpResponse(t.render(c))

   page_name = 'payment_confirmation'
   store_fields = {}
   extra_fields = {'output' : output, 'invoice_number' : invoice_number, }
   if not details is None :
      extra_fields.update(details)   
   extra_fields.update(process_details)


   # 31052012 If user is logged in use MyRocketTags bottom boxes
   # Otherwise use general bottom boxes
   if logged_in :
       t = loader.get_template('mytags/template.html')
   else :
       t = loader.get_template('store/template.html')
        
   (central_store_fragment, full_page_name) = get_central_store_fragment(request, page_name, store_fields, extra_fields, [])
   my_codes = {"request" : request,
        'main_top_menu_fragment' : get_main_top_menu(request),
        'central_fragment' : central_store_fragment,
        'page_name' : full_page_name,
       }
   my_codes.update(get_bottom_items(request))
            
   c = RequestContext(request, my_codes)
   return HttpResponse(t.render(c))


@csrf_exempt
def cancelview(request):
     
   page_name = 'payment_cancellation'
   store_fields = {}
   extra_fields = {}

   # 31052012 If user is logged in use MyRocketTags bottom boxes
   # Otherwise use general bottom boxes
   if request.user.is_authenticated() and not request.user.is_anonymous() :
      logged_in = True
   else :
      logged_in = False
      
   if logged_in :
       t = loader.get_template('mytags/template.html')
   else :
       t = loader.get_template('store/template.html')
        
   (central_store_fragment, full_page_name) = get_central_store_fragment(request, page_name, store_fields, extra_fields, [])
   my_codes = {"request" : request,
        'main_top_menu_fragment' : get_main_top_menu(request),
        'central_fragment' : central_store_fragment,
        'page_name' : full_page_name,
       }
   my_codes.update(get_bottom_items(request))
            
   c = RequestContext(request, my_codes)
   return HttpResponse(t.render(c))



def get_store_fields(request) :
    product_code = ''
    product_price = '0.00'
    product_code_tags = ''
    product_price_tags = '0.00'
    tag_code = ''
    email = ''
    num_tag_sheets = ''
    num_tags = '0'
    promo_code = ''
    amount = '0'
    invoice_number = ''
    
    (query_string_fields, next_page) = get_query_string_fields(request)

    if not query_string_fields is None :
        if 'tagid' in query_string_fields :
            tag_code = query_string_fields['tagid']
        elif 'id' in query_string_fields :
            tag_code = query_string_fields['id']
        if 'email' in query_string_fields :
            email = query_string_fields['email']

        if 'product_code' in query_string_fields :
            product_code = query_string_fields['product_code']
        if 'product_price' in query_string_fields :
            product_price = query_string_fields['product_price']

        if 'product_code_tags' in query_string_fields :
            product_code_tags = query_string_fields['product_code_tags']
        if 'product_price_tags' in query_string_fields :
            product_price_tags = query_string_fields['product_price_tags']

        if 'num_tag_sheets' in query_string_fields :
            num_tag_sheets = query_string_fields['num_tag_sheets']
        if 'num_tags' in query_string_fields :
            num_tags = query_string_fields['num_tags']
        if 'promo_code' in query_string_fields :
            promo_code = query_string_fields['promo_code']
        if 'amount' in query_string_fields :
            amount = query_string_fields['amount']

        if 'invoice_number' in query_string_fields :
            invoice_number = query_string_fields['invoice_number']
        

    if request.method == 'POST' :
        if 'tagid' in request.POST :
            tag_code = request.POST['tagid']
    
        if 'email' in request.POST :
            email = request.POST['email']

        if 'product_code' in request.POST :
            product_code = request.POST['product_code']
        if 'product_price' in request.POST :
            product_price = request.POST['product_price']

        if 'product_code_tags' in request.POST :
            product_code_tags = request.POST['product_code_tags']
        if 'product_price_tags' in request.POST :
            product_price_tags = request.POST['product_price_tags']

        if 'num_tag_sheets' in request.POST :
            num_tag_sheets = request.POST['num_tag_sheets']

        if 'num_tags' in request.POST :
            num_tags = request.POST['num_tags']

        if 'promo_code' in request.POST :
            promo_code = request.POST['promo_code']

        if 'amount' in request.POST :
            amount = request.POST['amount']
            
        if 'invoice_number' in request.POST :
            invoice_number = request.POST['invoice_number']

    if num_tags is None or len(num_tags) == 0:
        num_tags = '0'

    store_fields = {}   

    store_fields['product_code'] = product_code
    store_fields['product_price'] = product_price

    store_fields['product_code_tags'] = product_code_tags
    store_fields['product_price_tags'] = product_price_tags

    store_fields['tag_code'] = tag_code
    store_fields['email'] = email
    store_fields['num_tag_sheets'] = num_tag_sheets
    store_fields['num_tags'] = num_tags
    store_fields['promo_code'] = promo_code
    store_fields['amount'] = amount

    store_fields['invoice_number'] = invoice_number
    
    return store_fields


def get_shipping_address_fields(request) :
    address1 = ''
    address2 = ''
    city = ''
    state = ''
    postal_code = ''
    country = ''
    phone = ''
    mobile = ''
    
    if request.method != 'POST' :
       return None
      
    if request.method == 'POST' :

        if not 's_address1' in request.POST :
           return None

        if 's_address1' in request.POST :
            address1 = request.POST['s_address1']
            if address1 == 'address 1' :
               address1 = ''
    
        if 's_address2' in request.POST :
            address2 = request.POST['s_address2']
            if address2 == 'address 2' :
               address2 = ''

        if 's_city' in request.POST :
            city = request.POST['s_city']
            if city == 'city' :
               city = ''

        if 's_state' in request.POST :
            state = request.POST['s_state']
            if state == 'state' :
               state = ''

        if 's_postal_code' in request.POST :
            postal_code = request.POST['s_postal_code']
            if postal_code == 'postal code' :
               postal_code = ''

        if 's_country' in request.POST :
            country = request.POST['s_country']
            if country == 'country' :
               country = ''

        if 's_phone' in request.POST :
            phone = request.POST['s_phone']
            if phone == 'phone number' :
               phone = ''
            
        if 's_mobile' in request.POST :
            mobile = request.POST['s_mobile']         
            if mobile == 'mobile number' :
               mobile = ''

    address_fields = {}   

    address_fields['address1'] = address1
    address_fields['address2'] = address2
    address_fields['city'] = city
    address_fields['state'] = state
    address_fields['postal_code'] = postal_code
    address_fields['country'] = country
    address_fields['phone'] = phone
    address_fields['mobile'] = mobile
    
    return address_fields

def get_billing_address_fields(request) :
    address1 = ''
    address2 = ''
    city = ''
    state = ''
    postal_code = ''
    country = ''
    phone = ''
    mobile = ''
    
    if request.method != 'POST' :
       return None
      
    if request.method == 'POST' :

        if 'b_address1' in request.POST :
            address1 = request.POST['b_address1']
            if address1 == 'address 1' :
               address1 = ''
    
        if 'b_address2' in request.POST :
            address2 = request.POST['b_address2']
            if address2 == 'address 2' :
               address2 = ''

        if 'b_city' in request.POST :
            city = request.POST['b_city']
            if city == 'city' :
               city = ''

        if 'b_state' in request.POST :
            state = request.POST['b_state']
            if state == 'state' :
               state = ''

        if 'b_postal_code' in request.POST :
            postal_code = request.POST['b_postal_code']
            if postal_code == 'postal code' :
               postal_code = ''

        if 'b_country' in request.POST :
            country = request.POST['b_country']
            if country == 'country' :
               country = ''

        if 'b_phone' in request.POST :
            phone = request.POST['b_phone']
            if phone == 'phone number' :
               phone = ''
            
        if 'b_mobile' in request.POST :
            mobile = request.POST['b_mobile']         
            if mobile == 'mobile number' :
               mobile = ''

    address_fields = {}   

    address_fields['address1'] = address1
    address_fields['address2'] = address2
    address_fields['city'] = city
    address_fields['state'] = state
    address_fields['postal_code'] = postal_code
    address_fields['country'] = country
    address_fields['phone'] = phone
    address_fields['mobile'] = mobile
    
    return address_fields



def verify_address(address_fields) :
   
   #if address_fields['address1'] is None or address_fields['address1'] == 'None' or len(address_fields['address1']) == 0 :
   #   return 'You must supply a street address'

   #if len(address_fields['city']) == 0 :
   #   return 'You must supply a city'

   #if len(address_fields['postal_code']) == 0 :
   #   return 'You must supply a postal code'   

   return None

def setup_shipping_fields(address_fields) :

   shipping_address_fields = {}

   if not address_fields is None:
      shipping_address_fields['s_address1'] = address_fields['address1']
      shipping_address_fields['s_address2'] = address_fields['address2']
      shipping_address_fields['s_city'] = address_fields['city']
      shipping_address_fields['s_state'] = address_fields['state']
      shipping_address_fields['s_postal_code'] = address_fields['postal_code']
      shipping_address_fields['s_country'] = address_fields['country']
      shipping_address_fields['s_phone'] = address_fields['phone']
      shipping_address_fields['s_mobile'] = address_fields['mobile']

   return shipping_address_fields

def setup_billing_fields(address_fields) :

   billing_address_fields = {}

   if not address_fields is None:
      billing_address_fields['b_address1'] = address_fields['address1']
      billing_address_fields['b_address2'] = address_fields['address2']
      billing_address_fields['b_city'] = address_fields['city']
      billing_address_fields['b_state'] = address_fields['state']
      billing_address_fields['b_postal_code'] = address_fields['postal_code']
      billing_address_fields['b_country'] = address_fields['country']
      billing_address_fields['b_phone'] = address_fields['phone']
      billing_address_fields['b_mobile'] = address_fields['mobile']

   return billing_address_fields


def get_central_store_fragment(request, page_name, store_fields, extra_fields, delete_fields) :
    return get_central_fragment(request, 'store', page_name, store_fields, extra_fields, delete_fields)




def process_payment(request, invoice_number, code) :
    process_fields = {}
    process_fields['process_success'] = False
    process_fields['process_error_message'] = ''

    payment_details = backoffice.get_payment_details(invoice_number)
    if payment_details is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'empty payment details'
       return process_fields

    # TEST if payment has already been processed
    # if so return False with an error message
    has_been_processed = backoffice.get_payment_processed(invoice_number)
    if not has_been_processed is None and has_been_processed :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'This payment has already been processed'
       return process_fields    

    user = get_user(payment_details['user_id'])
    if user is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'User %s does not exist' % (payment_details['user_id'])
       return process_fields

    current_profile = user.get_profile()
    if current_profile is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'Profile for user %s does not exist' % (payment_details['user_id'])
       return process_fields
    
      
    extended_payment_details = backoffice.get_last_extended_payment_details(invoice_number)
    if extended_payment_details is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'empty extended payment details'
       return process_fields

    if extended_payment_details['payment_status'] != 'Completed' and extended_payment_details['payment_status'] != 'Pending' :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'invalid payment status (%s)' % extended_payment_details['payment_status']
       return process_fields

    shipping_address_details = get_address_details(user.email, 1)

    shipping_country_code = shipping_address_details['country']
    shipping_state_code = shipping_address_details['state']

    shipping_country_name = get_country_name(shipping_country_code)
    if len(shipping_country_name) > 0:
       shipping_address_details['country'] = shipping_country_name

    shipping_state_name = get_state_name(shipping_country_code, shipping_state_code)
    if len(shipping_state_name) > 0:
       shipping_address_details['state'] = shipping_state_name
    
    billing_address_details = get_address_details(user.email, 2)

    billing_country_code = billing_address_details['country']
    billing_state_code = billing_address_details['state']

    billing_country_name = get_country_name(billing_country_code)
    if len(billing_country_name) > 0:
       billing_address_details['country'] = billing_country_name

    billing_state_name = get_state_name(billing_country_code, billing_state_code)
    if len(billing_state_name) > 0:
       billing_address_details['state'] = billing_state_name
    
    # 26052012 PROMO extensions to subscription period
    # 11062012 If we have got through to here then we have already checked that the PROMO is still valid
    # and not expired
    num_promo_days = 0
    is_tag_code = False
    if tag_exists(code) :
        tag = get_tag(code)
        tag_type = get_tag_type(tag)
        if tag_type.typename == 'PROMO' :
            num_promo_days = int(tag_type.typeclass)
            is_tag_code = True
    elif not code is None :
        product_details = backoffice.get_product_details(code)
        if not product_details is None :
            if product_details['product_type'] == 'PROMO' :
                num_promo_days = int(product_details['product_subscription_period'])    

    process_fields['user_id'] = payment_details['user_id']
    
    process_fields['process_num_days'] = num_promo_days

    order_number = payment_details['order_number']

    order_details = backoffice.get_order_details(order_number)
    if order_details is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'empty order details'
       return process_fields
      
    # this is a pass through variable indicating for this views module the main type of the product involved
    # it can have values SUBSCRIPTION_TAGS, RESUBSCRIPTION or TAGS
    main_product_type = order_details['main_product_type'] 
    process_fields['process_main_product_type'] = main_product_type

    # Use order details to process payment

    if main_product_type == "SUBSCRIPTION_TAGS" :
    # If this is a first time subscription then we do the following:
    #
    #    This shouldn't happen but
    #  - check the subscribed status and existence of subscription record
    #    if these exist we produce an error
    #
    #  - calculate the total subscription period which is the sum of the
    #    base subscription period associated with the subscription product
    #    plus any PROMO extension period
    #
    #  - create a subscription record associated with this user id
    #
    #  - update the profile for the current user id. Set the subscription period,
    #    expiry date, and set the subscribed status which is 1 (waiting for tags) if
    #    no tag has been provided otherwise is set to 2 (fully subscribed)
    #
    #  - update subscription record status to be the same as the current_profile.status
    #
    #  - Update order shipping status to status Ready to Ship (status code 1)
    #  
    #  - Send emails to customer about newly created accessible account
    #    and to admin about need to ship tags

       num_items = backoffice.get_num_order_line_items(order_number)
       if num_items is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'invalid line items list'
          return process_fields
       if num_items == 0 :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty line items list'
          return process_fields      
    
       main_product_details = backoffice.get_order_line_item(order_number, 0)
       if main_product_details is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty main product details'
          return process_fields
          
       process_fields['process_product_subscription_period'] = main_product_details['product_subscription_period']

       subscribed_status = get_subscribed_status(payment_details['user_id'])
       if not subscribed_status is None :
          process_fields['process_user_subscribed_status'] = subscribed_status
       else :
          process_fields['process_user_subscribed_status'] = ''

       subscription_details = backoffice.get_subscription_details(payment_details['user_id'])
       if subscription_details is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty subscription details for user %s' % (payment_details['user_id'])
          return process_fields

       # This test should also occur before payment is made before entering paypal
       if subscribed_status > backoffice.SUBSCRIBED_STATUS_NOT_SUBSCRIBED or subscription_details['subscription_exists'] :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'User %s is already subscribed with status %s' % (payment_details['user_id'], process_fields['process_user_subscribed_status'])
          return process_fields

       # This is the total of the subscription period plus any PROMO extension
       total_subscription_period = int(main_product_details['product_subscription_period']) + int(num_promo_days)
                    
       subscription_number = backoffice.create_subscription(payment_details['user_id'], payment_details['invoice_number'], total_subscription_period, autorenew=False)

       current_profile.expiry_date = datetime.datetime.now() + datetime.timedelta(days=total_subscription_period)
       current_profile.subscription_period = total_subscription_period

       if is_tag_code :
          current_profile.status = backoffice.SUBSCRIBED_STATUS_FULLY_SUBSCRIBED # user has a tag so is fully subscribed
       else :
          current_profile.status = backoffice.SUBSCRIBED_STATUS_WAITING # user subscribed but waiting for the tags

       current_profile.save()

       # update subscription record status to be the same as the current_profile.status
       annotate_text = None
       if num_promo_days > 0 :
          annotate_text = "%s" % (code)
          
       backoffice.update_subscription(payment_details['user_id'], None, None, None, current_profile.status, annotate_text=annotate_text)

       create_log_entry('PROCESS_PAYMENT', 'New subscription of period %d created for user %s with resulting status %d' % (total_subscription_period, payment_details['user_id'], current_profile.status))
          
       # set order shipping status to READY TO SHIP (status code 1)
       backoffice.update_order_shipping_status(order_number, backoffice.ORDER_SHIPPING_STATUS_READY)

       product_code = main_product_details['product_code']
       product_code_elements = product_code.split('_')
       if len(product_code_elements) > 0:
          process_fields['process_num_additional_tags'] = int(product_code_elements[len(product_code_elements)-1])
       else:
          process_fields['process_num_additional_tags'] = 0
          
       create_log_entry('PROCESS_PAYMENT_SHIPPING', 'Shipping status for order %s upgraded to status STATUS_READY' % order_number) 
       # send email to admin about shipping
       email_result = False
       (email_result) = mailers.mail_initial_purchase_to_admin(request, user, 'store', invoice_number, main_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)

       # send email to customer
       if current_profile.status == 2 :
          # send email that account is ready to use with tag id xxxxxxxx

          email_result = False
          (email_result) = mailers.mail_subscription_confirmation(request, user, 'store', invoice_number, main_product_details, num_promo_days, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details, tag_code=code)
          
       elif current_profile.status == 1 :
          # send email that tags will be assigned to user

          email_result = False
          (email_result) = mailers.mail_subscription_confirmation(request, user, 'store', invoice_number, main_product_details, num_promo_days, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)

       subscribed_status = get_subscribed_status(payment_details['user_id'])
       if not subscribed_status is None :
          process_fields['process_user_subscribed_status'] = subscribed_status
       else :
          process_fields['process_user_subscribed_status'] = ''

      
    elif main_product_type == "RESUBSCRIPTION" :
    # If this is a re-subscription then we do the following:
    #  - check the subscribed status and existence of subscription record
    #    if these don't exist we produce an error
    #    not sure what to do about subscribed status 
    #
    #  - calculate the subscription period which is just the 
    #    base subscription period associated with the re-subscription product
    #
    #  - update the subscription record associated with this user id
    #
    #  - update the profile for the current user id. Set the subscription period,
    #    expiry date, and set the subscribed status probably always to 2 (fully subscribed)
    #
    #  - update subscription record status to be the same as the current_profile.status
    #
    #  - Update order shipping status to status Ready to Ship (status code 1)
    #  
    #  - Send emails to customer about newly created accessible account
    #    and to admin about need to ship tags
    
       # 18072012 Fix - for some reason the order details are back to front
       # so the main order is at position 1 and the tags are at position 0
       # 30072012 undo this fix

       # but only do this if there actually are 2 product line items so first
       # test for the number of items

       num_items = backoffice.get_num_order_line_items(order_number)
       if num_items is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'invalid line items list'
          return process_fields
       if num_items == 0 :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty line items list'
          return process_fields

       if num_items > 1:
          main_product_details = backoffice.get_order_line_item(order_number, 0)
       else:
          main_product_details = backoffice.get_order_line_item(order_number, 0)
       if main_product_details is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty main product details'
          return process_fields
          
       process_fields['process_product_subscription_period'] = main_product_details['product_subscription_period']

       subscribed_status = get_subscribed_status(payment_details['user_id'])
       if not subscribed_status is None :
          process_fields['process_user_subscribed_status'] = subscribed_status
       else :
          process_fields['process_user_subscribed_status'] = ''

       subscription_details = backoffice.get_subscription_details(payment_details['user_id'])
       if subscription_details is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty subscription details for user %s' % (payment_details['user_id'])
          return process_fields

       # This test should also occur before payment is made before entering paypal
       if not subscription_details['subscription_exists'] :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'User %s is not already subscribed' % (payment_details['user_id'])
          return process_fields

       remaining_period = 0
       if datetime.datetime.now() < current_profile.expiry_date :
          remaining_delta = current_profile.expiry_date - datetime.datetime.now()
          remaining_period = remaining_delta.days + 1

       # This is the total of the subscription period plus any PROMO extension
       total_subscription_period = int(main_product_details['product_subscription_period']) + remaining_period
                    
       current_profile.expiry_date = datetime.datetime.now() + datetime.timedelta(days=total_subscription_period)
       current_profile.subscription_period = total_subscription_period

       current_profile.status = backoffice.SUBSCRIBED_STATUS_FULLY_SUBSCRIBED # user has a tag so reset as fully subscribed

       current_profile.save()

       # update subscription record status to be the same as the current_profile.status
       # update the invoice number for the subscription if it hasn't already been set (as in the case of COMPLIMENTARY tags)
       # update the product code for resubscription
       backoffice.update_subscription(payment_details['user_id'], payment_details['invoice_number'], main_product_details['product_code'], main_product_details['product_subscription_period'], current_profile.status)

       create_log_entry('PROCESS_PAYMENT', 'Resubscription of period %d created for user %s with resulting status %d' % (int(main_product_details['product_subscription_period']), payment_details['user_id'], current_profile.status))
          
       # set order shipping status to READY TO SHIP (status code 1) if there is a subsidiary TAGS order line item
       # 18072012 Fix - for some reason the order details are back to front
       # so the main order is at position 1 and the tags are at position 0
       # 30072012 undo this fix
       if num_items > 1:
          tags_product_details = backoffice.get_order_line_item(order_number, 1)
       else:
          tags_product_details = None
       if not tags_product_details is None and tags_product_details['product_type'] == 'TAGS' :
          backoffice.update_order_shipping_status(order_number, backoffice.ORDER_SHIPPING_STATUS_READY)

          product_code = tags_product_details['product_code']
          product_code_elements = product_code.split('_')
          if len(product_code_elements) > 0:
             process_fields['process_num_additional_tags'] = int(product_code_elements[len(product_code_elements)-1])
          else:
             process_fields['process_num_additional_tags'] = 0

          create_log_entry('PROCESS_PAYMENT_SHIPPING', 'Shipping status for order %s upgraded to status STATUS_READY' % order_number)
          
          # send email to admin about shipping
          email_result = False
          (email_result) = mailers.mail_tags_purchase_to_admin(request, user, 'store', invoice_number, tags_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)

          # send email to customer about tags
          email_result = False
          (email_result) = mailers.mail_tags_purchase_confirmation(request, user, 'store', invoice_number, tags_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)

       # send email to customer about resubscription
       email_result = False
       (email_result) = mailers.mail_resubscription_confirmation(request, user, 'store', invoice_number, main_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)
       

       subscribed_status = get_subscribed_status(payment_details['user_id'])
       if not subscribed_status is None :
          process_fields['process_user_subscribed_status'] = subscribed_status
       else :
          process_fields['process_user_subscribed_status'] = ''
       
       
    elif main_product_type == "TAGS" :
    # If this is a purchase of tags only then we do the following:
    #
    # - If the product main item is of type tags update the order shipping status
    #
    # - Send an email to admin about shipping
    #
    # - Send a response email to the customer

       num_items = backoffice.get_num_order_line_items(order_number)
       if num_items is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'invalid line items list'
          return process_fields
       if num_items == 0 :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty line items list'
          return process_fields

       main_product_details = backoffice.get_order_line_item(order_number, 0)
       if main_product_details is None :
          process_fields['process_success'] = False
          process_fields['process_error_message'] = 'empty main product details'
          return process_fields

       process_fields['process_product_type'] = main_product_details['product_type']
    
       if main_product_details['product_type'] == 'TAGS' :
          backoffice.update_order_shipping_status(order_number, backoffice.ORDER_SHIPPING_STATUS_READY)

          product_code = main_product_details['product_code']
          product_code_elements = product_code.split('_')
          if len(product_code_elements) > 0:
             process_fields['process_num_additional_tags'] = int(product_code_elements[len(product_code_elements)-1])
          else:
             process_fields['process_num_additional_tags'] = 0


          create_log_entry('PROCESS_PAYMENT_SHIPPING', 'Shipping status for order %s upgraded to status STATUS_READY' % order_number) 

          # send email to admin about tags
          email_result = False
          (email_result) = mailers.mail_tags_purchase_to_admin(request, user, 'store', invoice_number, main_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)

          # send email to customer about tags
          email_result = False
          (email_result) = mailers.mail_tags_purchase_confirmation(request, user, 'store', invoice_number, main_product_details, process_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details)
          

    else :          
      process_fields['process_success'] = False
      process_fields['process_error_message'] = 'Invalid main product type'
      return process_fields

    backoffice.set_payment_processed(invoice_number)
    
    process_fields['process_success'] = True
    process_fields['process_error_message'] = ''

    return process_fields


def process_order(store_fields, user, logged_in) :

   process_fields = {}
   process_fields['process_success'] = False
   process_fields['process_error_message'] = ''

   if store_fields is None :
      process_fields['process_success'] = False
      process_fields['process_error_message'] = 'Empty store fields'
      return process_fields
      
   if user is None :
      process_fields['process_success'] = False
      process_fields['process_error_message'] = 'User not defined'
      return process_fields

   if not logged_in :

     # First check that the user does not already have a subscription
     subscribed_status = get_subscribed_status(user.email)
     if not subscribed_status is None :
       process_fields['process_user_subscribed_status'] = subscribed_status
     else :
       process_fields['process_user_subscribed_status'] = ''

     subscription_details = backoffice.get_subscription_details(user.email)
     if subscription_details is None :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'empty subscription details for user %s' % (user.email)
       return process_fields

     # This test should also occur after payment is made
     if subscribed_status > 0 or subscription_details['subscription_exists'] :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'User %s is already subscribed with status %s' % (user.email, process_fields['process_user_subscribed_status'])
       return process_fields
     
     product_code = store_fields['product_code']
     #if product_code == 'more_tags' : product_code = ''

     # If nothing has been selected return to the MyRocketTags front screen
     if len(product_code) == 0 :
       process_fields['process_success'] = False
       process_fields['process_error_message'] = 'Empty product code'
       return process_fields
      
     product_name = ''
     product_price = '0.00'
     product_details = backoffice.get_product_details(product_code)
     if not product_details is None :
        product_name = product_details['product_name']
        if 'product_price' in product_details :
           product_price = product_details['product_price']              

     # 22062012 Now create a single product order for this purchase
     order_number = backoffice.create_single_product_order(user.email, store_fields['amount'], product_code, main_product_type="SUBSCRIPTION_TAGS")

     create_log_entry('PROCESS_ORDER', 'Order %s created for user %s for product_code %s of type SUBSCRIPTION_TAGS' % (order_number, user.email, product_code)) 

     process_fields['process_success'] = True
     process_fields['process_product_code'] = product_code
     process_fields['process_product_code_tags'] = None
     process_fields['process_product_name'] = product_name
     process_fields['process_product_price'] = product_price
     process_fields['process_order_number'] = order_number

   else :

     #name = request.POST['name']

     product_code = store_fields['product_code']
     product_code_tags = store_fields['product_code_tags']

     # If nothing has been selected return to the MyRocketTags front screen
     if len(product_code) == 0 and len(product_code_tags) == 0 :
        process_fields['process_success'] = False
        process_fields['process_error_message'] = 'Empty product codes'
        return process_fields

     product_name = ''
     product_price = '0.00'
     if len(product_code) > 0 :
        product_details = backoffice.get_product_details(product_code)
        if not product_details is None :
           product_name = product_details['product_name']
           if 'product_price' in product_details :
              product_price = product_details['product_price']              

     product_name_tags = ''
     product_price_tags = '0.00'
     if len(product_code_tags) > 0 :
        product_details_tags = backoffice.get_product_details(product_code_tags)
        if not product_details_tags is None :
           product_name_tags = product_details_tags['product_name']
           if 'product_price' in product_details_tags :
              product_price_tags = product_details_tags['product_price']              

     if len(product_code) > 0 and len(product_code_tags) == 0 :
        product_name = '%s' % (product_name)
        product_price = float(product_price)

        # 25062012 Now create a single product order for this purchase
        amount = store_fields['amount']

        order_number = backoffice.create_single_product_order(user.email, amount, product_code, main_product_type="RESUBSCRIPTION")

        create_log_entry('PROCESS_ORDER', 'Order %s created for user %s for product_code %s of type RESUBSCRIPTION' % (order_number, user.email, product_code)) 

     elif len(product_code) == 0 and len(product_code_tags) > 0 :
        product_name = '%s' % (product_name_tags)
        product_price = float(product_price_tags)

        # 25062012 Now create a single product order for this purchase
        amount = store_fields['amount']

        order_number = backoffice.create_single_product_order(user.email, amount, product_code_tags, main_product_type="TAGS")

        create_log_entry('PROCESS_ORDER', 'Order %s created for user %s for product_code %s of type TAGS' % (order_number, user.email, product_code_tags))
        
     else :
        #product_name = '%s' % (product_name)
        product_name = '%s + %s' % (product_name, product_name_tags)
        product_price = float(product_price) + float(product_price_tags)

        # 25062012 Now create a double product order for this purchase
        amount = store_fields['amount']
     
        order_number = backoffice.create_multi_product_order(user.email, [amount, 0], [product_code, product_code_tags], main_product_type="RESUBSCRIPTION")

        create_log_entry('PROCESS_ORDER', 'Order %s created for user %s for product_code %s and product_code_tags %s of type RESUBSCRIPTION' % (order_number, user.email, product_code, product_code_tags)) 

     process_fields['process_success'] = True
     process_fields['process_product_code'] = product_code
     process_fields['process_product_code_tags'] = product_code_tags
     process_fields['process_product_name'] = product_name
     process_fields['process_product_price'] = product_price
     process_fields['process_order_number'] = order_number

   return process_fields


    
def index(request, page_name='front'):
 
    logged_in = False
    if request.user.is_authenticated() and not request.user.is_anonymous() :
        logged_in = True
    else :
        logged_in = False

    if settings.STORE_MAINTENANCE_MODE and not logged_in:
        return HttpResponseRedirect(links['maintenance_front'])

    tag = None
    tag_code = None
    email = None
    user = None

    store_fields = get_store_fields(request)

    tag_code = store_fields['tag_code']
    email = store_fields['email']
    promo_code = store_fields['promo_code']

    # 02062013 From now on the front page is bypassed
    if page_name == 'front':
       if not logged_in:
          if not tag_code is None and len(tag_code) > 0:
             return HttpResponseRedirect("%s?id=%s" % (links['process_store_registration'], tag_code))
          elif not promo_code is None and len(promo_code) > 0:
             return HttpResponseRedirect("%s?promo=%s" % (links['process_store_registration'], promo_code))
          else:
             return HttpResponseRedirect(links['process_store_registration'])

         
    #name = request.POST['name']


    my_codes = {}
    extra_fields = {}
    #extra_fields.update({ 'promo_code' : promo_code, })

    if page_name == 'payment' :
       request.META['email'] = email
       #name = request.POST['name']

    if logged_in :
        user = request.user
    elif not email is None and len(email) > 0 :
        user = get_user(email)
    else :
        user = request.user

    if not tag_code is None :
        tag = get_tag(tag_code)

    if not tag is None :
        extra_fields.update({'tagid' : tag.to_base36(),})   

    if page_name == 'tags' :
        # 25052012 If a user is registered and logged in and chooses any option
        # other than order more tags then go straight to payment
        # otherwise if a new user just registered go to order more tags

        request.META['details'] = store_fields
        #name = request.POST['name']
        
        if logged_in :
            product_code = store_fields['product_code']
            if product_code != 'more_tags' :
                page_name = 'payment'

    if page_name == 'payment' :

        request.META['details'] = store_fields

        verify_result = None
        shipping_address_fields = get_shipping_address_fields(request)
        billing_address_fields = get_billing_address_fields(request)

        if shipping_address_fields is None :
           verify_result = None
        else :
           verify_result = verify_address(shipping_address_fields)
           if verify_result is None and len(billing_address_fields['address1']) > 0 :
              verify_result = verify_address(billing_address_fields)
           
        if verify_result is None :
           
           if not shipping_address_fields is None :
              add_address_details(user.email, shipping_address_fields, 1) # 1 for SHIPPING
              if len(billing_address_fields['address1']) > 0 :
                 add_address_details(user.email, billing_address_fields, 2) # 2 for BILLING
              else :
                 add_address_details(user.email, shipping_address_fields, 2) # 2 for BILLING

           process_fields = process_order(store_fields, user, logged_in)
           if not process_fields['process_success'] :
              create_log_entry('PROCESS_ORDER_ERROR', process_fields['process_error_message'])
           

           request.META['process_fields'] = process_fields
           #name = request.POST['name']
           
           if not process_fields['process_success'] and len(process_fields['process_error_message']) == 0 :
              return HttpResponseRedirect(links['mytags_front'])

           if process_fields['process_success'] :

              extra_fields.update({'product_code' : process_fields['process_product_code'], })
              extra_fields.update({'product_code_tags' : process_fields['process_product_code_tags'], })
              
              extra_fields.update({'product_name' : process_fields['process_product_name'],
                                'product_price' : process_fields['process_product_price'],
                                })
              
              # 22062012 Next create an invoice
              invoice_number = backoffice.create_payment(process_fields['process_order_number'])

              # 22062012 Finally use this invoice to redirect the user into PayPal
              paypal_button = backoffice.generate_paypal_button(invoice_number, store_fields)
             
              extra_fields.update({'invoice_number' : invoice_number, 'paypal_button' : paypal_button, })

              if int(store_fields['num_tags']) < 6 :
                 extra_fields.update({'num_tags' : 0,})

           else :
              extra_fields.update({'error_message' : process_fields['process_error_message'], })
        else :
           extra_fields.update({'errors' : verify_result, })
           if not shipping_address_fields is None :
              extra_fields.update(setup_shipping_fields(shipping_address_fields))
              extra_fields.update(setup_billing_fields(billing_address_fields))
           page_name = 'address_details'

                   
    elif page_name == 'products' :
        # 14052012 If a tag is provided and is COMPLIMENTARY bypass the store
        # and go straight to the MyRocketTags site
        tag_type = None
        if not tag is None :
            tag_type = get_tag_type(tag)
        if not tag_type is None and tag_type.typename == 'COMPLIMENTARY' :
            if logged_in :
                return HttpResponseRedirect(links['mytags_front'])
    elif page_name == 'front' : # 25052012 if logged in and subscribed go straight to the products page
        if logged_in :
            page_name = 'products'
            #next_link = page_codes['store_registration']['next']
            #extra_fields.update({'next' : next_link, })
    elif page_name == 'set_tc_accept' :
        invoice_number = store_fields['invoice_number']
        payment_details = backoffice.get_payment_details(invoice_number)
        if not payment_details is None :
           result = backoffice.set_payment_tc_accept(invoice_number)
           if result :
               return HttpResponse("YES")
           else :
               return HttpResponse("Failure setting tc accept field")
        else :
           return HttpResponse("Invalid invoice number")

    extra_fields.update({'logged_in' : logged_in,})

    #if not email is None :
    extra_fields.update({'email' : email, })

    if page_name == 'products' :
      
        (product_codes, product_prices) = backoffice.get_products(["SUBSCRIPTION_TAGS", "RESUBSCRIPTION", "TAGS"])
        extra_fields.update({'product_codes' : product_codes, 'product_prices' : product_prices, })

        if logged_in :
           page_name = "registered_products"
        else :
           page_name = "products"
        
    elif page_name == 'tags' :
        product_code = store_fields['product_code']
        extra_fields.update({'tagid' : tag_code, 'product_code' : product_code, })

    elif page_name == 'address_details' :
        countries = get_countries()
        states = get_states('AU')
        extra_fields.update({'s_countries' : countries, 's_states' : states, 'b_countries' : countries, 'b_states' : states, })

    elif page_name == 'front' :

        (product_codes, product_prices) = backoffice.get_products(["SUBSCRIPTION_TAGS", "RESUBSCRIPTION", "TAGS"])
        extra_fields.update({'product_codes' : product_codes, 'product_prices' : product_prices, })

         
         

    # 31052012 If user is logged in use MyRocketTags bottom boxes
    # Otherwise use general bottom boxes
    if logged_in :
        t = loader.get_template('mytags/template.html')
    else :
        t = loader.get_template('store/template.html')
        
    (central_store_fragment, full_page_name) = get_central_store_fragment(request, page_name, store_fields, extra_fields, [])
    my_codes = {"request" : request,
         'main_top_menu_fragment' : get_main_top_menu(request, tab2=''),
         'central_fragment' : central_store_fragment,
         'page_name' : full_page_name,
        }
    my_codes.update(get_bottom_items(request))

    
            
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


