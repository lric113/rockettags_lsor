"""
Django settings for rockettags project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#p50018t9ckl%1nxyo!nemu1r)qgi*3%(@k1gke_4$z#zj(03s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'transition',
    'captcha',
    'import_export',
    'sheetapp',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'rockettags',
        'USER': 'postgres',
        'PASSWORD': 'viscsql',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(PROJECT_ROOT, STATIC_URL.strip("/"))

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

#TEMPLATE_DIRS = (os.path.join(PROJECT_ROOT, "templates"),)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_ROOT, "templates")
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'transition.contextprocessors.global_settings',
            ],
        },
    },
]

# CUSTOM settings
MAIN_INSECURE_HOST = 'http://ixia:8000'
MAIN_SECURE_HOST = 'http://ixia:8000'
BLIND_COPY_EMAIL = 'blindcopy@rockettags.com'
CSRF_FAILURE_VIEW = 'transition.views.csrf_failure'
FORCE_SCRIPT_NAME = ''
SITE_LOG = os.path.join(PROJECT_ROOT, 'site.log')
MAILER_HOST = 'www.rockettags.com'
WWW_ROOT = PROJECT_ROOT
MAILGUN_MESSAGES_URL = 'https://api.mailgun.net/v2/rockettags.com/messages'
MAILGUN_API_KEY = 'key-7kzzopnuy0-tgknbp0k1b5zww48ujdd0'
ACCOUNTS_MAIL_FROM = 'mailer@rockettags.com'
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
CAPTCHA_LETTER_ROTATION = None
CAPTCHA_NOISE_FUNCTIONS = None
CAPTCHA_FONT_SIZE = 40
CAPTCHA_OUTPUT_FORMAT = u'%(hidden_field)s %(image)s<br>%(text_field)s'
CAPTCHA_FOREGROUND_COLOR = '#ffffff'
CAPTCHA_BACKGROUND_COLOR = '#f37528'
ADMINS = (
    ('Administrator', 'admin@rockettags.com'),
)
MANAGERS = ADMINS

FIXTURE_DIRS = (
    os.path.join(PROJECT_ROOT, 'fixtures'),
)



