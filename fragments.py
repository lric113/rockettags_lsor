# Handlers for page fragments plus other general utilities/helper functions

from django.template import Context, loader, RequestContext
from rockettags.links import links
from rockettags.lists import page_codes
from rockettags.lists import bottom_items
from rockettags.lists import bottom_item_links
from rockettags.lists import page_names
from django.conf import settings
import sys
import requests
from webob.multidict import MultiDict
   
def get_fragment(request, path, name) :
    t = loader.get_template('%s/fragment_%s.html' % (path, name))
    c = RequestContext(request, {'request' : request})
    return t.render(c)

def get_fragment_with_link(request, path, name) :
    codes = {}
    codes['%s_link' % name] = links[name]
    return get_fragment_with_codes(request, path, 'linked', name, codes)

def get_fragment_with_links(request, path, name, link_names) :
    codes = {}
    for link_name in link_names :        
        codes['%s_link' % link_name] = links[link_name]
    return get_fragment_with_codes(request, path, 'linked', name, codes)

def get_fragment_with_codes(request, path, prefix, name, codes) :
    t = loader.get_template('%s/%s_fragment_%s.html' % (path, prefix, name))
    codes.update({'request' : request})
    c = RequestContext(request, codes)
    return t.render(c)

def get_codes(name) :
    return page_codes[name].copy()

def query_string(request, d, excludes) :
    if d is None :
        return ''
    output_string = '?'
    for thekey in d :
        if excludes is None or not thekey in excludes :
            output_string = '%s%s=%s&' % (output_string, thekey, d[thekey])
    output_string = output_string[:-1]
    return output_string

def post_message(responder, responder_text, receiver, subject, text, html, inline_image):

    WEB_IMAGES_DIR = '%s/html/static/img' % settings.WWW_ROOT
    if sys.platform.startswith('linux'):
        WEB_IMAGES_DIR = '%s/html/static/img' % settings.WWW_ROOT
    elif sys.platform.startswith('win'):
        WEB_IMAGES_DIR = '%s/html/static/img' % settings.WWW_ROOT

    bcc = 'blindcopy@rockettags.com'
    
    r = requests.\
        post(settings.MAILGUN_MESSAGES_URL,
             auth=("api", settings.MAILGUN_API_KEY),
             files=MultiDict([("inline", open("%s/%s" % (WEB_IMAGES_DIR, inline_image), 'rb'))]),
             data={
                 "from": "%s <%s>" % (responder_text, responder),
                 "to": [receiver,],
                 "bcc" : [bcc,],
                 "subject": subject,
                 "text": text,
                 "html": html,
                 }
             )
    return r

def get_value(dictionary, name) :

    if dictionary is None : return None
    if name in dictionary :
        return dictionary[name]
    else :
        return None

# Search GET and POST for a parameter value
# if precedence order is POST then POST takes precedence if parameter is in both GET and POST
# else GET takes precedence if GET exists
def get_parameter_from_session(request, parameter_name, order="POST"):

    value = None
    if parameter_name in request.GET: value = request.GET[parameter_name]
    if not value is None and len(value) == 0: value = None

    if order == "GET":
        if not value is None:
            return value

    if request.method == 'POST':
        valuetemp = None
        if parameter_name in request.POST: valuetemp = request.POST[parameter_name]
        if not valuetemp is None and len(valuetemp) == 0: valuetemp = None
        if not valuetemp is None: value = valuetemp
    
    return value
    

