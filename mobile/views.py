from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from django.contrib import auth
from django.shortcuts import render_to_response
from django.conf import settings
import re
from rockettags.tags.forms import CaptchaForm
from rockettags.main.dropdowns import get_countries, get_states, get_cities
from rockettags.tags.models import get_tag, tag_exists, FoundMessage
from rockettags.main.models import create_log_entry, test_if_subscribed, is_subscribed, get_user, register_user
from rockettags.main import mailers
from rockettags.tags.find_ancillary import get_item_types, get_contact_methods

import random

from rockettags.main.register_view import validate_registration_input, get_registration_fields
from rockettags.main.models import register_user, check_duplicate_username, check_duplicate_email, get_user, update_subscription
from rockettags.main.register_view import get_registration_fields, validate_email

from django.views.decorators.csrf import csrf_exempt

from rockettags.main.redeem_view import validate_code
from rockettags.tags.models import register_tag

from rockettags.fragments import get_parameter_from_session


REGISTRATION_PAGE_NAMES = {}
REGISTRATION_PAGE_NAMES["1"] = "Front Registration Page"
REGISTRATION_PAGE_NAMES["2"] = "Confirm Email"
REGISTRATION_PAGE_NAMES["3"] = "Successfully Registered"
REGISTRATION_PAGE_NAMES["4"] = "Add to Subscription"
REGISTRATION_PAGE_NAMES["5"] = "Confirm tag added to account"
REGISTRATION_PAGE_NAMES["6"] = "Tell a Friend"
REGISTRATION_PAGE_NAMES["7"] = "Tell a Friend - Done"

FIND_A_TAG_PAGE_NAMES = {}
FIND_A_TAG_PAGE_NAMES["1"] = "Front Found a Tag Page"
FIND_A_TAG_PAGE_NAMES["2"] = "Give Finder Email"
FIND_A_TAG_PAGE_NAMES["3"] = "Successfully Found"
FIND_A_TAG_PAGE_NAMES["4"] = "Successfully Tested"
FIND_A_TAG_PAGE_NAMES["5"] = "Filler"
FIND_A_TAG_PAGE_NAMES["6"] = "Tell a Friend"
FIND_A_TAG_PAGE_NAMES["7"] = "Tell a Friend - Done"

TITLE_COLORS = {}
TITLE_COLORS["1"] = "#FF6600"
TITLE_COLORS["2"] = "#FF6600"
TITLE_COLORS["3"] = "#FF6600"
TITLE_COLORS["4"] = "#FF6600"
TITLE_COLORS["5"] = "#FF6600"
TITLE_COLORS["6"] = "#FF6600"
TITLE_COLORS["7"] = "#FF6600"


def generate_captcha(maxint) :

    first = random.randint(1, maxint)
    operator = '+' #random.choice(['+','*'])
    second = random.randint(1, maxint)

    label = '%d %s %d = ' % (first, operator, second)

    answer = 0
    if operator == '+' :
        answer = first + second
    elif operator == '*' :
        answer = first * second

    return (label, answer)
    

def get_mobile_registration_fields(request, order="POST"):

    fields = {}

    fields['tag_id'] = get_parameter_from_session(request, 'tag_id', order)
    if fields['tag_id'] is None:
        fields['tag_id'] = get_parameter_from_session(request, 'id', order)
    fields['email'] = get_parameter_from_session(request, 'email', order)    
    fields['email2'] = get_parameter_from_session(request, 'email2', order)

    # 30052013 Treat email as case insensitive
    if not fields['email'] is None: fields['email'] = fields['email'].lower()
    if not fields['email2'] is None: fields['email2'] = fields['email2'].lower()
    
    fields['accepted_tcs'] = get_parameter_from_session(request, 'accepted_tcs', order)

    fields['entered_friends'] = get_parameter_from_session(request, 'entered_friends', order)
    
    fields['friend1'] = get_parameter_from_session(request, 'friend1', order)
    fields['friend2'] = get_parameter_from_session(request, 'friend2', order)
    fields['friend3'] = get_parameter_from_session(request, 'friend3', order)
    

    return fields


@csrf_exempt
def mobile_register(request, page_num="1") :

    mobile_fields = get_mobile_registration_fields(request, order="POST")

    tag_id = mobile_fields['tag_id']
    if not tag_id is None :
        if not tag_exists(tag_id) :
            tag_id = None
            page_num = "1"
            title_color = TITLE_COLORS[page_num]
            error = "Invalid Tag ID - Please Check and Enter Again."
            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

    email = mobile_fields['email']
    email2 = mobile_fields['email2']
    accepted_tcs = mobile_fields['accepted_tcs']

    create_log_entry('MOBILE REGISTER', 'Entered mobile registration for user %s with tag %s in mode %s' % (str(email), str(tag_id), request.method))

    error = "" 
    title_color = TITLE_COLORS[page_num]              

    if request.method == 'POST' :

        reg_fields = get_registration_fields(request)                

        response_type = None
        (validated, code, details, validate_code_fields, dummy) = validate_code(request, tag_id)
        if validate_code_fields is None: validate_code_fields = {}

        if not validated is None and validated is True:
            response_type = None
            if 'response_type' in details: response_type = details['response_type']

        if page_num == "2":

            if response_type is None or response_type != 1:
                if not response_type is None and response_type == 2:
                    #error = "This RocketTag I.D. is already registered. Please enter another valid RocketTag I.D."
                    page_num = "1"
                    title_color = TITLE_COLORS[page_num]
                    return render_to_response('mobile/found_a_tag.html',locals(), context_instance=RequestContext(request))
                else:
                    error = "This RocketTag I.D. is not available. Please enter a valid RocketTag I.D."
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                
            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "3":
            
            if email is None or email2 is None:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = "The email address field is empty!"
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

            if len(email) == 0 or len(email2) == 0:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = "The email address field is empty!"
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

            (result, error_string) = validate_email(request, email)
            if not result:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = error_string
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))
                
            if not email is None and email != email2:
                page_num = "2"
                title_color = TITLE_COLORS[page_num]
                error = "Your email address fields do not match!"
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

            user = get_user(email)
            if user is None:
                page_num = "3"
                title_color = TITLE_COLORS[page_num]
                really_here = "YES"

                # 24042012 We bypass activation for now. Users are automatically created activated and move onto the products page
                # 14052012 Users are activated but not necessarily subscribed and only logged in if the tag type is COMPLIMENTARY
                (new_user, error_string) = register_user(request, reg_fields, {}, activate_user=True, tag_code=tag_id)
                error = error_string

                num_subscription_days = 0
                if not details is None:
                    if 'tag_typeclass' in details:
                        num_subscription_days = int(details['tag_typeclass'])
                        
                mailers.mail_standardplus_confirmation(request, new_user, 'mobile', num_subscription_days, None, tag_code=tag_id)
                
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))
            else:
                page_num = "4"
                title_color = TITLE_COLORS[page_num]
                really_here = "NO"
                return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))
                
            page_num = "2"
            title_color = TITLE_COLORS[page_num]
            error = "Some other error!"
            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "5":

            if not response_type is None and response_type == 1:
        
                if not email is None:
                    user = get_user(email)
                    if not user is None:
                        success = register_tag(code, user)

                        if success:
                            mailers.mail_add_a_tag(request, user, 'mobile', None, tag_code=code)
                    else:
                        success = False
                        
                details['register_tag_success'] = success
            else:
                details['register_tag_success'] = False
                page_num = "4"
                title_color = TITLE_COLORS[page_num]        
                tag_id = None
                error = "This RocketTag I.D. is already registered!"
                    
            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "6":

            if not mobile_fields['entered_friends'] is None and mobile_fields['entered_friends'] == "true":

                friend1 = mobile_fields['friend1']
                friend2 = mobile_fields['friend2']
                friend3 = mobile_fields['friend3']

                if friend1 is None and friend2 is None and friend3 is None:

                    error = "You didn't enter any friends!"

                elif not email is None:

                    signor = get_user(email)
                    if not signor is None:

                        if not friend1 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend1, 'mobile', None, tag_code=tag_id)
                        if not friend2 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend2, 'mobile', None, tag_code=tag_id)
                        if not friend3 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend3, 'mobile', None, tag_code=tag_id)

                        mailers.mail_tellafriend_signor(request, signor, 'mobile', settings.TELL_A_FRIEND_EXTENSION, None, tag_code=tag_id)

                        page_num = "7"
                        
                    else:
                        error = "signor email is invalid!"

                else:
                    error = "Empty signor email!"
            
            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "7":

            return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))

        
    return render_to_response('mobile/register_a_tag.html', locals(), context_instance=RequestContext(request))


def get_mobile_found_fields(request, order="POST"):

    fields = {}

    fields['tag_id'] = get_parameter_from_session(request, 'tag_id', order)
    if fields['tag_id'] is None:
        fields['tag_id'] = get_parameter_from_session(request, 'id', order)
    fields['email'] = get_parameter_from_session(request, 'email', order)

    fields['test'] = get_parameter_from_session(request, 'test', order)

    fields['entered_friends'] = get_parameter_from_session(request, 'entered_friends', order)
    
    fields['friend1'] = get_parameter_from_session(request, 'friend1', order)
    fields['friend2'] = get_parameter_from_session(request, 'friend2', order)
    fields['friend3'] = get_parameter_from_session(request, 'friend3', order)
    

    return fields


@csrf_exempt
def mobile_find(request, page_num="1") :

    mobile_fields = get_mobile_found_fields(request, order="POST")

    tag_id = mobile_fields['tag_id']
    email = mobile_fields['email']
    test = mobile_fields['test']
    
    title_color = TITLE_COLORS[page_num]              

    # Works either in POST mode or in GET mode but only if tag_id and test fields have been provided
    if request.method == 'POST' or (not tag_id is None and not test is None) :

        response_type = None
        (validated, code, details, validate_code_fields, dummy) = validate_code(request, tag_id)
        if validate_code_fields is None: validate_code_fields = {}

        if not validated is None and validated is True:
            response_type = None
            if 'response_type' in details: response_type = details['response_type']

        if page_num == "2":

            if response_type is None or response_type != 2:
                if not response_type is None and response_type == 1:
                    page_num = "1"
                    title_color = TITLE_COLORS[page_num]
                    return render_to_response('mobile/register_a_tag.html',locals(), context_instance=RequestContext(request))
                else:
                    error = "This RocketTag I.D. is not available. Please enter a valid RocketTag I.D."
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))
            else:
                if not test is None and test == "true":                    
                    (result, owner_email, errormsg) = do_test(request, tag_id, None)
                    if not result:
                        page_num = "1"
                        title_color = TITLE_COLORS[page_num]
                        error = errormsg
                        
                        return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))
                    else:

                        if email is None:
                            email = owner_email
                        
                        page_num = "4"
                        title_color = TITLE_COLORS[page_num]
                        return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))
                else:
                    return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "3":
            
            if email is None:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = "The email address field is empty!"
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

            if len(email) == 0:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = "The email address field is empty!"
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

            (result, error_string) = validate_email(request, email)
            if not result:
                page_num = "1"
                title_color = TITLE_COLORS[page_num]
                error = error_string
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

            (result, errormsg) = add_message(request, tag_id, email, { 'email' : email })
            if not result:
                page_num = "2"
                title_color = TITLE_COLORS[page_num]
                error = errormsg
               
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))
            else:
                
                return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))
                
            page_num = "2"
            title_color = TITLE_COLORS[page_num]
            error = "Some other error!"
            return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "6":

            if not mobile_fields['entered_friends'] is None and mobile_fields['entered_friends'] == "true":

                friend1 = mobile_fields['friend1']
                friend2 = mobile_fields['friend2']
                friend3 = mobile_fields['friend3']

                if friend1 is None and friend2 is None and friend3 is None:

                    error = "You didn't enter any friends!"

                elif not email is None:

                    signor = get_user(email)
                    if not signor is None:

                        if not friend1 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend1, 'mobile', None, tag_code=tag_id)
                        if not friend2 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend2, 'mobile', None, tag_code=tag_id)
                        if not friend3 is None:
                            result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                            if not result is None:
                                mailers.mail_tellafriend_signee(request, signor, friend3, 'mobile', None, tag_code=tag_id)

                        mailers.mail_tellafriend_signor(request, signor, 'mobile', settings.TELL_A_FRIEND_EXTENSION, None, tag_code=tag_id)

                        page_num = "7"
                        
                    else:
                        error = "signor email is invalid!"

                else:
                    error = "Empty signor email!"
            
            return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

        elif page_num == "7":

            return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))

    return render_to_response('mobile/found_a_tag.html', locals(), context_instance=RequestContext(request))




@csrf_exempt
def mobile_find_old(request, code=None) :

    tag_id = code
    if tag_id is None:
        if 'id' in request.GET :
            tag_id = request.GET['id']
        elif 'tag_id' in request.GET :
            tag_id = request.GET['tag_id']

    create_log_entry('MOBILE TAG FIND', 'Entered tag find for tag %s in mode %s' % (str(tag_id), request.method))

    if not tag_id is None :
        if not tag_exists(tag_id) :
            tag_id = None
            errormsg = "Invalid Tag ID"
            return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

    email = None
    if "email" in request.GET :
        email = request.GET['email']

    human = False

    (captcha_label, captcha_answer) = generate_captcha(10)
    
    if request.method == 'POST' :

        if tag_id is None :
            if 'tag_id' in request.POST :
                tag_id = request.POST['tag_id']

        if email is None :
            if 'email' in request.POST :
                email = request.POST['email']

        test = None
        if 'test' in request.POST :
            test = request.POST['test']
        if test is None :
            test = 'False'
        if test == 'True' : test = True
        else : test = False

        if tag_id is None or not tag_exists(tag_id) or email is None :
            tag_id = None
            errormsg = "No tag ID or email or invalid tag id entered"
            return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

        if 'captcha' in request.POST and 'captcha_true_value' in request.POST :

            human = False
            given_value = int(request.POST['captcha'])
            true_value = int(request.POST['captcha_true_value'])

            if given_value == true_value :
                human = True

            if human :

                tag = get_tag(tag_id)
                if tag is None :
                    errormsg = "Invalid Tag ID"
                    return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
                
                user = tag.user
                if user is None :
                    errormsg = "This tag is not currently registered"
                    return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

                subscribed_status = is_subscribed(user)
                if not subscribed_status :
                    errormsg = "The owner of this tag is not currently subscribed"
                    return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
                
                # Try to get user for email
                find_user = None
                try :
                    find_user = auth.User.objects.get(email=email)
                except :
                    find_user = None

                if test :

                    # send test email to owner
                    mailers.mail_test_message_to_owner(request, user, find_user, tag)

                    return render_to_response('mobile/test_a_tag_confirm.html',locals(), context_instance=RequestContext(request))

                else :                
                    countries = get_countries()
                    states_US = get_states(country_code='US')
                    states_AU = get_states(country_code='AU')
                    cities = get_cities('US')
                    item_types = get_item_types()
                    contact_methods = get_contact_methods()
                    
                    front_screen_toggle = "false"
                    details_screen_toggle = "true"
                    #return render_to_response('mobile/found_a_tag_2.html',locals(), context_instance=RequestContext(request))
                    return tag_find_details(request, code=tag_id)
            else :
                return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

    return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))



def get_found_detail_fields(request) :

    if request.method != 'POST' :
        return None

    #if not "details" in request.POST :
    #    return None

    tag_id = ''
    email = ''
    firstname = ''
    lastname = ''
    address = ''
    phone_number = ''
    country = ''
    state = ''
    city = ''
    contact_method = ''
    item_type = ''
    item_description = ''
    item_location = ''
    
    try :
        if 'tag_id' in request.POST : tag_id = request.POST['tag_id']        
        if 'email' in request.POST : email = request.POST['email']
        if 'firstname' in request.POST : firstname = request.POST['firstname']
        if firstname == 'first name' : firstname = ''
        if 'lastname' in request.POST : lastname = request.POST['lastname']
        if lastname == 'last name' : lastname = ''
        if 'address' in request.POST : address = request.POST['address']
        if address == 'address' : address = ''
        if 'phone_number' in request.POST : phone_number = request.POST['phone_number']
        if phone_number == 'phone number' : phone_number = ''
        if 'country' in request.POST : country = request.POST['country']
        if 'state' in request.POST : state = request.POST['state']
        if 'city' in request.POST : city = request.POST['city']
        if 'contact_method' in request.POST : contact_method = request.POST['contact_method']
        if contact_method == 'preferred method of contact' : contact_method = ''
        if 'item_type' in request.POST : item_type = request.POST['item_type']
        if item_type == ' ' : item_type = ''
        if 'item_description' in request.POST : item_description = request.POST['item_description']
        if item_description == 'description' : item_description = ''
        if 'item_location' in request.POST : item_location = request.POST['item_location']
        if item_location == 'description' : item_location = ''
    except :
        return None

    found_tag_fields = {}
    found_tag_fields['tag_id'] = tag_id
    found_tag_fields['email'] = email
    found_tag_fields['firstname'] = firstname
    found_tag_fields['lastname'] = lastname
    found_tag_fields['address'] = address
    found_tag_fields['phone_number'] = phone_number
    found_tag_fields['country'] = country
    found_tag_fields['state'] = state
    found_tag_fields['city'] = city
    found_tag_fields['contact_method'] = contact_method
    found_tag_fields['item_type'] = item_type
    found_tag_fields['item_description'] = item_description
    found_tag_fields['item_location'] = item_location
    
    return found_tag_fields


@csrf_exempt
def mobile_find_details(request, code=None) :

    tag_id = code
    if tag_id is None:
        if 'id' in request.GET :
            tag_id = request.GET['id']
        elif 'tag_id' in request.GET :
            tag_id = request.GET['tag_id']

    email = None
    if "email" in request.GET :
        email = request.GET['email']

    #name = request.POST['name']

    found_detail_fields = get_found_detail_fields(request)
    
    if found_detail_fields is None :
        errormsg = "Invalid details"
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
    
    if tag_id is None :
        tag_id = found_detail_fields['tag_id']

    if email is None :
        email = found_detail_fields['email']

    if tag_id is None or len(tag_id) == 0:
        errormsg = "Invalid tag id"        
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
    if email is None or len(email) == 0:
        errormsg = "Invalid finder email"        
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

    # 14062012 no longer required fields
    #if len(found_detail_fields['item_type']) == 0 :
    #    if len(found_detail_fields['item_description']) == 0 :
    #        countries = get_countries()
    #        states_US = get_states(country_code='US')
    #        states_AU = get_states(country_code='AU')
    #        cities = get_cities('US')
    #        item_types = get_item_types()
    #        contact_methods = get_contact_methods()
            
    #        front_screen_toggle = "false"
    #        details_screen_toggle = "true"
    #        return render_to_response('mobile/found_a_tag_2.html',locals(), context_instance=RequestContext(request))

    tag = None
    user = None

    tag = get_tag(tag_id)
    if tag is None :
        errormsg = "Invalid Tag ID"
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
    
    user = tag.user
    if user is None :
        errormsg = "This tag is not currently registered"
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))

    subscribed_status = is_subscribed(user)
    if not subscribed_status :
        errormsg = "The owner of this tag is not currently subscribed"
        return render_to_response('mobile/found_a_tag_1.html',locals(), context_instance=RequestContext(request))
    
    # Try to get user for email
    find_user = None
    try :
        find_user = auth.User.objects.get(email=email)
    except :
        find_user = None

    new_message = FoundMessage(tag=tag, email=email)

    new_message.phone = found_detail_fields['phone_number']
    new_message.firstname = found_detail_fields['firstname']
    new_message.lastname = found_detail_fields['lastname']
    new_message.address = found_detail_fields['address']
    new_message.city = found_detail_fields['city']
    new_message.state = found_detail_fields['state']
    new_message.country = found_detail_fields['country']

    contact_method = found_detail_fields['contact_method']
    if len(contact_method) == 0 : contact_method = 'email'

    new_message.contact_method = contact_method

    if len(found_detail_fields['item_type']) > 0 :
        description = found_detail_fields['item_type']
        if len(found_detail_fields['item_description']) > 0 :
            description = "%s - %s" % (description, found_detail_fields['item_description'])                
        new_message.item_description = description
        
    else :
        new_message.item_description = found_detail_fields['item_description']
           
    new_message.item_location = found_detail_fields['item_location']
    new_message.status = 1
    
    new_message.save()

    create_log_entry('Create Found Messsage', 'Item %s has been found by %s.' % (new_message.item_description, new_message.email), tablename='FoundMessage')
       
    # send email to owner
    if contact_method == 'email' :
        mailers.mail_message_to_owner(request, user, find_user, new_message)

    # 25062012 If the finder has given an email send a thank you email to them
    if len(email) > 0 :
        mailers.mail_message_to_finder(request, email, find_user, new_message)

    return render_to_response('mobile/found_a_tag_normal_confirm.html',locals(), context_instance=RequestContext(request))



def add_message(request, tag_id, email, found_details):

    tag = get_tag(tag_id)
    if tag is None :
        errormsg = "Invalid Tag ID"
        return (False, errormsg)
    
    user = tag.user
    if user is None :
        errormsg = "This tag is not currently registered"
        return (False, errormsg)

    subscribed_status = is_subscribed(user)
    if not subscribed_status :
        errormsg = "The owner of this tag is not currently subscribed"
        return (False, errormsg)
    
    # Try to get user for email
    find_user = None
    try :
        find_user = auth.User.objects.get(email=email)
    except :
        find_user = None

    new_message = FoundMessage(tag=tag, email=email)

    if 'phone_number' in found_details: new_message.phone = found_details['phone_number']
    if 'firstname' in found_details: new_message.firstname = found_details['firstname']
    if 'lastname' in found_details: new_message.lastname = found_details['lastname']
    if 'address' in found_details: new_message.address = found_details['address']
    if 'city' in found_details: new_message.city = found_details['city']
    if 'state' in found_details: new_message.state = found_details['state']
    if 'country' in found_details: new_message.country = found_details['country']

    contact_method = ""
    if 'contact_method' in found_details: contact_method = found_details['contact_method']
    if len(contact_method) == 0 : contact_method = 'email'

    new_message.contact_method = contact_method

    if 'item_type' in found_details: 
        if len(found_details['item_type']) > 0 :
            description = found_details['item_type']
            if 'item_description' in found_details and len(found_details['item_description']) > 0 :
                description = "%s - %s" % (description, found_details['item_description'])                
            new_message.item_description = description
            
        else :
            if 'item_description' in found_details: new_message.item_description = found_details['item_description']
    else:
        new_message.item_description = ""
        
    if 'item_location' in found_details: new_message.item_location = found_details['item_location']
    new_message.status = 1
    
    new_message.save()

    create_log_entry('Create Found Messsage', 'Item %s has been found by %s.' % (new_message.item_description, new_message.email), tablename='FoundMessage')
       
    # send email to owner
    if contact_method == 'email' :
        mailers.mail_message_to_owner(request, user, find_user, new_message)

    # 25062012 If the finder has given an email send a thank you email to them
    if len(email) > 0 :
        mailers.mail_message_to_finder(request, email, find_user, new_message)
    
    return (True, "")

def do_test(request, tag_id, email):

    tag = get_tag(tag_id)
    if tag is None :
        errormsg = "Invalid Tag ID"
        return (False, None, errormsg)
    
    user = tag.user
    if user is None :
        errormsg = "This tag is not currently registered"
        return (False, None, errormsg)

    subscribed_status = is_subscribed(user)
    if not subscribed_status :
        errormsg = "The owner of this tag is not currently subscribed"
        return (False, user.email, errormsg)
    
    # Try to get user for email
    find_user = None
    try :
        find_user = auth.User.objects.get(email=email)
    except :
        find_user = None

    # send test email to owner
    mailers.mail_test_message_to_owner(request, user, find_user, tag)

    return (True, user.email, "")

    

def update_view(request) :

    tag_id = None
    if "tag_id" in request.GET :
        tag_id = request.GET['tag_id']

    email = None
    if "email" in request.GET :
        email = request.GET['email']

    country_code = None
    if "country_code" in request.GET :
        country_code = request.GET['country_code']
       
    countries = get_countries()

    states = None
    if not country_code is None :
        states = get_states(country_code=country_code)

    return render_to_response('mobile/found_a_tag_2.html',locals(), context_instance=RequestContext(request))

