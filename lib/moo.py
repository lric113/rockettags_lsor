#import httplib 
import json
import urllib2
import rockettags.lib.oauth as oauth
import rockettags.lib.MultipartPostHandler as MultipartPostHandler
import requests


class MOOClient(oauth.OAuthClient):

    server = 'secure.moo.com'
    oauth_port = 443
    service_port = 80
    request_token_url = 'https://secure.moo.com/oauth/request_token.php'
    access_token_url = 'https://secure.moo.com/oauth/access_token.php'
    authorization_url = 'https://secure.moo.com/oauth/authorize.php'
    service_url =  'http://www.moo.com/api/service/'
    access_token = None

    def __init__(self, consumer_key, consumer_secret):
        self.consumer = oauth.OAuthConsumer(consumer_key, consumer_secret)
        #self.oauth_connection = httplib.HTTPSConnection("%s:%d" % (self.server, self.oauth_port))
        #self.service_connection = httplib.HTTPConnection("%s:%d" % (self.server, self.service_port))


    def __del__(self):
        self.oauth_connection.close()
        #self.service_connection.close()


    def fetch_access_token(self, oauth_request):
        #self.oauth_connection.request(oauth_request.http_method, self.access_token_url, headers=oauth_request.to_header()) 
        #response = self.oauth_connection.getresponse()
        #r = response.read()
        #print(r)
        return None #oauth.OAuthToken.from_string(r)

    def is_authorized(self):
        authorized = False;
        if self.access_token is not None:
            authorized = True
        return authorized


    def get_request_token(self, callback):
        oauth_request = oauth.OAuthRequest.from_consumer_and_token(\
            self.consumer, callback=callback, http_url=self.request_token_url)
        oauth_request.sign_request(oauth.OAuthSignatureMethod_PLAINTEXT(), self.consumer, None)
        #self.oauth_connection.request(oauth_request.http_method, self.request_token_url, headers=oauth_request.to_header())
        #response = self.oauth_connection.getresponse()
        return None #oauth.OAuthToken.from_string(response.read())
        


    def get_authorization_url(self, request_token):
        return "%s?oauth_token=%s" % (self.authorization_url, request_token.key)


    def get_access_token(self, request_token, verification_code):
        oauth_request = oauth.OAuthRequest.from_consumer_and_token( \
            self.consumer, token=request_token, http_url=self.access_token_url, 
            parameters={'oauth_verifier': verification_code})
        oauth_request.sign_request(oauth.OAuthSignatureMethod_PLAINTEXT(), self.consumer, request_token)
        return self.fetch_access_token(oauth_request)

    @staticmethod
    def upload_image(image_file_path):
        #import pdb; pdb.set_trace()
        params = { 'imageFile' : file(image_file_path, 'rb'), 'method':'moo.image.uploadImage' }
        opener = urllib2.build_opener(MultipartPostHandler.MultipartPostHandler)
        resp = json.loads(opener.open("http://www.moo.com/api/service/", params).read())
        item = resp["imageBasketItem"]
        itemResourceUri = item['resourceUri']
        itemResourceName = itemResourceUri.split('/')[-1]
        itemResourceCode = itemResourceName.split('.')[0]        
        return itemResourceCode


    def get_template(self, access_token, template_name):
        params={}
        params['method']= 'moo.template.getTemplate' 
        params['templateCode']= template_name
        
        oauth_request = oauth.OAuthRequest.from_consumer_and_token(self.consumer, http_method='GET', \
            token=access_token, http_url=self.service_url, parameters=params)
        
        oauth_request.sign_request(oauth.OAuthSignatureMethod_HMAC_SHA1(), self.consumer, access_token)
        #self.service_connection.request('GET', oauth_request.to_url())
        #response = self.service_connection.getresponse()
        return None #json.loads(response.read())
        
    def create_empty_pack(self, access_token, product_name):
        params={}
        params['method'] = 'moo.pack.createPack'
        #params['method'] = 'moo.pack.addToCart'
        params['product'] = product_name
        packfile = open('c:/development/teststickerbook3.json', 'r')
        pack = ""
        try:
            for line in packfile:
                pack = "%s%s" % (pack, line)
        finally:
            packfile.close()
        pack_json = json.loads(pack)
        pack = json.dumps(pack_json)
        
        params['pack'] = pack
        
        oauth_request = oauth.OAuthRequest.from_consumer_and_token(self.consumer, http_method='POST', \
            token=access_token, http_url=self.service_url, parameters=params)
        
        oauth_request.sign_request(oauth.OAuthSignatureMethod_HMAC_SHA1(), self.consumer, access_token)
        #self.service_connection.request('POST', oauth_request.to_url())
        #response = self.service_connection.getresponse()        
        return None #json.loads(response.read())

    
    

    def create_stickerbook(self, template_file, image_ids, input_file=None):
        access_token = '' # we don't need an access token since we are using 2legged OAuth authentication
        params={}
        params['method'] = str('moo.pack.createPack')
        params['product'] = 'sticker'

        if input_file is None :
            packfile = open(template_file, 'r')
            pack = ""
            try:
                for line in packfile:
                    pack = "%s%s" % (pack, line)
            finally:
                packfile.close()
            pack_json = json.loads(pack)

            unique_image_ids = set(image_ids)
            

            for image_id in unique_image_ids :

                new_basket_item = {}
                new_basket_item["copyrightOwner"] = None
                new_basket_item["imageItems"] = []               

                new_image_item = {}
                new_image_item["type"] = "print"
                new_image_item["height"] = 268
                new_image_item["rotation"] = 0
                new_image_item["width"] = 221
                new_image_item["resourceUri"] = "http://www.moo.com/is/o/%s.jpeg" % image_id
                new_basket_item["imageItems"].append(new_image_item)

                new_image_item = {}
                new_image_item["type"] = "preview"
                new_image_item["height"] = 1024
                new_image_item["rotation"] = 0
                new_image_item["width"] = 844
                new_image_item["resourceUri"] = "http://www.moo.com/is/r/1024/%s.jpeg" % image_id
                new_basket_item["imageItems"].append(new_image_item)

                new_image_item = {}
                new_image_item["type"] = "thumbnail"
                new_image_item["height"] = 75
                new_image_item["rotation"] = 0
                new_image_item["width"] = 75
                new_image_item["resourceUri"] = "http://www.moo.com/is/t/75/%s.jpeg" % image_id
                new_basket_item["imageItems"].append(new_image_item)
                
                new_basket_item["cacheId"] = "partner_interface_uploader:%s.jpeg" % image_id
                new_basket_item["shouldEnhance"] = False
                new_basket_item["imageBox"] = None
                new_basket_item["type"] = "front"
                new_basket_item["removable"] = True
                new_basket_item["croppable"] = True
                new_basket_item["resourceUri"] = "filestore://image_original/%s.jpeg" % image_id

                pack_json["imageBasket"]["items"].append(new_basket_item)

            side_num = 1
            for image_id in image_ids :

                new_side = {}
                new_side["type"] = "image"
                new_side["sideNum"] = side_num
                new_side["templateCode"] = "sticker_image"
                new_side["data"] = []

                new_side_data = {}
                new_side_data["type"] = "imageData"
                new_side_data["enhance"] = False
                new_side_data["linkId"] = "variable_image_front"
                new_side_data["imageBox"] = {}
                new_side_data["imageBox"]["center"] = {}
                new_side_data["imageBox"]["center"]["y"] = 12
                new_side_data["imageBox"]["center"]["x"] = 12
                new_side_data["imageBox"]["angle"] = 0
                new_side_data["imageBox"]["width"] = 24
                #new_side_data["imageBox"]["height"] = 29.1040723982
                new_side_data["imageBox"]["height"] = 24
                new_side_data["imageStoreFileId"] = "%s" % image_id
                new_side_data["resourceUri"] = "filestore://image_original/%s.jpeg" % image_id
                new_side["data"].append(new_side_data)

                pack_json["sides"].append(new_side)

                side_num = side_num + 1
                
        else :
            packfile = open(input_file, 'r')
            pack = ""
            try:
                for line in packfile:
                    pack = "%s%s" % (pack, line)
            finally:
                packfile.close()
            pack_json = json.loads(pack)
                    
        pack = json.dumps(pack_json)

        outfile = open("%s_output.json" % template_file, 'w')
        outfile.write(pack)
        outfile.close()

        params['pack'] = pack
        
        oauth_request = oauth.OAuthRequest.from_consumer_and_token(self.consumer, http_method='POST', \
            token=None, http_url=self.service_url, parameters=params)
        
        oauth_request.sign_request(oauth.OAuthSignatureMethod_HMAC_SHA1(), self.consumer, access_token)
        #self.service_connection.request('POST', oauth_request.to_url())
        #self.service_connection.request('POST', oauth_request.get_normalized_http_url(), body=oauth_request.to_postdata(), headers={'Expect':'',})
        #self.service_connection.request('POST', oauth_request.get_normalized_http_url(), body=oauth_request.get_normalized_parameters(), headers={'Expect':'',})
        #self.service_connection.request('POST', '%s&method=moo.pack.createPack&%s' % (oauth_request.to_url_oauth(),oauth_request.to_postdata())) # , body=oauth_request.to_postdata(), headers={'Expect':'',})
        #self.service_connection.request('POST', oauth_request.get_normalized_http_url(), body=str(oauth_request.to_postdata()), headers={str('Expect'):str(''),})

        r = requests.post(oauth_request.get_normalized_http_url(), data=oauth_request.to_postdata())        
        
        #self.service_connection.request('POST', 'http://localhost/tags/captcha/', body=oauth_request.to_postdata())

        #test_connection = httplib.HTTPConnection("%s:%d" % ('localhost', 80))
        #test_connection.request('POST', 'http://localhost/tags/captcha/', body=oauth_request.to_postdata())
        
        #request_url = oauth_request.get_normalized_http_url()
        request_url = '%s<BR>\r\n<BR>\r\n<BR>\r\n%s' % (oauth_request.get_normalized_http_url(), oauth_request.to_postdata())

        #response = self.service_connection.getresponse()
        #response = test_connection.getresponse()
        
        json_response = None
        #raw_response = response.read()
        raw_response = r.content
        try :
            json_response = json.loads(raw_response)
        except :
            json_response = None
            
            
        return (pack, raw_response, request_url, json_response)
