def handle_uploaded_file(f, destfile):
    destination = open(destfile, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
