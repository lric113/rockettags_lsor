from captcha.fields import CaptchaField
from django import forms
from django.shortcuts import render_to_response
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from transition.models import Tag, UserProfile
from django.contrib.auth import authenticate
from transition.item_types import item_types
from datetime import datetime
from lib import handle_uploaded_file
from mailers import mail_staff_alert
import pytz
import uuid
import csv


class PromoForm(forms.Form):
    """ Promo page form """
    email = forms.EmailField(
        error_messages={'required': 'Email is required'},
        widget=forms.TextInput(attrs={'placeholder': 'Email Address', 'class': 'form-control'}))

    def clean_email(self):
        # This extra lower and strip is for iPads doing funny stuff.
        email = self.cleaned_data['email'].lower().strip()
        return email


class GetTagsForm(forms.Form):
    batch_num = forms.IntegerField()


class MyRocketTagsForm(forms.Form):
    email = forms.EmailField(error_messages={'required': 'Email is required'})

    def clean_email(self):

        email = self.cleaned_data['email'].lower().strip()
        try:
            user = User.objects.get(email=email)
            raise forms.ValidationError("%s is already in use" % email)
        except User.DoesNotExist:
            pass
        return email


class LoginForm(forms.Form):
    """
    Main login form
    """

    email = forms.EmailField(error_messages={'required': 'Email is required'})
    code = forms.CharField(max_length=10, error_messages={'required': 'RocketTag ID or password is required'})
    login_type = forms.HiddenInput()

    def clean_email(self):
        email = self.cleaned_data['email'].lower().strip()
        return email

    def clean_code(self):
        code = self.cleaned_data.get("code").strip()
        return code

    def clean(self):
        """
        Main clean method
        """

        cleaned_data = self.cleaned_data
        email = cleaned_data.get("email")
        code = cleaned_data.get("code")

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise forms.ValidationError("%s is not registered" % email)

        # try to get the user's UserProfile
        try:
            user_profile_set = UserProfile.objects.filter(user_id=user.id)
            user_profile = user_profile_set[0]
        except Exception:
            raise forms.ValidationError("This user does not have a UserProfile! Please contact the RocketTags Staff")

        if user_profile.is_corporate:
            # user is corporate, use code for password if that does not work fall back to normal mode
            auth_user = authenticate(username=email, password=code)
            if auth_user is None:
                # user is corporate but authentication failed so fallback to normal auth mode
                raise forms.ValidationError("Please try again using the correct RocketTag ID or password")
        else:
            # user is not corporate, check if tag exists
            code = self.cleaned_data.get("code").upper().strip()
            try:
                tag = Tag.objects.get(code=code)
            except Tag.DoesNotExist:
                raise forms.ValidationError("Invalid RocketTag code")
            try:
                tag = Tag.objects.get(code=code, user__isnull=False)
            except Tag.DoesNotExist:
                raise forms.ValidationError("The tag is unregistered")

        return cleaned_data


class ResetForm(forms.Form):
    email = forms.EmailField()
    secret_code = forms.CharField(max_length=10, error_messages={'required': 'Secret code is required'})


class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()


class RegisterTagForm1(forms.Form):
    code = forms.CharField(max_length=10, error_messages={'required': 'RocketTag ID is required'})
    step = forms.IntegerField(widget=forms.HiddenInput())

    def clean_code(self):

        code = self.cleaned_data.get("code").upper().strip()
        try:
            tag = Tag.objects.get(code=code)
        except:
            raise forms.ValidationError("Invalid RocketTag code")
        try:
            tag = Tag.objects.get(code=code, user__isnull=True)
        except:
            raise forms.ValidationError("The tag is already registered")
        return code


class RegisterTagForm2(forms.Form):
    email = forms.EmailField()
    email2 = forms.EmailField()
    code = forms.CharField(max_length=10, widget=forms.HiddenInput())
    step = forms.IntegerField(widget=forms.HiddenInput())

    def clean_email(self):
        email = self.cleaned_data['email'].lower().strip()
        return email

    def clean_email2(self):
        email2 = self.cleaned_data['email2'].lower().strip()
        return email2

    def clean(self):
        cleaned_data = self.cleaned_data
        email = cleaned_data.get("email")
        email2 = cleaned_data.get("email2")

        if len(email) > 30:
            mail_staff_alert("An email address over 30 characters (%s) was used to register. Perhaps fix this?" % email)
            raise forms.ValidationError("Email address should not exceed 30 characters")

        if email is None and email2 is None:
            raise forms.ValidationError("Email and confirm required")
        if email != email2:
            raise forms.ValidationError("Emails do not match")
        return cleaned_data


class RegisterTagForm3(forms.Form):
    email = forms.EmailField(widget=forms.HiddenInput())
    code = forms.CharField(max_length=10, widget=forms.HiddenInput())
    step = forms.IntegerField(widget=forms.HiddenInput())
    yes_no = forms.IntegerField(widget=forms.HiddenInput())


class TestTagForm1(forms.Form):
    """ In order to copy existing functionality there is no validation
        on code. The user is not told they have a valid code or not.
    """

    code = forms.CharField(max_length=10, error_messages={'required': 'RocketTag ID is required'})
    step = forms.IntegerField(widget=forms.HiddenInput())

    def clean_code(self):
        """ Upper the code so case sensitivity is not an issue.
            TagTagForm2 will not need the code cleaned again.
        """

        code = self.cleaned_data['code'].upper().strip()
        return code


class TestTagForm2(forms.Form):
    """ The only real validation is on the captcha.
        The user is not told they have a valid code or not.
    """

    captcha = CaptchaField()
    code = forms.CharField(max_length=10, widget=forms.HiddenInput())
    step = forms.IntegerField(widget=forms.HiddenInput())


class FoundTagForm1(forms.Form):
    code = forms.CharField(max_length=10, error_messages={'required': 'RocketTag ID is required'})
    email = forms.EmailField(error_messages={'required': 'Email is required'})
    item_description = forms.CharField(required=False, max_length=50)
    step = forms.IntegerField(widget=forms.HiddenInput())

    def clean_code(self):
        tag = None
        code = self.cleaned_data['code'].upper().strip()
        try:
            tag = Tag.objects.get(code=code)
        except:
            pass

        if tag is None:
            raise forms.ValidationError("Invalid RocketTag ID")
        return code


class FoundTagForm2(forms.Form):
    captcha = CaptchaField()
    step = forms.IntegerField(widget=forms.HiddenInput())
    fid = forms.IntegerField(widget=forms.HiddenInput())


class FoundTagForm3(forms.Form):
    step = forms.IntegerField(widget=forms.HiddenInput())
    fid = forms.IntegerField(widget=forms.HiddenInput())
    first_name = forms.CharField(required=False, max_length=50)
    last_name = forms.CharField(required=False, max_length=50)
    address = forms.CharField(required=False, max_length=128)
    phone_number = forms.CharField(required=False, max_length=32)
    contact_method = forms.CharField(required=False, max_length=32)
    item_description = forms.CharField(required=False, max_length=225)
    item_location = forms.CharField(required=False, max_length=225)


class CustomFieldsForm(forms.Form):
    """
    Form subclass for the four custom field
    """

    field1 = forms.CharField(label='Field 1', max_length=40, required=False)
    field2 = forms.CharField(label='Field 2', max_length=40, required=False)
    field3 = forms.CharField(label='Field 3', max_length=40, required=False)
    field4 = forms.CharField(label='Field 4', max_length=40, required=False)


class AddCorporateAccountForm(forms.Form):
    """
    Form subclass for the add corporate account form
    """

    email = forms.EmailField(label='email', max_length=200, required=True,
                             widget=forms.EmailInput(attrs={'autocomplete': 'off',
                                                            'size': '40',
                                                            }))

    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput, label="Password")
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput, label="Confirm password")

    def clean_email(self):
        """
        Clean email field value
        """
        email = self.cleaned_data['email'].strip()
        try:
            validate_email(email)
        except ValidationError:
            raise forms.ValidationError("Please provide a valid email address")
        return email

    def clean(self):
        """
        main clean() method to verify password match
        """
        cleaned_data = self.cleaned_data
        email = cleaned_data.get("email")
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if (type(password1) is None) or (type(password2) is None):
            raise forms.ValidationError({"password1": "Please provide a password on both password fields"})

        if (type(password1) is str and len(password1) == 0) or (type(password2) is str and len(password2) == 0):
            raise forms.ValidationError({"password1": "Please provide a password on both password fields"})

        if password1 != password2:
            raise forms.ValidationError({"password1": "Your password do not match"})

        # check if email address is used
        accounts = User.objects.filter(email=email)
        if len(accounts) > 0:
            raise forms.ValidationError({"email": "The email address you gave is already in use by another account"})

        return cleaned_data


class UploadCorporateTagsForm(forms.Form):
    """
    Form subclass for the upload of a batch of tags for the corporate accounts
    """

    account = forms.ModelChoiceField(label="Corporate account",
                                     queryset=User.objects.filter(userprofile__is_corporate=True).order_by('email'))
    tagsfileupload = forms.FileField(label='Select a file')
    numberoftags = forms.CharField(max_length=5, label="Number of tags in the file")

    def clean_account(self):
        """
        Clean method for account field
        :return:
        """
        account = self.cleaned_data['account']
        return account

    def clean_tagfileupload(self):
        """
        Clean method for file upload field
        :return:
        """
        tagsfileupload = self.cleaned_data['tagsfileupload'].strip()
        return tagsfileupload

    def clean_numberoftags(self):
        """
        Clean method for the number of tags to upload value
        :return:
        """
        numberoftags = self.cleaned_data['numberoftags'].strip()
        try:
            numberoftags = int(numberoftags)
        except Exception:
            return 0

        return numberoftags

    def clean(self):
        """
        Main clean method the upload form
        :return:
        """

        cleaned_data = self.cleaned_data
        account = cleaned_data.get("account")
        tagsfileupload = cleaned_data.get("tagsfileupload")
        numberoftags = cleaned_data.get("numberoftags")

        if tagsfileupload is not None:
            # handle the file upload so that we can check its contents here
            csv_file_path = "/tmp/"+tagsfileupload.name
            handle_uploaded_file(tagsfileupload, csv_file_path)

            # open file
            with open(csv_file_path, 'rb') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                tags_list = []
                for row in spamreader:
                    # add tags to list
                    tags_list.append(row)

            if tags_list[0][0] == "Row#":
                # file contained a header, remove the first line
                tags_list.pop(0)

            # check the number of tags in the file
            if len(tags_list) != numberoftags:
                raise ValidationError({"tagsfileupload": "The number of tags uploaded does not match the number given"})

            # check to see if the list of tag uploaded is
            already_reged_tags_list = []
            for tag_list in tags_list:
                listed_tag = Tag.objects.filter(code=tag_list[2], user_id__isnull=False, registered_date__isnull=False)
                if len(listed_tag) > 0:
                    already_reged_tags_list.append(listed_tag[0])

            if len(already_reged_tags_list) > 0:
                raise ValidationError({"tagsfileupload": "%s tag(s) where already found to be registered!" %
                                                         len(already_reged_tags_list)})

        if account is None:
            raise forms.ValidationError({"account": "Please select an account"})

        # check if account given is corporate
        account_profiles = UserProfile.objects.filter(user_id=account.id)
        if len(account_profiles) > 0:
            if not account_profiles[0].is_corporate:
                raise forms.ValidationError({"account": "The account selected is not a corporate account"})

        if type(numberoftags) is not int:
            raise forms.ValidationError({"numberoftags": "Please provide a number"})

        if numberoftags == 0 or numberoftags < 1:
            raise forms.ValidationError({"numberoftags": "Please provide a valid number (1 or more)"})
