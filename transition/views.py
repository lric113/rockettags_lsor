from mailers import mail_test_message_to_owner, mail_message_to_finder, mail_message_to_owner, mail_add_a_tag, \
    send_promo_email
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.db import transaction
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from forms import *
from transition.models import Tag, FoundMessage, TestTag
from transition.models import UserProfile
from transition.models import Subscription
from models import Promo
from datetime import datetime, timedelta
from django.conf import settings
import pytz

CHECK_TIMEOUT_MINS = 15  # See check function


# Home page -------------------------------------------------------------

def home(request):
    """
    Home page
    A straight copy of the shopify home page.
    """
    page = 'home'
    return render(request, 'home.html', locals())


# MyRocketTags page ---------------------------------------------------------------------------

@login_required(login_url='/login')
@transaction.atomic
def myrockettags(request):
    """
    Shows a list of registered tags for a user.
    Previously it also show a subscription history
    """

    page = 'myrockettags'
    if request.method == 'POST':
        form = MyRocketTagsForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            user = request.user
            user.username = email
            user.email = email
            user.set_password(email)
            user.save()
            user = User.objects.get(username=email)
            auth_user = authenticate(username=email, password=email)
            login(request, auth_user)
            update_msg = True
    profile = request.user.userprofile
    tags = Tag.objects.filter(user=request.user).order_by('registered_date')
    # Subscriptions turned off subs = Subscription.objects.filter(user=request.user).order_by('creation_date')
    subs = None
    combined = combined_tags_subs(tags, subs)
    return render(request, 'myrockettags.html', locals())


def combined_tags_subs(tags, subs):
    """
    This was a chronologically sorted list of tags and subscription entries
    Subscription has been removed.
    """

    """
    Subscriptions turned off.
    lookup = {  396: '396 day renewal',
        1126: '1126 day renewal',
        730: '2yr sub renewal',
        1095: '3yr sub renewal',
        821: '821 day renewal',
        915: '915 day renewal',
        551: '551 day renewal',
        365: '1yr sub renewal' }
    """
    combined = []
    for tag in tags:
        # adding this catch in case registered_date in None
        if tag.registered_date is None:
            tag.registered_date = datetime(1980, 1, 1, 0, 0, 0).replace(tzinfo=pytz.utc)
        combined.append((tag.code, tag.registered_date))
    """
    Subscriptions turned off.
    for sub in subs:
        combined.append( (lookup[sub.period], sub.creation_date) )
    """
    combined = sorted(combined, key=lambda item: item[1])  # sort by date
    return combined


# Login / logout stuff ------------------------------------------------------------------

def login_(request):
    """
    Site wide login
    :param request:
    :return:
    """

    page = 'myrockettags'
    normal_login_form_classes = []
    corporate_login_form_classes = []

    if request.method == 'POST':

        login_type = request.POST.get('login_type', None)

        if login_type == "normal":
            normal_login_form_classes.append("active")
        elif login_type == "corporate":
            corporate_login_form_classes.append("active")
        else:
            normal_login_form_classes.append("active")

        normal_login_form_classes = " ".join(normal_login_form_classes)
        corporate_login_form_classes = " ".join(corporate_login_form_classes)

        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            code = form.cleaned_data['code']
            user = User.objects.get(username=email)

            user_profile_set = UserProfile.objects.filter(user_id=user.id)
            user_profile = user_profile_set[0]

            if user_profile.is_corporate:
                # user is corporate, use code for password if that does not work fall back to normal mode
                auth_user = authenticate(username=email, password=code)
                next_page = '/corpmanage'
            else:
                # user is not corporate
                auth_user = authenticate(username=email, password=email)
                next_page = request.GET.get('next', '/myrockettags')

            login(request, auth_user)
            return redirect(next_page)
    else:
        normal_login_form_classes = ["active"]
        normal_login_form_classes = " ".join(normal_login_form_classes)
        form = LoginForm()
    return render(request, 'login.html', locals())


def user_logout(request):
    logout(request)
    return redirect('http://store.rockettags.com/')


# QR Code stuff --------------------------------------------------------------

def qrcode(request, code=None):
    """
    This for mobile users scanning their qrcode and following the link
    """

    try:
        tag = Tag.objects.get(code=code, user__isnull=False)
        return redirect('/foundtag/%s' % code)
    except Tag.DoesNotExist:
        return redirect('/registertag/%s' % code)


# Reset ------------------------------------------------

def reset(request):
    """
    Way to completely delete a user and reset their tags
    Should be used by Tim and Terry over https
    """

    if request.POST:
        form = ResetForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            secret_code = form.cleaned_data['secret_code']
            if secret_code == 'rocket2012':
                try:
                    user = User.objects.get(username=email)
                    deregister_tags(user)
                    user.delete()
                    message = 'Deleted!'
                except User.DoesNotExist:
                    pass
    else:
        form = ResetForm()
    return render(request, 'reset.html', locals())


def deregister_tags(user):
    """
    Helper method to deregister tags
    """

    tags = Tag.objects.filter(user=user)
    for tag in tags:
        tag.registered_date = None
        tag.user = None
        tag.save()


# Test tag -------------------------------------

def test_tag(request):
    """
    Users can test their tag is properly registered to their email address.
    All forms POST here and then a handle function is used.
    Validation logic in the respective Form.
    """

    page = 'testtag'
    if request.method == 'POST':
        step = request.POST.get('step', '')
        if step == '1':
            return handle_testtag_step1_post(request)
        elif step == '2':
            return handle_testtag_step2_post(request)
    else:
        initial = {'step': 1}
        form = TestTagForm1(initial=initial)
        return render(request, 'test_tag_1.html', locals())


def handle_testtag_step1_post(request):
    page = 'testtag'
    form = TestTagForm1(data=request.POST)
    if form.is_valid():
        initial = {'code': form.cleaned_data['code'],
                   'step': 2}
        form = TestTagForm2(initial=initial)
        return render(request, 'test_tag_2.html', locals())
    else:
        return render(request, 'test_tag_1.html', locals())


def handle_testtag_step2_post(request):
    """
    If they get the captcha right the form will be valid even
    though the code might be invalid.
    """

    page = 'testtag'
    form = TestTagForm2(data=request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        try:
            tag = Tag.objects.get(code=code)
            if tag.user is not None:
                mail_test_message_to_owner(request, tag.user, None, tag)
        except Tag.DoesNotExist:
            pass
            # TODO add message to site.log              
        return render(request, 'test_tag_3.html', locals())
    else:
        return render(request, 'test_tag_2.html', locals())


# Register tag ---------------------------------------------------------------

def register_tag(request, code=None):
    """
    A user can register a new tag
    All forms POST here and then a handle function is used.
    Validation logic in the respective Form.
    """

    page = 'registertag'
    if request.method == 'POST':
        step = request.POST.get('step', '')
        if step == '1':
            return handle_register_tag_1_post(request)
        elif step == '2':
            return handle_register_tag_2_post(request)
        elif step == '3':
            return handle_register_tag_3_post(request)
    else:
        initial = {'step': 1, 'code': code}
        form = RegisterTagForm1(initial=initial)
        return render(request, 'register_tag_1.html', locals())


def handle_register_tag_1_post(request):
    page = 'registertag'
    form = RegisterTagForm1(data=request.POST)
    if form.is_valid():
        initial = {'code': form.cleaned_data['code'], 'step': 2}
        form = RegisterTagForm2(initial=initial)
        return render(request, 'register_tag_2.html', locals())
    else:
        return render(request, 'register_tag_1.html', locals())


def handle_register_tag_2_post(request):
    page = 'registertag'
    form = RegisterTagForm2(data=request.POST)
    if form.is_valid():
        email = form.cleaned_data['email']
        code = form.cleaned_data['code']
        try:
            user = User.objects.get(username=email)
            initial = {'code': form.cleaned_data['code'],
                       'email': form.cleaned_data['email'],
                       'step': 3, 'yes_no': 0}
            form = RegisterTagForm3(initial=initial)
            return render(request, 'register_tag_3.html', locals())
        except User.DoesNotExist:
            now = datetime.utcnow()
            now = now.replace(tzinfo=pytz.utc)
            user = User.objects.create_user(username=email, email=email, password=email)
            userProfile, created = UserProfile.objects.get_or_create(
                user=user,
                registration_date=now,
                creation_date=now)
            auth_user = authenticate(username=email, password=email)
            login(request, auth_user)
            register_code(code, user)
            mail_add_a_tag(request, user, 'tags', None, tag_code=code)
            return render(request, 'register_tag_4.html', locals())
    else:
        return render(request, 'register_tag_2.html', locals())


def handle_register_tag_3_post(request):
    page = 'registertag'
    form = RegisterTagForm3(data=request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        yes_no = form.cleaned_data['yes_no']
        email = form.cleaned_data['email']
        if yes_no:
            # Login and send email           
            user = User.objects.get(username=email)
            auth_user = authenticate(username=email, password=email)
            login(request, auth_user)
            profile = user.userprofile
            now = datetime.utcnow()
            now = now.replace(tzinfo=pytz.utc)
            profile.save()
            register_code(code, user)
            mail_add_a_tag(request, user, 'tags', None, tag_code=code)
    return render(request, 'register_tag_4.html', locals())


def register_code(code, user):
    """
    Helper function used by handle_register_tag_2_post
    """
    tag = Tag.objects.get(code=code)
    now = datetime.utcnow()
    now = now.replace(tzinfo=pytz.utc)
    tag.registered_date = now
    tag.user = user
    tag.save()


# Found a tag ----------------------------------------------------------------------------

def found_tag(request, code=None):
    """
    Found a tag process.
    Basically the owner and the finder get emails.
    All forms POST here and then a handle function is to delegate.
    Validation logic in the respective Form.
    """

    page = 'foundtag'
    if request.method == 'POST':
        step = request.POST.get('step', '')
        if step == '1':
            return handle_found_tag_1_post(request)
        elif step == '2':
            return handle_found_tag_2_post(request)
        elif step == '3':
            return handle_found_tag_3_post(request)
    else:
        initial = {'step': 1, 'code': code}
        form = FoundTagForm1(initial=initial)
        return render(request, 'found_tag_1.html', locals())


def handle_found_tag_1_post(request):
    """
    User has submitted form with code, email and item_description
    We save the information into a FoundMessage so that if they discard
    the form we can still send a found message to the user later.
    """

    page = 'foundtag'
    form = FoundTagForm1(data=request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        email = form.cleaned_data['email']
        item_description = form.cleaned_data['item_description']
        tag = Tag.objects.get(code=code)
        found = FoundMessage()
        found.tag = tag
        found.email = email
        found.generated_date = datetime.utcnow()
        found.item_description = item_description
        found.complete = False
        found.save()

        initial = {'step': 2,
                   'fid': found.id}
        form = FoundTagForm2(initial=initial)
        return render(request, 'found_tag_2.html', locals())
    else:
        return render(request, 'found_tag_1.html', locals())


def handle_found_tag_2_post(request):
    """
    This is the optional details form
    """

    page = 'foundtag'
    form = FoundTagForm2(data=request.POST)
    if form.is_valid():
        fid = form.cleaned_data['fid']
        found = FoundMessage.objects.get(pk=fid)
        initial = {'item_description': found.item_description,
                   'step': 3,
                   'fid': found.id}
        form = FoundTagForm3(initial=initial)
        return render(request, 'found_tag_3.html', locals())
    else:
        return render(request, 'found_tag_2.html', locals())


def handle_found_tag_3_post(request):
    page = 'foundtag'
    form = FoundTagForm3(data=request.POST)
    if form.is_valid():
        fid = form.cleaned_data['fid']
        found = FoundMessage.objects.get(pk=fid)
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        address = form.cleaned_data['address']
        phone_number = form.cleaned_data['phone_number']
        contact_method = form.cleaned_data['contact_method']
        if contact_method is None:
            contact_method = 'email'
        item_description = form.cleaned_data['item_description']
        item_location = form.cleaned_data['item_location']

        found.firstname = first_name
        found.lastname = last_name
        found.address = address
        found.phone = phone_number
        found.contact_method = contact_method
        found.item_description = item_description
        found.item_location = item_location
        found.completed = True
        found.save()
        tag = found.tag

        if tag.user is not None:
            mail_message_to_owner(request, tag.user, tag, found)
            mail_message_to_finder(request, found.email, None, found)

    return render(request, 'found_tag_4.html', locals())


def check(request):
    """
    This is called by a cron job - you need to setup every CHECK_TIMEOUT_MINS fifteen_mins_ago
    It checks if someone has started a found a tag (given their email address and the tag found) but
    quit before giving more details.
    This way the owner gets an email even if the finder can't be bothered fully completing the
    found a tag wizard.
    """

    fifteen_mins_ago = datetime.utcnow() - timedelta(minutes=CHECK_TIMEOUT_MINS)
    fifteen_mins_ago = fifteen_mins_ago.replace(tzinfo=pytz.utc)
    messages = FoundMessage.objects.filter(
        completed=False,
        generated_date__lte=fifteen_mins_ago)

    for found in messages:
        tag = found.tag
        if tag.user is not None:
            mail_message_to_owner(request, tag.user, tag, found)
            mail_message_to_finder(request, found.email, None, found)
            found.completed = True
            found.save()
    return HttpResponse('Checked!')


@login_required(login_url='/login')
def bulk_test(request):
    """
    Terry was looking for a mechanism to check a bunch of tag codes
    against the Tag table to see if they exist
    """

    class Dummy:
        def __init__(self, code, status):
            self.code = code
            self.status = status

    tested = []
    testtags = TestTag.objects.all()
    for testtag in testtags:
        try:
            tag = Tag.objects.get(code=testtag.code)
            tested.append(tag)
        except Tag.DoesNotExist:
            tested.append(Dummy(testtag.code, 'Not found!'))
    return render(request, 'bulk-test.html', locals())


# Promo stuff --------------------------------------------------------------------------

def corp_promo(request):
    """ Just shows a simple page
        Could be moved to urls.py
    """

    page = 'corp_promo'
    return render(request, 'corp_promo.html', locals())


def corp_solutions(request):
    """ Just show a simple page
        Could be moved to urls.py
    """

    page = 'corp_solutions'
    return render(request, 'corp_solutions.html', locals())


def redirect_to_promo_offer(request):
    """ See urls Tim wanted this redirect.
        Could be moved to urls.py
    """

    return redirect('promo/offer')


@user_passes_test(lambda u: u.is_staff, login_url='/login')
def cam01(request):
    """
    Terry hired students to promote RT.
    We wanted them logged in so we tracked when they signed up a customer for
    a promo discount offer.
    """

    promo_detail = {'percentage': 20, 'code': 'PXWOXPM8ISRZ'}
    campaign_promo = True
    if request.POST:
        form = PromoForm(request.POST)
        if form.is_valid():
            # Save promo customer to DB
            promo = Promo()
            promo.promoter_email = request.user.email
            promo.customer_email = form.cleaned_data['email']
            promo.save()
            send_promo_email(request=request,
                             bcc=settings.BLIND_COPY_EMAIL + ',' + request.user.email,
                             to_email=form.cleaned_data['email'],
                             promo_detail=promo_detail,
                             subject='RocketTags Discounts Offer Enclosed')
            # Successful so wipe out form
            customer_email = form.cleaned_data['email']
            form = PromoForm()
    else:
        form = PromoForm()
    return render(request, 'promo.html', locals())


def promo(request):
    """
    Gives normal users a place they can sign up with their email
    for a discount offer.
    """

    promo_detail = {'percentage': 50, 'code': 'IT4V13KRATD9'}
    campaign_promo = False
    if request.POST:
        form = PromoForm(request.POST)
        if form.is_valid():
            # Save promo customer to DB
            promo = Promo()
            promo.promoter_email = '-'
            promo.customer_email = form.cleaned_data['email']
            promo.save()
            send_promo_email(request=request,
                             bcc=settings.BLIND_COPY_EMAIL,
                             to_email=form.cleaned_data['email'],
                             promo_detail=promo_detail,
                             subject='Enclosed is Your RocketTags Discount Offer')
            # Successful so redirect to thank you.
            return redirect('/promo-thankyou')
    else:
        form = PromoForm()
    return render(request, 'promo.html', locals())


def promo_thankyou(request):
    """
    Could be moved to urls.py
    """
    return render(request, 'promo_thankyou.html', locals())


# Helper stuff -----------------------------------------------------------------------------

def test_captcha(request):
    """
    Not really part of the app
    """

    if request.POST:
        form = CaptchaTestForm(request.POST)
        if form.is_valid():
            human = True
    else:
        form = CaptchaTestForm()
    return render(request, 'test_captcha.html', locals())


def google_seo(request):
    """
    Just a way for google to validate the site
    """

    return render(request, 'googlecb48235008046c8e.html', locals())


def get_tags(request):
    """
    I don't use this ever
    It was a way to identify batches of tags.
    """

    if request.POST:
        form = GetTagsForm(request.POST)
        if form.is_valid():
            batch_num = form.cleaned_data['batch_num']
            tags = Tag.objects.filter(batch_num=batch_num).order_by('id')
    else:
        form = GetTagsForm()
    return render(request, 'get_tags.html', locals())


def csrf_failure(request, reason=None):
    """
    If there is a CSRF failure - this happens if the user is no cookies
    """
    return render(request, 'home.html', locals())


def trainingvideo(request):
    """
    Tim wanted a redirect to this site.
    This should be moved to urls.py
    """
    return redirect('https://www.youtube.com/watch?v=yREh_ojWD5M&spfreload=10')


def facebooklanding(request):
    """
    Facebook landing page
    """

    return render(request, 'landing/facebook.html', locals())
