from django.conf import settings


def global_settings(request):
    """
    Add any global value from settings here to make it available to the template layer
    :param request:
    :return dict:
    """
    return {
        'MAIN_SECURE_HOST': settings.MAIN_SECURE_HOST,
        'MAIN_INSECURE_HOST': settings.MAIN_INSECURE_HOST,
        'MAILER_HOST': settings.MAILER_HOST,
    }
