from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from models import Promo, TestTag, UserProfile, Subscription, Tag


class TestTagResource(resources.ModelResource):
    class Meta:
        model = TestTag


class TestTagAdmin(ImportExportModelAdmin):
    resource_class = TestTagResource
    pass


class PromoResource(resources.ModelResource):
    class Meta:
        model = Promo


class PromoAdmin(ImportExportModelAdmin):
    resource_class = PromoResource
    pass


class UserProfileResource(resources.ModelResource):
    class Meta:
        model = UserProfile


class UserProfileAdmin(ImportExportModelAdmin):
    resource_class = UserProfileResource
    pass


class SubscriptionResource(resources.ModelResource):
    class Meta:
        model = Subscription


class SubscriptionAdmin(ImportExportModelAdmin):
    resource_class = SubscriptionResource
    pass


class TagResource(resources.ModelResource):
    class Meta:
        model = Tag


class TagAdmin(ImportExportModelAdmin):
    resource_class = TagResource
    pass


admin.site.register(Promo, PromoAdmin)
admin.site.register(TestTag, TestTagAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Tag, TagAdmin)
