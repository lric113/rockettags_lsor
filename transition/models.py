from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.conf import settings
import pytz


class TestTag(models.Model):
    code = models.CharField(null=False, blank=False, unique=True, max_length=8)

    def __unicode__(self):
        return u'%s' % self.code


class Tag(models.Model):
    code = models.CharField(null=False, blank=False, unique=True, max_length=8)
    user = models.ForeignKey(User, null=True, blank=True, related_name='tags', on_delete=models.SET_NULL)
    issued_date = models.DateTimeField(default=None)
    registered_date = models.DateTimeField(null=True, blank=True)
    edit_time = models.DateTimeField(default=None, null=True)
    assigned_to = models.EmailField(null=True, blank=True)
    status = models.PositiveIntegerField(default=0)  # 0 - not registered, 1 = registered
    tag_type = models.PositiveIntegerField(default=0)  # 0 - GENERATED, 1 = STANDARD, etc.
    product_id = models.IntegerField(null=True)
    batch_num = models.IntegerField(null=True)
    email_cc = models.BooleanField(default=True)
    field1 = models.CharField(null=True, default=None, max_length=255)
    field2 = models.CharField(null=True, default=None, max_length=255)
    field3 = models.CharField(null=True, default=None, max_length=255)
    field4 = models.CharField(null=True, default=None, max_length=255)

    def to_base36(self):
        val = self.code
        result = val
        return result

    to_base36.short_description = 'Tag ID'

    def __unicode__(self):
        return u'%s' % self.to_base36()

    # Factory Methods for creating Tags

    @classmethod
    def create(cls, tagcode=None):
        if tagcode is None:
            return cls.create_from_random()
        else:
            return cls.create_from_given_code(tagcode)

    @classmethod
    def create_from_random(cls):
        newtagcode = cls.generate_tagcode()
        newtag = Tag(code=newtagcode)
        newtag.issued_date = datetime.now()
        return newtag

    @classmethod
    def generate_tagcode(cls):
        maxnumber = pow(2, 32)

        randomnum = random.randint(1, maxnumber)
        randomtag = cls.convert_to_base36(randomnum)

        while cls.exists(randomtag) or not cls.is_valid(randomtag):
            randomnum = random.randint(1, maxnumber)
            randomtag = cls.convert_to_base36(randomnum)

        return randomtag

    @classmethod
    def create_from_given_code(cls, newtagcode):
        # don't accept if the tagcode is not valid or is offensive
        if not cls.is_valid(newtagcode) or cls.has_offensive_word(newtagcode):
            return None
        # don't accept if the tagid is already assigned
        if cls.exists(newtagcode):
            return None
        newtag = Tag(code=newtagcode)
        newtag.issued_date = datetime.now()
        return newtag

    # Extension to get method for locating by base36 tagcode
    @classmethod
    def get_from_base36(cls, tagcode):
        found_tag = cls.objects.get(code=tagcode)
        return found_tag

    # Utility methods for converting between decimal and base36

    @classmethod
    def convert_to_base36(cls, val):
        result = ''
        temp = []
        while val >= 36:
            div = val / 36
            mod = val % 36
            temp.append(mod)
            val = div
        temp.append(val)

        while len(temp) > 0:
            digit = temp.pop()
            if digit > 9:
                result = '%s%s' % (result, chr(digit + 55))
            else:
                result = '%s%s' % (result, digit)
        return result

    @classmethod
    def convert_from_base36(cls, val):
        return int(val, 36)

    # Validation methods

    @classmethod
    def is_valid(cls, tagcode):
        if len(tagcode) > 7:
            return False
        elif not re.search('([AEIOU]+)', tagcode) is None:
            return False
        elif not re.search('([0-9BCDFGHJKLMNPQRSTVWXYZ]{1,8})', tagcode) is None:
            tagvalue = cls.convert_from_base36(tagcode)
            if tagvalue <= pow(2, 32): return True
        return False

    @classmethod
    def has_offensive_word(cls, tagcode):
        for word in bad_words.offensive_words:
            pattern = '.*%s.*' % word
            if re.search(pattern, tagcode):
                return word
        return None

    @classmethod
    def exists(cls, tagcode):
        try:
            existing_tag = cls.objects.get(code=tagcode)
        except:
            existing_tag = None
        if existing_tag is not None:
            return True
        return False

    @classmethod
    def create_pool(cls, request, poolsize, tag_type=0):

        if poolsize == 0:
            return None

        tags = []
        for i in range(0, poolsize):
            new_tag = cls.create()
            new_tag.tag_type = tag_type
            new_tag.save()

            # rockettags.main.models.create_log_entry('Generated tag code', 'Generated new tag code %s; number %d of 15; internal id %d' % (new_tag.to_base36(), i+1, new_tag.id), tablename='Tag')

            tags.append(new_tag)

        return tags

    @classmethod
    def obtain_pool(cls, request, poolsize, start_tag_id, end_tag_id):

        tags = cls.objects.all()[start_tag_id - 1:end_tag_id]

        return tags

    @classmethod
    def obtain_pool_of_codes(cls, request, poolsize, start_tag_id, end_tag_id):

        tags = cls.objects.all()[start_tag_id - 1:end_tag_id]

        tagcodes = []
        for current_tag in tags:
            # rockettags.main.models.create_log_entry('Obtained tag code', 'Obtained existing tag code %s; number %d of 15; internal id %d' % (current_tag.to_base36(), i+1, current_tag.id), tablename='Tag')

            tagcode = current_tag.to_base36()

            tagcodes.append(tagcode)

        return tagcodes

    @classmethod
    def update_pool_tag_type(cls, request, start_tag_id, end_tag_id, new_tag_type):

        tags = cls.objects.all()[start_tag_id - 1:end_tag_id]

        for atag in tags:
            atag.tag_type = new_tag_type
            atag.save()

        return tags

    @classmethod
    def update_tag_type_by_code(cls, request, tag_code, new_tag_type):

        atag = cls.objects.get(code=tag_code)

        if not atag is None:
            atag.tag_type = new_tag_type
            atag.save()

        return atag


class FoundMessage(models.Model):
    tag = models.ForeignKey(Tag, related_name='messages', null=True, blank=True, on_delete=models.SET_NULL)
    email = models.EmailField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    firstname = models.CharField(max_length=128, blank=True, null=True)
    lastname = models.CharField(max_length=128, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=64, blank=True, null=True)
    state = models.CharField(max_length=64, blank=True, null=True)
    country = models.CharField(max_length=5, blank=True, null=True)
    contact_method = models.CharField(max_length=255, blank=True, null=True)
    item_description = models.CharField(max_length=255, blank=True, null=True)
    item_location = models.CharField(max_length=255, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)
    generated_date = models.DateTimeField(default=None)
    completed = models.BooleanField(default=False)
    acted_upon_date = models.DateTimeField(null=True, blank=True)


class Subscription(models.Model):
    user = models.ForeignKey(User)
    autorenew = models.PositiveIntegerField(default=0)
    period = models.PositiveIntegerField(default=0)
    creation_date = models.DateTimeField(default=None)
    expiry_date = models.DateTimeField(null=True, blank=True)
    extra1 = models.CharField(max_length=128, null=True, blank=True)  # reserved for future subscription id
    extra2 = models.CharField(max_length=64, null=True, blank=True)  # reserved for annotation about latest change
    extra3 = models.CharField(max_length=32, null=True,
                              blank=True)  # 25022013 reserved for whether or not T&Cs in version 2.0 have been accepted (when subscription happens without a payment)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.user_id


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # preferences
    tag_code = models.CharField(max_length=8, null=True, blank=True)
    tag_format = models.CharField(max_length=12, null=True,
                                  blank=True)  # 28072012 reserved as a flag for whether or not the user wants to be contactable (values are 0 for no, 1 for email)
    tag_colour = models.CharField(max_length=12, null=True,
                                  blank=True)  # 26072012 used for whether a user has been sent their promo for verifying their email
    subscription_period = models.IntegerField(default=0)

    # activation key - 26072012 used to hold whether or not a user has verified their email address
    # is either True or False. If empty or None is False.
    activation_key = models.CharField(max_length=40, null=True, blank=True)
    key_expires = models.DateTimeField(null=True, blank=True)

    # contact details
    # 28072012 These fields are no longer in use
    address1 = models.CharField(max_length=128, null=True, blank=True)
    address2 = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=64, null=True, blank=True)
    state = models.CharField(max_length=64, null=True, blank=True)
    country = models.CharField(max_length=32, null=True, blank=True)
    postcode = models.CharField(max_length=32, null=True, blank=True)
    phone = models.CharField(max_length=32, null=True, blank=True)
    mobile = models.CharField(max_length=32, null=True, blank=True)

    # Important Dates
    creation_date = models.DateTimeField(default=None)
    registration_date = models.DateTimeField(null=True, blank=True)

    status = models.PositiveIntegerField(default=0)
    # 14052012 status takes the following values: 0 - unsubscribed
    #                                             1 - subscribed but waiting for tag
    #                                             2 - fully subscribed
    #                                             3 - quarantine
    is_corporate = models.BooleanField(default=False)
    custom_fields = models.CharField(default=None, null=True, blank=True, max_length=255)

    def __unicode__(self):
        return u'%s' % self.user.get_full_name()


class Promo(models.Model):
    promoter_email = models.CharField(max_length=128)
    customer_email = models.CharField(max_length=128)
    date_time = models.DateTimeField(default=None)
    action = models.PositiveIntegerField(default=0)


def create_log_entry(category, message, subcategory=None, tablename=None):
    if category is None or len(category) == 0:
        return None

    if subcategory is None:
        subcategory = ''

    if tablename is None:
        tablename = ''

    #    new_entry = SiteLog(tablename=tablename, category=category, subcategory=subcategory, message=message)
    #    new_entry.save()

    #    return new_entry

    message_list = message.rsplit('\n')
    new_message = ':'.join(message_list)

    new_entry = '%s|%s|%s|%s|%s|' % (str(datetime.now()), tablename, category, subcategory, new_message)
    f = open(settings.SITE_LOG, 'a')
    try:
        f.write('%s\n' % new_entry)
    finally:
        f.close()

    return new_entry
