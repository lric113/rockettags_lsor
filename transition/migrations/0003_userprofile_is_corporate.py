# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transition', '0002_auto_20151028_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='is_corporate',
            field=models.BooleanField(default=False),
        ),
    ]
