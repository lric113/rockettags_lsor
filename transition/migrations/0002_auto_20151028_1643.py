# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transition', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='assigned_to',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='edit_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='email_cc',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='field1',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='field2',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='field3',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='field4',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='custom_fields',
            field=models.CharField(default=None, max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='foundmessage',
            name='generated_date',
            field=models.DateTimeField(default=None),
        ),
        migrations.AlterField(
            model_name='promo',
            name='date_time',
            field=models.DateTimeField(default=None),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='creation_date',
            field=models.DateTimeField(default=None),
        ),
        migrations.AlterField(
            model_name='tag',
            name='issued_date',
            field=models.DateTimeField(default=None),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='creation_date',
            field=models.DateTimeField(default=None),
        ),
    ]
