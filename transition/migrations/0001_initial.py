# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FoundMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=128, null=True, blank=True)),
                ('phone', models.CharField(max_length=20, null=True, blank=True)),
                ('firstname', models.CharField(max_length=128, null=True, blank=True)),
                ('lastname', models.CharField(max_length=128, null=True, blank=True)),
                ('address', models.CharField(max_length=255, null=True, blank=True)),
                ('city', models.CharField(max_length=64, null=True, blank=True)),
                ('state', models.CharField(max_length=64, null=True, blank=True)),
                ('country', models.CharField(max_length=5, null=True, blank=True)),
                ('contact_method', models.CharField(max_length=255, null=True, blank=True)),
                ('item_description', models.CharField(max_length=255, null=True, blank=True)),
                ('item_location', models.CharField(max_length=255, null=True, blank=True)),
                ('message', models.CharField(max_length=255, null=True, blank=True)),
                ('status', models.PositiveIntegerField(default=0)),
                ('generated_date', models.DateTimeField(default=datetime.datetime(2015, 10, 28, 16, 29, 25, 201751))),
                ('completed', models.BooleanField(default=False)),
                ('acted_upon_date', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Promo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('promoter_email', models.CharField(max_length=128)),
                ('customer_email', models.CharField(max_length=128)),
                ('date_time', models.DateTimeField(default=datetime.datetime(2015, 10, 28, 16, 29, 25, 205107, tzinfo=utc))),
                ('action', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('autorenew', models.PositiveIntegerField(default=0)),
                ('period', models.PositiveIntegerField(default=0)),
                ('creation_date', models.DateTimeField(default=datetime.datetime(2015, 10, 28, 16, 29, 25, 202728))),
                ('expiry_date', models.DateTimeField(null=True, blank=True)),
                ('extra1', models.CharField(max_length=128, null=True, blank=True)),
                ('extra2', models.CharField(max_length=64, null=True, blank=True)),
                ('extra3', models.CharField(max_length=32, null=True, blank=True)),
                ('status', models.PositiveIntegerField(default=0)),
                ('update_date', models.DateTimeField(null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=8)),
                ('issued_date', models.DateTimeField(default=datetime.datetime(2015, 10, 28, 16, 29, 25, 200636))),
                ('registered_date', models.DateTimeField(null=True, blank=True)),
                ('status', models.PositiveIntegerField(default=0)),
                ('tag_type', models.PositiveIntegerField(default=0)),
                ('product_id', models.IntegerField(null=True)),
                ('batch_num', models.IntegerField(null=True)),
                ('user', models.ForeignKey(related_name='tags', on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TestTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag_code', models.CharField(max_length=8, null=True, blank=True)),
                ('tag_format', models.CharField(max_length=12, null=True, blank=True)),
                ('tag_colour', models.CharField(max_length=12, null=True, blank=True)),
                ('subscription_period', models.IntegerField(default=0)),
                ('activation_key', models.CharField(max_length=40, null=True, blank=True)),
                ('key_expires', models.DateTimeField(null=True, blank=True)),
                ('address1', models.CharField(max_length=128, null=True, blank=True)),
                ('address2', models.CharField(max_length=128, null=True, blank=True)),
                ('city', models.CharField(max_length=64, null=True, blank=True)),
                ('state', models.CharField(max_length=64, null=True, blank=True)),
                ('country', models.CharField(max_length=32, null=True, blank=True)),
                ('postcode', models.CharField(max_length=32, null=True, blank=True)),
                ('phone', models.CharField(max_length=32, null=True, blank=True)),
                ('mobile', models.CharField(max_length=32, null=True, blank=True)),
                ('creation_date', models.DateTimeField(default=datetime.datetime(2015, 10, 28, 16, 29, 25, 203926))),
                ('registration_date', models.DateTimeField(null=True, blank=True)),
                ('status', models.PositiveIntegerField(default=0)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='foundmessage',
            name='tag',
            field=models.ForeignKey(related_name='messages', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='transition.Tag', null=True),
        ),
    ]
