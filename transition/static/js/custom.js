$( document ).ready(function() {
    
    if ( $('#flash').length){
        $('#flash').fadeIn("fast", function() {
            setTimeout(function() {
                $('#flash').fadeOut();
            }, 1000);
        });
    }
    
    function new_url(url) {
        event.preventDefault();
        window.location.assign(url);
    }
    
    $('#update_email').click(function() {  
        event.preventDefault();              
        $('#myrockettags').submit();
    });
    
    $('#yes').click(function() {                
        $('#id_yes_no').val(1);
        $('#register_tag3_form').submit();
    });

    $("nav.mobile select").change(function(){ window.location = $(this).val(); });
    
});
