from django.template import Context, loader
from models import create_log_entry
from django.conf import settings
from webob.multidict import MultiDict
from django.utils.timezone import utc
from datetime import timedelta
from transition.models import Tag
import datetime
import pytz
import requests

"""
Legacy email functions written by previous developer
"""


def post_message(responder, responder_text, receiver, subject, text, html, images, bcc):
    file_data = []
    for image in images:
        file_data.append(("inline", open(image, 'rb')))

    r = requests.post(settings.MAILGUN_MESSAGES_URL,
                      auth=("api", settings.MAILGUN_API_KEY),
                      files=MultiDict(file_data),
                      data={
                          "from": "%s <%s>" % (responder_text, responder),
                          "to": ", ".join(receiver) if type(receiver) is list else [receiver],
                          "bcc": bcc,
                          "subject": subject,
                          "text": text,
                          "html": html,
                      })
    return r


def send_promo_email(request, to_email, promo_detail):
    t_plain = loader.get_template('mail/promo.mail')
    t_html = loader.get_template('mail/promo.html')

    images = list()
    images.append('%s/transition/static/img/promo-logo.png' % settings.PROJECT_ROOT)
    images.append('%s/transition/static/img/special-offer-mail.png' % settings.PROJECT_ROOT)

    c = Context({'request': request,
                 'host': settings.MAILER_HOST,
                 'email': to_email,
                 'logo': 'promo-logo.png',
                 'special_offer': 'special-offer-mail.png',
                 'percentage': promo_detail['percentage'],
                 'code': promo_detail['code']})

    response = post_message(
        responder=settings.ACCOUNTS_MAIL_FROM,
        responder_text='RocketTags Mailer',
        receiver=to_email,
        subject='RocketTags Discounts Offer Enclosed',
        text=t_plain.render(c),
        html=t_html.render(c),
        images=images,
        bcc=settings.BLIND_COPY_EMAIL + ',' + request.user.email)

    create_log_entry(category='MAIL', message=response.content, subcategory='PROMOMESSAGE')


def mail_message_to_owner(request, owner, tag, message):
    """
    Send an email to the owner of the tag that this tag was found
    :param request:
    :param owner:
    :param tag:
    :param message:
    :return:
    """

    t_plain = loader.get_template('mail/foundmessage.mail')
    t_html = loader.get_template('mail/foundmessage_mail.html')

    images = list()
    images.append('%s/transition/static/img/rt_email_header.jpg' % settings.PROJECT_ROOT)
    c = Context({'request': request, 'host': settings.MAILER_HOST, 'owner': owner, 'message': message,
                 'logo': 'rt_email_header.jpg', })

    to_addresses = [owner.email]

    # check if tag has an assignee, if so email him the alert
    if tag.email_cc and tag.assigned_to:
        to_addresses.append(tag.assigned_to)

    response = post_message(responder=settings.ACCOUNTS_MAIL_FROM,
                            responder_text='RocketTags Mailer',
                            receiver=to_addresses,
                            subject='Your item has been found!',
                            text=t_plain.render(c),
                            html=t_html.render(c),
                            images=images,
                            bcc=settings.BLIND_COPY_EMAIL)

    create_log_entry(category='MAIL', message=response.content, subcategory='FOUNDMESSAGE')

    return


def mail_message_to_finder(request, finder_email, finder, message):
    """
    Send an email to the owner with a link?
    :param request:
    :param finder_email:
    :param finder:
    :param message:
    :return:
    """

    t_plain = loader.get_template('mail/findermessage.mail')
    t_html = loader.get_template('mail/findermessage.html')
    images = []
    images.append('%s/transition/static/img/rt_email_header.jpg' % settings.PROJECT_ROOT)

    c = Context({'request': request, 'host': settings.MAILER_HOST, 'email': finder_email, 'message': message,
                 'logo': 'rt_email_header.jpg', })

    response = post_message(
        responder=settings.ACCOUNTS_MAIL_FROM,
        responder_text='RocketTags Mailer',
        receiver=finder_email,
        subject='Owner has been notified, thank you from the RocketTags team!',
        text=t_plain.render(c),
        html=t_html.render(c),
        images=images,
        bcc=settings.BLIND_COPY_EMAIL)

    create_log_entry(category='MAIL', message=response.content, subcategory='FINDERMESSAGE')

    return


def mail_test_message_to_owner(request, owner, finder, tag):
    """
    Sends a test message to the owner of the tag and the assignee of the tag
    :param request:
    :param owner:
    :param finder:
    :param tag:
    :return:
    """

    t_plain = loader.get_template('mail/testmessage.mail')
    t_html = loader.get_template('mail/testmessage_mail.html')

    images = list()
    images.append('%s/transition/static/img/rt_email_header.jpg' % settings.PROJECT_ROOT)
    c = Context(
        {'request': request, 'host': settings.MAILER_HOST, 'owner': owner, 'tag': tag, 'logo': 'rt_email_header.jpg', })

    to_addresses = [owner.email]
    # if tag has an assignee then email him too
    if tag.email_cc and tag.assigned_to:
        to_addresses.append(tag.assigned_to)

    response = post_message(responder=settings.ACCOUNTS_MAIL_FROM,
                            responder_text='RocketTags Mailer',
                            receiver=to_addresses,
                            subject='Testing your tag!',
                            text=t_plain.render(c),
                            html=t_html.render(c),
                            images=images,
                            bcc=settings.BLIND_COPY_EMAIL)

    create_log_entry(category='MAIL', message=response.content, subcategory='TESTMESSAGE')


# 20042013 New confirmation email for Adding a Tag
def mail_add_a_tag(request, user, location, extra_fields, tag_code=None):
    t_plain = loader.get_template('mail/add_a_tag.mail')
    t_html = loader.get_template('mail/add_a_tag.html')

    images = list()
    images.append('%s/transition/static/img/rt_email_header.jpg' % settings.PROJECT_ROOT)

    if not tag_code is None:
        tag_code = tag_code.upper()
    c = Context({'request': request,
                 'user': user,
                 'location': location,
                 'host': settings.MAILER_HOST,
                 'full_name': user.get_full_name(),
                 'extra_fields': extra_fields,
                 'logo': 'rt_email_header.jpg',
                 'tag_code': tag_code,
                 })

    if not extra_fields is None:
        c.update(extra_fields)

    response = post_message(
        responder=settings.ACCOUNTS_MAIL_FROM,
        responder_text='RocketTags Mailer',
        receiver=user.email,
        subject='You have Added a New RocketTag',
        text=t_plain.render(c),
        html=t_html.render(c),
        images=images,
        bcc=settings.BLIND_COPY_EMAIL)

    create_log_entry(category='MAIL', message=response.content, subcategory='ADD_A_TAG')

    return True


def mail_list_of_edited_tags(request):
    """
    Function that will mail the owner of the account a list of tags that were edited today

    :param request:
    :param owner:
    :return:
    """

    # today means 24 hours period
    start_date = datetime.datetime.utcnow().replace(tzinfo=utc).replace(hour=0, minute=0, second=0, microsecond=0)
    end_date = start_date + timedelta(days=1)

    # get a list of tags that have being edited today
    edited_today_tags = Tag.objects.filter(user_id=request.user.id, edit_time__range=[start_date, end_date])

    # setup templates
    t_plain = loader.get_template('mail/editedtoday.mail')
    t_html = loader.get_template('mail/editedtoday.mail.html')

    images = list()
    images.append('%s/transition/static/img/rt_email_header.jpg' % settings.PROJECT_ROOT)

    # setup the context to be injected into the mail template
    c = Context({'request': request, 'host': settings.MAILER_HOST, 'owner': request.user, 'logo': 'rt_email_header.jpg',
                 'edited_today_date': datetime.datetime.now().replace(tzinfo=pytz.UTC).date(),
                 'tag_list': edited_today_tags})

    response = post_message(
        responder=settings.ACCOUNTS_MAIL_FROM,
        responder_text='RocketTags Mailer',
        receiver=request.user.email,
        subject='List of edited tags',
        text=t_plain.render(c),
        html=t_html.render(c),
        images=images,
        bcc=settings.BLIND_COPY_EMAIL)

    create_log_entry(category='MAIL', message=response.content, subcategory='EMAILEDITED')

    return


def mail_staff_alert(message):
    """
    Function that will mail an alert message to the staff of rockettags
    :return:
    """

    response = post_message(
        responder=settings.ACCOUNTS_MAIL_FROM,
        responder_text='RocketTags Staff Alert Mailer',
        receiver=['stratos.goudelis@gmail.com', 'terrydunn61@gmail.com'],
        subject='Staff alert!',
        text=message,
        html='',
        images='',
        bcc=settings.BLIND_COPY_EMAIL)

    return response
