# Holds the view method for redeem a code
# plus associated utility methods
#

from django.template import Context, loader, RequestContext, Template
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.conf import settings
from rockettags.fragments import get_fragment_with_links, get_fragment_with_codes, get_codes
from rockettags.fragments import query_string,  get_value
from rockettags.lists import page_names, page_codes
from rockettags.store.views import get_store_fields
from rockettags.main.screenelements import get_bottom_items, get_main_top_menu, get_central_fragment, generate_page_from_fragments 
from rockettags.main.screenelements import get_query_string_fields
from rockettags.tags.models import get_tag_codes, update_primary_tag, get_tag_details, tag_exists, get_tag, register_tag
from rockettags.store import backoffice
from rockettags.links import links
from rockettags.main.models import create_log_entry, get_user
import datetime
from rockettags.tags.find_ancillary import detect_mobile
from rockettags.main.check_codes import get_code_details, has_expired
from rockettags.main.subscriptions import extend_subscription_with_promo
    
    

# Contains the code field
# 09022013 may be extended to include an optional email field
# If email field provided then pass this through
def get_validate_code_fields(request) :
    code = ''
    
    (query_string_fields, next_page) = get_query_string_fields(request)

    if not query_string_fields is None :
        if 'tagid' in query_string_fields :
            code = query_string_fields['tagid']
        elif 'id' in query_string_fields :
            code = query_string_fields['id']

    if request.method == 'POST' :
        if 'code' in request.POST :
            code = request.POST['code']
    
    validate_code_fields = {}   
    validate_code_fields['code'] = code
    
    return validate_code_fields


# Central validation routine for handling entry of tag and promo codes
# Require input:    a pointer to the current request object
#                   the code to check (optional)
#
# Returns a tuple (success, code, code_details, request_details, dummy)
# where:            success - true or false the code is valid for use
#                           if true look at code_details['response_type'] for further details
#                               'response_type':    1 - this tag can be registered
#                                                   2 - this tag is registered and valid and can be found
#                                                   3 - this code is a promo code
#                           if false look at code_details['error'] for detailed message
#
#                   code - the tag or promo code involved
#                   code_details - the tag or product details for the code
#                   request_details - the current session fields, currently only contains the code involved
#                   dummy - a dummy field for future use (gives some information about validation)
#   
def validate_code(request, code=None):

    validate_code_fields = get_validate_code_fields(request)

    # code is drawn from the POST data
    # otherwise it is drawn from the GET query string
    if code is None:
        code = validate_code_fields['code']
    elif len(code) > 0:
        validate_code_fields['code'] = code

    if code is None or len(code) == 0:
        return (None, None, None, None, None)

    details = get_code_details(code)

    success = False
    
    # 09022013 Change test to check if code is None
    # if code is not None then process it
    # Redeem code becomes a gateway point

    # 23022013 Changed so runs whenever code field is not empty
    # code can be filled by either POST or GET
    # 23023013 OLD CODE: if page_name == 'processed' :
    if not code is None and len(code) > 0:
        
        tag_status = get_value(details, 'tag_status')

        if not has_expired(code, details) :

            '''
                        if request.user.is_authenticated() and not request.user.is_anonymous() :

                            if not tag_status is None and tag_status == 0 :
                                
                                dummy = 1
                                return (None, code, None, validate_code_fields, dummy)
                                    
                            else :

                                # 09022013 Remove handling of promo codes
                                # 23022013 For now disable handling of PROMO tags
                                # product_type = get_value(details, 'product_type')
                                # if not product_type is None and product_type == 'PROMO' :
                                #    # extend the users subscription
                                #    (result, error) = extend_subscription_with_promo(request.user, code, details)
                                #
                                #    if result :
                                #        return HttpResponseRedirect(links['mytags_front'])
                                #    else :
                                #        details.update({'error' : error, })
                                dummy = 2
                                return (None, code, None, validate_code_fields, dummy)
            '''                    
            #else :

            if not tag_status is None and tag_status == 0 :        
                tag_typename = get_value(details, 'tag_typename')

                # 11062012 the tag can be either a COMPLIMENTARY or PROMO tag
                # 09022013 the tag can be COMPLIMENTARY or STANDARD+
                # For now both are handled the same way
                # 23022013 introduce the STANDARD+ tag
                # 23022013  OLD CODE: if not tag_typename is None and tag_typename == 'COMPLIMENTARY' :
                if not tag_typename is None and (tag_typename == 'COMPLIMENTARY' or tag_typename == 'STANDARD+') :
                    success = True
                    details.update({'response_type' : 1, })
                    

                # 23022013 For now disable handling of PROMO tags
                #elif not tag_typename is None and tag_typename == 'PROMO' :
                #    return HttpResponseRedirect("%s?id=%s" % (links['store_front'], code))
                
                else :
                    # 23022013 OLD CODE: details.update({'error' : 'this is not a COMPLIMENTARY or PROMO tag', })
                    success = False # for now
                    details.update({'response_type' : 3, })
                    details.update({'error' : 'this is not a COMPLIMENTARY or STANDARD+ tag', })
                    

            else :
                # 26052012 this may be a promo code
                # 11062012 otherwise this may be a generic promo code
                # 09022013 Remove handling of promo codes

                # 23022013 For now disable handling of PROMO tags
                # product_type = get_value(details, 'product_type')

                #if not product_type is None and product_type == 'PROMO' :
                #    return HttpResponseRedirect("%s?promo_code=%s" % (links['store_front'], code))
                #else :
                #    details.update({'error' : 'this is not a PROMO code', })
                if tag_status is None:
                    success = False
                    details.update({'error' : 'this is not a known code', })
                elif tag_status == 1:
                    success = True
                    details.update({'response_type' : 2, })
                    details.update({'error' : 'this code is already registered', })
                    
        else :
            details.update({'error' : 'This code has expired', })

    return (success, code, details, validate_code_fields, None)


# Central gateway routine for handling entry of tag and promo codes
# checks the code using validate_code then acts on the response
# 09022013 can be activated by form or by url call
def redeem_code(request, page_name='front', code=None):

    (validated, code, details, validate_code_fields, dummy) = validate_code(request, code)

    request.META['details'] = details
    request.META['validate_fields'] = validate_code_fields
    #name = request.POST['name']

    if not validated is None and validated is True:
        response_type = None
        if 'response_type' in details: response_type = details['response_type']

        #name = request.POST['name']

        if response_type == 1:
            if request.user.is_authenticated() and not request.user.is_anonymous():
                return HttpResponseRedirect("%s?id=%s" % (links['process_register_tag'], code))
            else:
                return HttpResponseRedirect("%s?id=%s" % (links['process_store_registration'], code))

    if not validated is None:
        page_name = "processed"

    return generate_page_from_fragments(request, 'redeem', page_name, validate_code_fields, details, [], 'template')





