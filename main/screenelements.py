from django.template import Context, loader, RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response

# Handlers for page fragments
from rockettags.lists import bottom_items
from rockettags.lists import bottom_item_links, page_codes, page_names
from rockettags.fragments import get_fragment_with_links, get_fragment_with_codes, get_codes, query_string
   

def get_bottom_items(request, item_list=None) :
    items_dict = {}
    if item_list is None : item_list = bottom_items
    for item_name in item_list :
        link_list = bottom_item_links[item_name]
        items_dict['%s_bottom_fragment' % item_name] = get_fragment_with_links(request, 'fragments', item_name, link_list)
    return items_dict

# Currently only 5 menu items are allowed. If True they are links, if false they are not
def get_main_top_menu(request, tab1='/', tab2='/', tab3='/', tab4='/', tab5='/') :
    codes = { 'tab1_linked' : tab1,
              'tab2_linked' : tab2,
              'tab3_linked' : tab3,
              'tab4_linked' : tab4,
              'tab5_linked' : tab5,
            }
    fragment = get_fragment_with_codes(request, 'fragments', 'coded', 'top_menu_main', codes)
    return fragment

def get_central_fragment(request, location, page_name, main_fields, extra_fields, delete_fields) :
    fragment_codes = get_codes('%s_%s' % (location, page_name))

    if not delete_fields is None:
        for delkey in delete_fields :
            if delkey in fragment_codes :
                del fragment_codes[delkey]
            
    if not main_fields is None : fragment_codes.update(main_fields)
    if not extra_fields is None : fragment_codes.update(extra_fields)
    central_fragment = get_fragment_with_codes(request, '%s/fragments' % location, 'coded', '%s_%s' % (location, page_name), fragment_codes)
    full_page_name = page_names['%s_%s' % (location, page_name)]
    return (central_fragment, full_page_name)

def generate_page_from_fragments(request, location, page_name, main_fields, extra_fields, delete_fields, template_name):

    (central_fragment, full_page_name) = get_central_fragment(request, location, page_name, main_fields, extra_fields, delete_fields)

    template_file = '%s.html' % template_name
    
    t = loader.get_template('%s/%s' % (location, template_file))
    my_codes = {"request" : request,
         'main_top_menu_fragment' : get_main_top_menu(request),
         'central_fragment' : central_fragment,
         'page_name' : full_page_name,
        }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))
    

def show_form_screen(request, form, location, form_name, include_fields, error_string, more_fields={}, exclude_fields=[], template_name='template') :
    extra_fields = {
        'errors' : error_string,
        'process' : get_form_process_link(request, location, form_name, include_fields, []),
        }
    extra_fields.update(more_fields)
    (central_fragment, full_page_name) = get_central_fragment(request, location, form_name, include_fields, extra_fields, exclude_fields)
    main_top_menu = ''
    if location == 'store' :
        main_top_menu = get_main_top_menu(request, tab2='')
    else :
        main_top_menu = get_main_top_menu(request)
        
    d = {
        'form': form,
        'main_top_menu_fragment' : main_top_menu,
        'central_fragment' : central_fragment,
        'page_name' : full_page_name,
        }
    d.update(get_bottom_items(request))
    
    return render_to_response('%s/%s.html' % (location, template_name), d, context_instance=RequestContext(request))   

def show_registration_screen(request, form, location, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, location, 'registration', include_fields, error_string, more_fields, exclude_fields)

def show_confirmation_screen(request, form, location, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, location, 'confirmation', include_fields, error_string, more_fields, exclude_fields)

# 23022013 login screen now allows for subforms
def show_login_screen(request, form, include_fields, error_string, more_fields={}, exclude_fields=[], form_name='login') :
    return show_form_screen(request, form, 'mytags', form_name, include_fields, error_string, more_fields, exclude_fields, template_name='template_login')

def show_lost_credentials_screen(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'mytags', 'lost_credentials', include_fields, error_string, more_fields, exclude_fields, template_name='template_login')

def show_registration_states_screen(request, states, mobile) :
    (central_fragment, full_page_name) = get_central_fragment(request, 'store', 'registration_states', { 'states' : states, 'mobile' : mobile }, {}, [] )
    return HttpResponse(central_fragment)   

def show_registration_cities_screen(request, cities, mobile) :
    (central_fragment, full_page_name) = get_central_fragment(request, 'store', 'registration_cities', { 'cities' : cities, 'mobile' : mobile }, {}, [] )
    return HttpResponse(central_fragment)

def show_found_a_tag_text_screen(request, test) :
    (central_fragment, full_page_name) = get_central_fragment(request, 'tags', 'found_a_tag_text', { 'test' : test }, {}, [] )
    return HttpResponse(central_fragment)   

    


def get_form_process_link(request, location, form_name, include_fields, exclude_fields) :
    fragment_codes = get_codes('%s_%s' % (location, form_name))
    if not include_fields is None : fragment_codes.update(include_fields)
    if 'request' in fragment_codes :
        del fragment_codes['request']
    exclude_fields.append('process')
    process_link = '%s%s' % (page_codes['%s_%s' % (location, form_name)]['process'], query_string(request, fragment_codes, exclude_fields))
    return process_link

def get_registration_process_link(request, location, include_fields, exclude_fields) :
    return get_form_process_link(request, location, 'registration', include_fields, exclude_fields)

def get_confirmation_process_link(request, location, include_fields, exclude_fields) :
    return get_form_process_link(request, location, 'confirmation', include_fields, exclude_fields)

def get_login_process_link(request, location, include_fields, exclude_fields) :
    return get_form_process_link(request, location, 'login', include_fields, exclude_fields)

def get_lost_credentials_process_link(request, location, include_fields, exclude_fields) :
    return get_form_process_link(request, location, 'lost_credentials', include_fields, exclude_fields)
    
def get_query_string_fields(request) :
    query_string_fields = {}
    if len(request.GET) > 0 :
        for some_key in request.GET :
            if some_key in request.GET :
                query_string_fields[some_key] = request.GET[some_key]
        
    next_page = ''
    if 'next' in query_string_fields :
        next_page = query_string_fields['next']
        del query_string_fields['next']
    if len(query_string_fields) == 0 :
        query_string_fields = None
    return (query_string_fields, next_page)
