from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from rockettags.fragments import post_message
from rockettags.main.models import create_log_entry
from django.conf import settings

ACCOUNTS_MAIL_FROM = 'mailer@rockettags.com'
ADMIN_MAIL_FROM = 'mailer@rockettags.com'
NEWSUBS_MAIL_TO = 'newsubs@rockettags.com'
RENEWALS_MAIL_TO = 'renewals@rockettags.com'

def mail_activation_key(request, user, location, activation_key) :

    # Send an email with the confirmation link

    t_plain = loader.get_template('mail/activation.mail')
    t_html = loader.get_template('mail/activation_mail.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'location' : location, 'host' : settings.MAILER_HOST, 'full_name' : user.get_full_name(), 'activation_key' : user.profile.activation_key, 'logo' : logo, })

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Account confirmation'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='ACTIVATION')    

    return True

def mail_account_confirmation(request, user, location, tag_code=None) :

    # Send an email with the confirmation link

    t_plain = loader.get_template('mail/account_confirmation.mail')
    t_html = loader.get_template('mail/account_confirmation.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request, 'location' : location, 'host' : settings.MAILER_HOST, 'full_name' : user.get_full_name(), 'activation_key' : '', 'logo' : logo, 'tag_code' : tag_code })

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Account confirmation'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='CONFIRMATION')    

    return True

def mail_new_password(request, user, new_password) :

    # Send an email with the confirmation link

    t_plain = loader.get_template('mail/new_password.mail')
    t_html = loader.get_template('mail/new_password.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'full_name' : user.get_full_name(), 'email' : user.email, 'password' : new_password, 'logo' : logo, })

    email_from = ADMIN_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new temporary password'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='NEW_PASSWORD')    

    return True

def mail_verify_address_to_user(request, user) :

    t_plain = loader.get_template('mail/verify_address.mail')
    t_html = loader.get_template('mail/verify_address.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'host' : settings.MAILER_HOST, 'full_name' : user.get_full_name(), 'email' : user.email, 'logo' : logo, })

    email_from = ADMIN_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your RocketTags Email Address Verification'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='VERIFICATION')    

    return True

def mail_verify_address_completion_to_user(request, user, promo_code, expiry_date) :

    t_plain = loader.get_template('mail/verify_address_completion.mail')
    t_html = loader.get_template('mail/verify_address_completion.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'host' : settings.MAILER_HOST, 'firstname' : user.first_name, 'email' : user.email, 'promo_code' : promo_code, 'expiry_date' : expiry_date, 'logo' : logo, })

    email_from = ADMIN_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your RocketTags.com Verified Email subscription extension'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='EMAIL_VERIFICATION_COMPLETION')    

    return True


def mail_user_interest(request, email, firstname, lastname) :

    # Send an email with the confirmation link

    t_plain = loader.get_template('mail/user_interest.mail')
    t_html = loader.get_template('mail/user_interest.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'email' : email, 'host' : '23.21.227.115', 'firstname' : firstname, 'lastname' : lastname, 'logo' : logo, })

    email_from = ADMIN_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_to = 'nothome@rockettags.com'
    email_subject = 'A user has shown interest in the site'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, email_to, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='USER_INTEREST')    

    return True


def mail_message_to_owner(request, owner, finder, message) :

    # Send an email to the owner with a link?

    t_plain = loader.get_template('mail/foundmessage.mail')
    t_html = loader.get_template('mail/foundmessage_mail.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'host' : settings.MAILER_HOST, 'owner' : owner, 'message' : message, 'logo' : logo, })

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your item has been found!'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, owner.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='FOUNDMESSAGE')    

    return


def mail_message_to_finder(request, finder_email, finder, message) :

    # Send an email to the owner with a link?

    t_plain = loader.get_template('mail/findermessage.mail')
    t_html = loader.get_template('mail/findermessage.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'host' : settings.MAILER_HOST, 'email' : finder_email, 'message' : message, 'logo' : logo, })

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Thank you for registering this lost item!'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, finder_email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='FINDERMESSAGE')    

    return


def mail_test_message_to_owner(request, owner, finder, tag) :

    # Send an email to the owner with a link?

    t_plain = loader.get_template('mail/testmessage.mail')
    t_html = loader.get_template('mail/testmessage_mail.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'host' : settings.MAILER_HOST, 'owner' : owner, 'tag' : tag, 'logo' : logo, })

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Testing your tag!'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, owner.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TESTMESSAGE')    


def mail_contact_details(request, mailto, email, firstname, lastname, message) :

    # Send an email with the confirmation link

    t_plain = loader.get_template('mail/contact_us.mail')
    t_html = loader.get_template('mail/contact_us.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request, 'email' : email, 'host' : '23.21.227.115', 'firstname' : firstname, 'lastname' : lastname, 'message' : message, 'logo' : logo, })

    email_from = ADMIN_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_to = mailto
    email_subject = 'A user has contacted us'
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, email_to, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='CONTACT_US')    

    return True


def mail_subscription_confirmation(request, user, location, invoice_number, product_details, num_promo_days, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details, tag_code=None) :

    t_plain = loader.get_template('mail/subscription_confirmation.mail')
    t_details = loader.get_template('mail/order_details.html')     
    t_html = loader.get_template('mail/subscription_confirmation.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'num_promo_days' : num_promo_days,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,
                  })
    
    if not extra_fields is None :
        c.update(extra_fields)

    email_header = t_html.render(c)
    email_details = t_details.render(c)        

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Subscription'
    email_body_plain = t_plain.render(c)
    email_body_html = '%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='SUBSCRIPTION')    

    return True

def mail_complimentary_confirmation(request, user, location, invoice_number, product_details, num_promo_days, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details, tag_code=None) :

    t_plain = loader.get_template('mail/complimentary_confirmation.mail')
    t_details = loader.get_template('mail/order_details.html')     
    t_html = loader.get_template('mail/complimentary_confirmation.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'num_promo_days' : num_promo_days,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,                  
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_header = t_html.render(c)
    email_details = t_details.render(c)        

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Complimentary Subscription'
    email_body_plain = t_plain.render(c)
    email_body_html = email_header #'%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='COMPLIMENTARY_SUBSCRIPTION')    

    return True

    
def mail_resubscription_confirmation(request, user, location, invoice_number, product_details, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details) :

    t_plain = loader.get_template('mail/resubscription_confirmation.mail')
    t_details = loader.get_template('mail/order_details.html') 
    t_html = loader.get_template('mail/resubscription_confirmation.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,                  
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_header = t_html.render(c)
    email_details = t_details.render(c)        

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your RocketTags Subscription Renewal'
    email_body_plain = t_plain.render(c)
    email_body_html = '%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='RESUBSCRIPTION')    

    return True
    
def mail_tags_purchase_confirmation(request, user, location, invoice_number, product_details, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details) :

    t_plain = loader.get_template('mail/tags_purchase_confirmation.mail')
    t_details = loader.get_template('mail/order_details.html')     
    t_html = loader.get_template('mail/tags_purchase_confirmation.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,                  
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_header = t_html.render(c)
    email_details = t_details.render(c)        

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new Rocket Tags'
    email_body_plain = t_plain.render(c)
    email_body_html = '%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TAGS_PURCHASE')    

    return True

def mail_tags_purchase_to_admin(request, user, location, invoice_number, product_details, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details) :

    t_plain = loader.get_template('mail/tags_purchase_to_admin.mail')
    t_html = loader.get_template('mail/tags_purchase_to_admin.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,                  
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_from = ADMIN_MAIL_FROM
    email_to = RENEWALS_MAIL_TO
    email_from_name ='RocketTags Mailer'
    email_subject = 'New RocketTags Tags for customer %s with invoice number %s' % (user.email, invoice_number)
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, email_to, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TAGS_PURCHASE_TO_ADMIN')    

    return True
    
def mail_initial_purchase_to_admin(request, user, location, invoice_number, product_details, extra_fields, payment_details, extended_payment_details, order_details, shipping_address_details, billing_address_details) :

    t_plain = loader.get_template('mail/initial_purchase_to_admin.mail')
    t_html = loader.get_template('mail/initial_purchase_to_admin.html')
    logo = 'rt_email_header.jpg'
    c = Context({ 'request' : request,
                  'user' : user,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'invoice_number' : invoice_number,
                  'product_details' : product_details,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'payment_details' : payment_details,
                  'extended_payment_details' : extended_payment_details,
                  'order_details' : order_details,
                  'shipping_address_details' : shipping_address_details,
                  'billing_address_details' : billing_address_details,                  
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_from = ADMIN_MAIL_FROM
    email_to = NEWSUBS_MAIL_TO
    email_from_name ='RocketTags Mailer'
    email_subject = 'New RocketTags Subscription for customer %s with invoice number %s' % (user.email, invoice_number)
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, email_to, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TAGS_PURCHASE_TO_ADMIN')    

    return True
    

# 21032013 New confirmation email for STANDARD+ tag types
def mail_standardplus_confirmation(request, user, location, num_subscription_days, extra_fields, tag_code=None) :

    t_plain = loader.get_template('mail/standardplus_confirmation.mail')
    t_html = loader.get_template('mail/standardplus_confirmation.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,                  
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'num_subscription_days' : num_subscription_days,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_body = t_html.render(c)

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Subscription'
    email_body_plain = t_plain.render(c)
    email_body_html = email_body #'%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='STANDARDPLUS_SUBSCRIPTION')    

    return True
    
# 21032013 New confirmation email for Tell a Friend signors
def mail_tellafriend_signor(request, user, location, num_extension_days, extra_fields, tag_code=None) :

    t_plain = loader.get_template('mail/tellafriend_signor.mail')
    t_html = loader.get_template('mail/tellafriend_signor.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,                  
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'num_extension_days' : num_extension_days,
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_body = t_html.render(c)

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'Your new RocketTags Extension'
    email_body_plain = t_plain.render(c)
    email_body_html = email_body #'%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TELLAFRIEND_SIGNOR')    

    return True
    
# 21032013 New confirmation email for Tell a Friend signees
def mail_tellafriend_signee(request, user, email, location, extra_fields, tag_code=None) :

    t_plain = loader.get_template('mail/tellafriend_signee.mail')
    t_html = loader.get_template('mail/tellafriend_signee.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,
                  'email' : email,
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_body = t_html.render(c)

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'You have been recommended to us'
    email_body_plain = t_plain.render(c)
    email_body_html = email_body #'%s\n<br>\n<br>\n%s' % (email_header, email_details)

    response = post_message(email_from, email_from_name, email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='TELLAFRIEND_SIGNEE')    

    return True
    
# 21032013 Generic method for generating a mailer
def mail_generic(request, email, location, template_name, from_name, subject, extra_fields=None, user=None) :

    # Send a generic email

    t_plain = loader.get_template('mail/%s.mail' % template_name)
    t_html = loader.get_template('mail/%s.html' % template_name)
    logo = 'rt_email_header.jpg'
    the_email = email
    c = Context({ 'request' : request, 'location' : location, 'host' : settings.MAILER_HOST, 'logo' : logo, })
    if not user is None:
        c.update({'full_name': user.get_full_name(), })
        the_email = user.email
    if not extra_fields is None:
        c.update({'extra_fields':extra_fields, })
        c.update(extra_fields)

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name = from_name
    email_subject = subject
    email_body_plain = t_plain.render(c)
    email_body_html = t_html.render(c)

    response = post_message(email_from, email_from_name, the_email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory=template_name)    

    return True

# 20042013 New confirmation email for Adding a Tag
def mail_add_a_tag(request, user, location, extra_fields, tag_code=None) :

    t_plain = loader.get_template('mail/add_a_tag.mail')
    t_html = loader.get_template('mail/add_a_tag.html')
    logo = 'rt_email_header.jpg'
    if not tag_code is None: tag_code = tag_code.upper()
    c = Context({ 'request' : request,
                  'user' : user,                  
                  'location' : location,
                  'host' : settings.MAILER_HOST,
                  'full_name' : user.get_full_name(),
                  'extra_fields' : extra_fields,
                  'logo' : logo,
                  'tag_code' : tag_code,
                  })

    if not extra_fields is None :
        c.update(extra_fields)

    email_body = t_html.render(c)

    email_from = ACCOUNTS_MAIL_FROM
    email_from_name ='RocketTags Mailer'
    email_subject = 'You have Added a New RocketTag'
    email_body_plain = t_plain.render(c)
    email_body_html = email_body 

    response = post_message(email_from, email_from_name, user.email, email_subject, email_body_plain, email_body_html, logo)

    create_log_entry(category='MAIL', message=response.content, subcategory='ADD_A_TAG')    

    return True
