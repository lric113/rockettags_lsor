# Utility methods for working with subscriptions
# subscriptions are actually stored in the store submodel behind the backoffice facade

from django.template import Context, loader, RequestContext, Template
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.conf import settings
from rockettags.fragments import get_fragment_with_links, get_fragment_with_codes, get_codes
from rockettags.fragments import query_string, get_value
from rockettags.lists import page_names, page_codes
#from rockettags.store.views import get_store_fields
from rockettags.main.screenelements import get_bottom_items, get_main_top_menu, get_central_fragment 
from rockettags.main.screenelements import get_query_string_fields
from rockettags.tags.models import get_tag_codes, update_primary_tag, get_tag_details, tag_exists, get_tag, register_tag
from rockettags.store import backoffice
from rockettags.links import links
from rockettags.main.models import create_log_entry, get_user
import datetime

# Utility method to extend a subscription given a promo code
# or tag code with associated promo information
def extend_subscription_with_promo(user, code, details) :

    if user is None :
        return (False, 'invalid user')
    
    tag_status = get_value(details, 'tag_status')
    if not tag_status is None :
        tag_typename = get_value(details, 'tag_typename')
        if tag_typename is None or tag_typename != 'PROMO' :
            return (False, 'this tag is not of PROMO type')
        tag = get_tag(code)
        product_code = tag.product.product_code
        product_details = backoffice.get_product_details(product_code)
        
        subscription_period = product_details['product_subscription_period']
        
    else :
        product_code = get_value(details, 'product_code')
        product_type = get_value(details, 'product_type')
        if product_type is None or product_type != 'PROMO' :
            return (False, 'this is not a PROMO code')
        subscription_period = details['product_subscription_period']

    # 04072012 check that this user hasn't already applied this same promo code
    subscription_details = backoffice.get_subscription_details(user.email)
    if subscription_details is None :
        return (False, 'This user does not have a subscription record')

    previous_promo_code = get_value(subscription_details, 'promo_code')

    if not previous_promo_code is None and product_code == previous_promo_code :
        return (False, 'This user has already applied this promo code')
    
    current_profile = user.get_profile()

    remaining_period = 0
    if datetime.datetime.now() < current_profile.expiry_date :
      remaining_delta = current_profile.expiry_date - datetime.datetime.now()
      remaining_period = remaining_delta.days + 1

    # This is the total of the subscription period plus any PROMO extension
    total_subscription_period = int(subscription_period) + remaining_period
                
    current_profile.expiry_date = datetime.datetime.now() + datetime.timedelta(days=total_subscription_period)
    current_profile.subscription_period = total_subscription_period

    current_profile.save()

    # update subscription record status to be the same as the current_profile.status
    # update the invoice number for the subscription if it hasn't already been set (as in the case of COMPLIMENTARY tags)
    # update the product code for resubscription

    annotate_text = None
    if subscription_period > 0 :
      annotate_text = "%s" % (product_code)
    
    backoffice.update_subscription(user.email, None, None, subscription_period, current_profile.status, annotate_text=annotate_text)

    create_log_entry('UPDATE_SUBSCRIPTION', 'Addition of PROMO of period %d created for user %s with resulting status %d' % (int(subscription_period), user.email, current_profile.status))

    return (True, '')




