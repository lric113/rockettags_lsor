from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from forms import ConfirmationForm
from django.conf import settings
from rockettags.fragments import query_string
from rockettags.main.screenelements import show_confirmation_screen
from rockettags.main.screenelements import get_query_string_fields
from rockettags.main.models import UserProfile
import datetime
    

def get_confirmation_fields(request) :

    if request.method != 'POST' :
        return None

    activation_key = ''
    password = ''
    confirmed = False
    confirmed_string = 'False'

    try :
        activation_key = request.POST['activation_key']
        if 'password' in request.POST : password = request.POST['password']
        confirmed_string = request.POST['confirmed']
    except :
        return None

    if confirmed_string == 'True' :
        confirmed = True
        
    confirmation_fields = {}
    confirmation_fields['activation_key'] = activation_key
    confirmation_fields['password'] = password
    confirmation_fields['confirmed'] = confirmed

    return confirmation_fields

def activate_user(activation_key) :
    current_profile = None
    try :
        current_profile = UserProfile.objects.get(activation_key=activation_key)
    except :
        current_profile = None

    if current_profile is None :
        return (False, 'Unknown activation key')

    if current_profile.key_expires < datetime.datetime.today():
        return (False, 'Activation key has expired')

    current_user = current_profile.user
    current_user.is_active = True
    current_user.save()
    return (True, '')
    
def get_email_from_activation_key(activation_key) :
    current_profile = None
    try :
        current_profile = UserProfile.objects.get(activation_key=activation_key)
    except :
        current_profile = None

    if current_profile is None :
        return ''

    current_user = current_profile.user
    return current_user.email



def confirm(request, location='mytags') :

    (query_string_fields, next_page) = get_query_string_fields(request)
    
    if len(next_page) == 0 and request.method == 'POST' :
        if 'next' in request.POST :
            next_page = request.POST['next']
            
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))

    activation_key = ''
    if 'activation_key' in query_string_fields :
        activation_key = query_string_fields['activation_key']
    if len(activation_key) == 0 and request.method == 'POST' :
        if 'activation_key' in request.POST :
            activation_key = request.POST['activation_key']

    if request.method == 'POST' :
        form = ConfirmationForm(None, request.POST, auto_id=True) # A form bound to the POST data

        confirmation_fields = get_confirmation_fields(request)

        if len(activation_key) == 0 :
            return show_confirmation_screen(request, form, location, query_string_fields, 'No activation key given', {'confirmed' : False, 'authenticated' : False, })

        confirmed = confirmation_fields['confirmed']

        if not confirmed :
            (activation_result, error_string) = activate_user(activation_key)
            if not activation_result :
                return show_confirmation_screen(request, form, location, query_string_fields, error_string, {'confirmed' : False, 'authenticated' : False, })
            else :
                return show_confirmation_screen(request, form, location, query_string_fields, '', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })

        # Now the user should be confirmed and we are responding to the final screen. Log him in...

        # find username from email
        email = get_email_from_activation_key(activation_key)
        if len(email) == 0 :
            return show_confirmation_screen(request, form, location, query_string_fields, 'Problem obtaining email from activation key', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })
        
        try :
            possible_user = User.objects.get(email=email)
            username = possible_user.username                
        except :
            username = ''

        if username is None or len(username) == 0 :
            # Show an error page
            return show_confirmation_screen(request, form, location, query_string_fields, 'Invalid email address', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })            

        password = confirmation_fields['password']
        if len(password) == 0 :
            return show_confirmation_screen(request, form, location, query_string_fields, 'Empty password', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })

        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # Correct password, and the user is marked "active"
            auth.login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect(next_page)
        else:
            # Show an error page
            return show_confirmation_screen(request, form, location, query_string_fields, 'Invalid authentication details - password is wrong', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })      
        
    else :
        form = ConfirmationForm(None, None, auto_id=True) # A form not bound

        if len(activation_key) == 0 :
            return show_confirmation_screen(request, form, location, query_string_fields, 'No activation key given', {'confirmed' : False, 'expired' : False, 'authenticated' : False, })

        (activation_result, error_string) = activate_user(activation_key)
        if not activation_result :
            return show_confirmation_screen(request, form, location, query_string_fields, error_string, {'confirmed' : False, 'authenticated' : False, })
        else :
            return show_confirmation_screen(request, form, location, query_string_fields, '', {'confirmed' : True, 'authenticated' : False, 'activation_key' : activation_key, })


    
