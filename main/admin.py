from models import UserProfile, SiteLog, AddressDetails
from django.contrib import admin

admin.site.register(UserProfile)

class SiteLogAdmin(admin.ModelAdmin):
    # ...
    list_display = ('category', 'tablename', 'creation_date')

class AddressDetailsAdmin(admin.ModelAdmin):
    # ...
    list_display = ('user', 'address_type')


admin.site.register(SiteLog, SiteLogAdmin)
admin.site.register(AddressDetails, AddressDetailsAdmin)

