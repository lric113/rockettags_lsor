from django.template import Context, loader, RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from forms import LoginForm, LostCredentialsForm
from django.conf import settings
from rockettags.fragments import query_string
from rockettags.main.screenelements import show_login_screen, show_lost_credentials_screen
from rockettags.main.screenelements import get_query_string_fields
from rockettags.main.models import create_log_entry, test_if_subscribed, generate_new_password
import datetime
from rockettags.tags.models import get_tag_user, get_tag_details
from rockettags.links import links
from rockettags.main.mailers import mail_new_password
    
# 23022013 With Rev 2.0 there are two login forms - admin and normal
# admin works as it does now
# normal doesn't request a password but instead a valid registered tag code
#

def validate_login_input(request, check_password=True) :

    if request.method != 'POST' :
        return (False, 'Not in POST mode')

    email = ''
    password = ''

    try :
        email = request.POST['email']
        
    except :
        return (False, 'Not all required fields present')

    if 'password' in request.POST: password = request.POST['password']

    if len(email) == 0 :
        return (False, 'Email field is empty')

    if check_password and len(password) == 0 :
        return (False, 'Password field is empty')

    return (True, '')

def get_login_fields(request) :

    if request.method != 'POST' :
        return None

    email = ''
    password = ''
    tagcode = ''

    if 'email' in request.POST: email = request.POST['email']
    if 'password' in request.POST: password = request.POST['password']
    if 'tagcode' in request.POST: tagcode = request.POST['tagcode']

    # 30052013 Treat email/username as case insensitive
    if not email is None: email = email.lower()

    login_fields = {}
    login_fields['email'] = email
    login_fields['password'] = password
    login_fields['tagcode'] = tagcode

    return login_fields

# 23022013 This is the original login form now used only for admin level access
def login_admin(request) :

    # 10062012 Login can be redirected to whatever page in MyTags we want it to go to
    # based on the value of 'next' passed in to the query string
    (query_string_fields, orig_next_page) = get_query_string_fields(request)

    next_page = ''
    if len(orig_next_page) == 0 :
        if request.method == 'POST' and 'next' in request.POST:
            next_page = request.POST['next']
        else :
            next_page = links['mytags_front']
    else :
        next_page = orig_next_page

    #request.META['orig_next_page'] = orig_next_page
    #request.META['next_page'] = next_page
    #name = request.POST['name']
        
    if request.method == "POST" and 'email' in request.POST :
        email = request.POST['email']
        if len(email) > 0 :
            query_string_fields['email'] = email
        
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))    

    if request.method == 'POST' :
        form = LoginForm(None, request.POST, auto_id=True) # A form bound to the POST data

        login_fields = get_login_fields(request)

        # find username from email
        email = login_fields['email']
        if len(email) == 0 :
            # 16022013 go to login screen with tag code not password
            return show_login_screen(request, form, query_string_fields, 'Empty email address', {'form' : form,})
        
        try :
            possible_user = User.objects.get(email=email)
            username = possible_user.username                
        except :
            username = ''

        if username is None or len(username) == 0 :
            # Show an error page
            # 16022013 go to login screen with tag code not password
            return show_login_screen(request, form, query_string_fields, 'Invalid email address', {'form' : form,})

        # 16022013 If login_fields['tag_code'] is not empty
        # 16022013 then check tag code belongs to user
        # 16022013 If yes then try to login with password as username so set password = username
        # 16022013 If no login with explicit password
        # 16022013 If authenticate fails go to login screen with explicit password
        
        password = login_fields['password']
        if len(password) == 0 :
            return show_login_screen(request, form, query_string_fields, 'Empty password', {'form' : form, 'email' : login_fields['email'], })

        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # Correct password, and the user is marked "active"
            # 14052012 it is not enough to have correct credentials and be active you must also be currently subscribed

            # 14052012 Test the expiry date for the user
            # If the user is not subscribed and in quarantine send an email
            # else if not subscribed don't log them in
            subscribed_status = test_if_subscribed(user)

            if subscribed_status == 0 :
                create_log_entry('User Login', 'User %s is not currently subscribed. He may have exceeded expiry date and is outside of quarantine' % (user.email), tablename='User')
                return show_login_screen(request, form, query_string_fields, '%s is not currently subscribed' % user.email, {'form' : form, 'email' : login_fields['email'], })                                                             
            elif subscribed_status == 1 :
                create_log_entry('User Login', 'User %s is not yet subscribed.' % (user.email), tablename='User')
                return show_login_screen(request, form, query_string_fields, '%s is not yet subscribed' % user.email, {'form' : form, 'email' : login_fields['email'], })                                                             
            elif subscribed_status == 3 :
                create_log_entry('User Login', 'User %s has exceeded expiry date but is still in quarantine' % (user.email), tablename='User')
                # 14052012 send email to user warning he needs to extend subscription
                
            auth.login(request, user)

            create_log_entry('User Login', 'User %s has been successfully logged into the system' % (user.email), tablename='User')
            # Redirect to a success page.

            return HttpResponseRedirect(next_page)
        else:
            # Show an error page
            create_log_entry('User Login', 'User %s failed system authentication' % (login_fields['email']), tablename='User')
            
            return show_login_screen(request, form, query_string_fields, 'Invalid authentication details - password is wrong', {'form' : form, 'email' : login_fields['email'], })      
        
    else :
        form = LoginForm(None, None, auto_id=True) # A form not bound

        # 10062012 Login can be redirected to whatever page in MyTags we want it to go to
        # Simply grab the original 'next' from the query string and pass it back into the query_string_fields dict
        # and set the 'next' value in the additional fields dict before calling show_login_screen
        # 13092012 Fixed way in which next parameter is used so that following login one is always directed
        # to the page given in the next parameter
        if len(orig_next_page) > 0:
            if query_string_fields is None :
                query_string_fields = {}
            
            query_string_fields['next'] = orig_next_page
            
        return show_login_screen(request, form, query_string_fields, '', more_fields={'form' : form, 'next' : orig_next_page, }, exclude_fields=[], form_name='login_admin')


# 23022013 This is the new login form which doesn't require a password but does require a
# valid tag code
def login_normal(request) :

    # 10062012 Login can be redirected to whatever page in MyTags we want it to go to
    # based on the value of 'next' passed in to the query string
    (query_string_fields, orig_next_page) = get_query_string_fields(request)

    next_page = ''
    if len(orig_next_page) == 0 :
        if request.method == 'POST' and 'next' in request.POST:
            next_page = request.POST['next']
        else :
            next_page = links['mytags_front']
    else :
        next_page = orig_next_page

    #request.META['orig_next_page'] = orig_next_page
    #request.META['next_page'] = next_page
    #name = request.POST['name']
        
    if request.method == "POST" and 'email' in request.POST :
        email = request.POST['email']
        if len(email) > 0 :
            query_string_fields['email'] = email
        
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))    

    if request.method == 'POST' :
        form = LoginForm(None, request.POST, auto_id=True) # A form bound to the POST data

        login_fields = get_login_fields(request)

        # find username from email
        email = login_fields['email']
        if len(email) == 0 :
            # 16022013 go to login screen with tag code not password
            return show_login_screen(request, form, query_string_fields, 'Empty email address', {'form' : form,})
        
        try :
            possible_user = User.objects.get(email=email)
            username = possible_user.username                
        except :
            username = ''

        if username is None or len(username) == 0 :
            # Show an error page
            # 16022013 go to login screen with tag code not password
            return show_login_screen(request, form, query_string_fields, 'Invalid email address', {'form' : form,})

        # 16022013 If login_fields['tag_code'] is not empty
        # 16022013 then check tag code belongs to user
        # 16022013 If yes then try to login with password as username so set password = username
        # 16022013 If no login with explicit password
        # 16022013 If authenticate fails go to login screen with explicit password
        
        tagcode = login_fields['tagcode']
        if len(tagcode) == 0 :
            return show_login_screen(request, form, query_string_fields, 'Empty tag code', {'form' : form, 'email' : login_fields['email'], })

        # 23022013 check validity of tag code here
        tag_details = get_tag_details(tagcode)
        tag_user = get_tag_user(tagcode)

        if tag_details is None:
            return show_login_screen(request, form, query_string_fields, 'Invalid tag code', {'form' : form, 'email' : login_fields['email'], 'tagcode' : tagcode, })

        if tag_details['tag_status'] == 0:
            return show_login_screen(request, form, query_string_fields, 'Unregistered tag code', {'form' : form, 'email' : login_fields['email'], 'tagcode' : tagcode, })

        if tag_user is None:
            return show_login_screen(request, form, query_string_fields, 'Unregistered tag code', {'form' : form, 'email' : login_fields['email'], 'tagcode' : tagcode, })

        if tag_user.email != email:
            return show_login_screen(request, form, query_string_fields, 'This is not your tag code', {'form' : form, 'email' : login_fields['email'], 'tagcode' : tagcode, })


        # 23022013 login/authenticate with password as username
        user = auth.authenticate(username=username, password=username)
        if user is not None and user.is_active:
            # Correct password, and the user is marked "active"
            # 14052012 it is not enough to have correct credentials and be active you must also be currently subscribed

            # 14052012 Test the expiry date for the user
            # If the user is not subscribed and in quarantine send an email
            # else if not subscribed don't log them in
            subscribed_status = test_if_subscribed(user)

            if subscribed_status == 0 :
                create_log_entry('User Login', 'User %s is not currently subscribed. He may have exceeded expiry date and is outside of quarantine' % (user.email), tablename='User')
                return show_login_screen(request, form, query_string_fields, '%s is not currently subscribed' % user.email, {'form' : form, 'email' : login_fields['email'], })                                                             
            elif subscribed_status == 1 :
                create_log_entry('User Login', 'User %s is not yet subscribed.' % (user.email), tablename='User')
                return show_login_screen(request, form, query_string_fields, '%s is not yet subscribed' % user.email, {'form' : form, 'email' : login_fields['email'], })                                                             
            elif subscribed_status == 3 :
                create_log_entry('User Login', 'User %s has exceeded expiry date but is still in quarantine' % (user.email), tablename='User')
                # 14052012 send email to user warning he needs to extend subscription
                
            auth.login(request, user)

            create_log_entry('User Login', 'User %s has been successfully logged into the system' % (user.email), tablename='User')
            # Redirect to a success page.

            return HttpResponseRedirect(next_page)
        else:
            # Show an error page
            create_log_entry('User Login', 'User %s failed system authentication' % (login_fields['email']), tablename='User')
            
            return show_login_screen(request, form, query_string_fields, 'Invalid authentication details - password is wrong', {'form' : form, 'email' : login_fields['email'], })      
        
    else :
        form = LoginForm(None, None, auto_id=True) # A form not bound

        # 10062012 Login can be redirected to whatever page in MyTags we want it to go to
        # Simply grab the original 'next' from the query string and pass it back into the query_string_fields dict
        # and set the 'next' value in the additional fields dict before calling show_login_screen
        # 13092012 Fixed way in which next parameter is used so that following login one is always directed
        # to the page given in the next parameter
        if len(orig_next_page) > 0:
            if query_string_fields is None :
                query_string_fields = {}
            
            query_string_fields['next'] = orig_next_page
            
        return show_login_screen(request, form, query_string_fields, '', {'form' : form, 'next' : orig_next_page, })


        
def validate_lost_credentials_input(request) :

    if request.method != 'POST' :
        return (False, 'Not in POST mode')

    email = ''
    tagid = ''

    try :
        email = request.POST['email']
        tagid = request.POST['tagid']
    except :
        return (False, 'Not all required fields present')

    if len(email) == 0 and len(tagid) == 0:
        return (False, 'No credentials provided')

    return (True, '')

def get_lost_credentials_fields(request) :

    if request.method != 'POST' :
        return None

    email = ''
    tagid = ''

    try :
        email = request.POST['email']
        tagid = request.POST['tagid']
    except :
        return None

    if 'Your email' in email :
        email = ''
    if 'Your RocketTag Code' in tagid :
        tagid = ''

    lost_credentials_fields = {}
    lost_credentials_fields['email'] = email
    lost_credentials_fields['tagid'] = tagid

    return lost_credentials_fields




def lost_credentials(request) :

    (query_string_fields, next_page) = get_query_string_fields(request)
    
    if len(next_page) == 0 and request.method == 'POST' :
        if 'next' in request.POST :
            next_page = request.POST['next']
            
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))

    if request.method == 'POST' :
        form = LostCredentialsForm(None, request.POST, auto_id=True) # A form bound to the POST data

        lost_credentials_fields = get_lost_credentials_fields(request)

        # find username from email
        email = lost_credentials_fields['email']
        tagid = lost_credentials_fields['tagid']
        if len(email) == 0 and len(tagid) == 0 :
            return show_lost_credentials_screen(request, form, query_string_fields, 'Empty email address and tag code', {'form' : form,})

        if len(email) > 0 :
            try :
                user = User.objects.get(email=email)
            except :
                user = None
                
        elif len(tagid) > 0 :
            try :
                # find user with tag id
                user = get_tag_user(tagid)
            except :
                user = None                        

        if user is None :
            # Show an error page
            return show_lost_credentials_screen(request, form, query_string_fields, 'Invalid email address or tag code', {'form' : form,})            

        # generate new password
        new_password = generate_new_password()                
        
        # set password to new password
        user.set_password(new_password)
        user.save()

        # mail message to user containing new password 
        mail_new_password(request, user, new_password)

        create_log_entry('User Lost Credentials', 'User %s has had new credentials mailed to them' % (user.email), tablename='User')

        return HttpResponseRedirect(next_page)
       
    else :
        form = LostCredentialsForm(None, None, auto_id=True) # A form not bound

        return show_lost_credentials_screen(request, form, query_string_fields, '', {'form' : form,})


        



def logout(request):
    next_page = "/"
    auth.logout(request)
    create_log_entry('User Logout', 'User %s has been logged out of the system' % (user.email), tablename='User')
    # Redirect to a success page.
    return HttpResponseRedirect("/")


