from django.utils.translation import ugettext_lazy as _
from django import forms
from django.contrib.auth.forms import AuthenticationForm

class LoginForm(AuthenticationForm) :
    email = forms.CharField(max_length=128)
    username = forms.CharField(label=_("Username"), max_length=128, required=False)

    # At some point do custom validation
    def is_valid(self) :
        return True

class LostCredentialsForm(forms.Form) :
    email = forms.CharField(max_length=128)
    username = forms.CharField(label=_("Username"), max_length=128, required=False)

    # At some point do custom validation
    def is_valid(self) :
        return True

class RegistrationForm(forms.Form) :
    email = forms.CharField(max_length=128)
    email2 = forms.CharField(label=_("E-mail (again)"), max_length=128, required=False)
    firstname = forms.CharField(max_length=128, required=False)
    lastname = forms.CharField(max_length=128, required=False)
    password1 = forms.CharField(max_length=128, required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=128, required=True, widget=forms.PasswordInput)
    country = forms.CharField(max_length=3, required=False)
    state = forms.CharField(max_length=3, required=False)
    
    # At some point do custom validation
    def is_valid(self) :
        return True

class ConfirmationForm(forms.Form) :
    activation_key = forms.CharField(max_length=128, required=False)    
    email = forms.CharField(max_length=128, required=False)    
    password = forms.CharField(max_length=128, required=False, widget=forms.PasswordInput)
    
    # At some point do custom validation
    def is_valid(self) :
        return True
