from django.template import Context, loader, RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from forms import RegistrationForm
from django.conf import settings
from rockettags.fragments import query_string
from rockettags.main.screenelements import show_registration_screen, show_confirmation_screen, show_registration_states_screen, show_registration_cities_screen
from rockettags.main.screenelements import get_query_string_fields
from rockettags.main.models import check_duplicate_username, check_duplicate_email, register_user, setup_activation_key, create_log_entry
import datetime, sys
import re
from rockettags.main.dropdowns import get_countries, get_states, get_cities
from rockettags.fragments import get_codes
from rockettags.tags.models import get_tag, get_tag_type
from rockettags.links import links
from rockettags.main.mailers import mail_account_confirmation, mail_complimentary_confirmation, mail_standardplus_confirmation
from rockettags.tags.views import register_tag_view
    

def validate_email(request, email):

    if len(email) == 0 :
        return (False, 'Email field is empty')

    if not re.search('(\@{1,1})', email) :
        return (False, 'Invalid email address - no @ symbol')

    if not re.search('([0-9a-zA-Z\@\_\-\.]{4,128})', email) :
        return (False, 'Invalid email address - invalid length')

    if re.search('([\,\'\"\;\:\+\=]+)', email) :
        return (False, 'Invalid email address - invalid characters')

    return (True, '')
    
def validate_registration_input(request, check_password=True) :

    if request.method != 'POST' :
        return (False, 'Not in POST mode')

    email = ''
    email2 = ''
    firstname = ''
    lastname = ''
    password1 = ''
    password2 = ''

    try :
        email = request.POST['email']
        email2 = request.POST['email2']
    except :
        return (False, 'Not all required fields present')

    if 'firstname' in request.POST: firstname = request.POST['firstname']
    if 'lastname' in request.POST: lastname = request.POST['lastname']
    if 'password1' in request.POST: password1 = request.POST['password1']
    if 'password2' in request.POST: password2 = request.POST['password2']

    #if len(email) == 0 :
    #    return (False, 'Email field is empty')

    #if not re.search('(\@{1,1})', email) :
    #    return (False, 'Invalid email address - no @ symbol')

    #if not re.search('([0-9a-zA-Z\@\_\-\.]{4,128})', email) :
    #    return (False, 'Invalid email address - invalid length')

    #if re.search('([\,\'\"\;\:\+\=]+)', email) :
    #    return (False, 'Invalid email address - invalid characters')

    (result, error_string) = validate_email(request, email)
    if not result: return (result, error_string)

    if len(email2) == 0 :
        return (False, 'Repeat email field is empty')

    if email != email2 :
        return (False, 'Email fields do not match')

    if check_password :
        if len(password1) == 0 :
            return (False, 'Password field is empty')

        if password1 != password2 :
            return (False, 'Passwords do not match')

    return (True, '')


# 23022013 Redo obtain registration fields
# Only the email and email confirm fields are certain to be present
def get_registration_fields(request) :

    if request.method != 'POST' :
        return None

    email = ''
    email2 = ''
    firstname = ''
    lastname = ''
    password = ''
    contact_method = '0'
    # 25022013 Add new field for whether or not the user has accepted the T&Cs
    accepted_tcs = '0'

    try :
        email = request.POST['email']
        email2 = request.POST['email2']
    except :
        return None

    # 30052013 Treat email as case insensitive
    if not email is None: email = email.lower()
    if not email2 is None: email2 = email2.lower()

    if 'firstname' in request.POST: firstname = request.POST['firstname']
    if 'lastname' in request.POST: lastname = request.POST['lastname']
    if 'password' in request.POST: password = request.POST['password1']
    if 'contact_method' in request.POST: contact_method = request.POST['contact_method']

    if len(contact_method) > 0 and contact_method == '1':
        contact_method = '1'
    else:
        contact_method = '0'

    # 25022013 Add new field for whether or not the user has accepted the T&Cs
    if 'accepted_tcs' in request.POST: accepted_tcs = request.POST['accepted_tcs']

    if len(accepted_tcs) > 0 and accepted_tcs == '1':
        accepted_tcs = '1'
    else:
        accepted_tcs = '0'

    registration_fields = {}
    registration_fields['email'] = email
    registration_fields['email2'] = email2
    registration_fields['firstname'] = firstname
    registration_fields['lastname'] = lastname
    registration_fields['password'] = password
    registration_fields['contact_method'] = contact_method
    # 25022013 Add new field for whether or not the user has accepted the T&Cs
    registration_fields['accepted_tcs'] = accepted_tcs

    return registration_fields
        


def register(request, location='mytags'):


    (query_string_fields, next_page) = get_query_string_fields(request)

    tag_code = None
    if not query_string_fields is None :
        if 'tagid' in query_string_fields :
            tag_code = query_string_fields['tagid']
        elif 'id' in query_string_fields :
            tag_code = query_string_fields['id']

    # 30062012 If the store is in maintenance mode
    # then don't allow registration unless there is a tag id and it is COMPLIMENTARY
    if settings.STORE_MAINTENANCE_MODE :
        if not tag_code is None :
            tag = None
            tag_type = None
            tag = get_tag(tag_code)
            if not tag is None :
                tag_type = get_tag_type(tag)
                if not tag_type is None and tag_type.typename != 'COMPLIMENTARY' and tag_type.typename != 'STANDARD+':
                    return HttpResponseRedirect(links['maintenance_front'])
            else :
                return HttpResponseRedirect(links['maintenance_front'])
        else :
            return HttpResponseRedirect(links['maintenance_front'])
                
    
    if len(next_page) == 0 and request.method == 'POST' :
        if 'next' in request.POST :
            next_page = request.POST['next']
            
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))
    #except :
    #    next_page = '/mytags'

    profile_fields = {}
   
    if request.method == 'POST': # If the form has been submitted...
        form = RegistrationForm(None, request.POST, auto_id=True) # A form bound to the POST data
        #next_page = request.POST['next']

        reg_fields = get_registration_fields(request)

        #if form.is_valid(): # All validation rules pass
        # 23022013 no longer validate password field
        (is_valid, validation_error_string) = validate_registration_input(request, check_password=False)
        # do this until we can work out how form.is_valid() works....
        if is_valid :
            # Process the data in form.cleaned_data
            # ...

            # in this system the username is the email address
            # 23022013 As of Rev 2.0 the username is also the password unless manually changed

            # 23022013 We now check for duplicate username or email
            # if either is duplicated we handle this specially            
            (username_exists, error_string1) = check_duplicate_username(request, reg_fields['email'])
            (email_exists, error_string2) = check_duplicate_email(request, reg_fields['email'])
            
            if username_exists or email_exists:
                # 23022013 Handle existing account specially
                # Stub for now
                if not tag_code is None:
                    return register_tag_view(request, code=tag_code, email=reg_fields['email'])
                else:
                    return show_registration_screen(request, form, location, query_string_fields, 'Duplicate user - %s' % (error_string1), reg_fields)                                                                     

            # 25022013 Add new field for whether or not the user has accepted the T&Cs
            if reg_fields['accepted_tcs'] != '1':
                return show_registration_screen(request, form, location, query_string_fields, 'You must accept the Terms and Conditions!', reg_fields)

            # 24042012 We bypass activation for now. Users are automatically created activated and move onto the products page
            # 14052012 Users are activated but not necessarily subscribed and only logged in if the tag type is COMPLIMENTARY
            # A user has to be fully subscribed or in quarantine to be able to log in and to receive found messages
            # 23022013 registration has changed for Rev 2.0 (passwords are now usernames and STANDARD+ tags are processed
            (new_user, error_string) = register_user(request, reg_fields, profile_fields, activate_user=True, tag_code=tag_code)
            if new_user is None :
                return show_registration_screen(request, form, location, query_string_fields, error_string, reg_fields)

            # jump straight to the page following the old confirmation screen
            confirmation_codes = get_codes('%s_confirmation' % location)
            next_page = confirmation_codes['next']
            # 14052012 add email field to query_string
            query_string_fields.update({'email' : reg_fields['email'],})
            next_page = '%s%s' % (next_page, query_string(request, query_string_fields, None))

            # disable activation key setup
            #(activation_key) = setup_activation_key(request, new_user)

            # 14052012 log the user in if the tag exists and tag type is COMPLIMENTARY
            # 23022013 log the user in if the tag exists and tag type is COMPLIMENTARY or STANDARD+
            if not tag_code is None :
                tag = None
                tag_type = None
                tag = get_tag(tag_code)
                if not tag is None :
                    tag_type = get_tag_type(tag)
                    # 23022013 OLD CODE: if not tag_type is None and tag_type.typename == 'COMPLIMENTARY' :
                    if not tag_type is None and (tag_type.typename == 'COMPLIMENTARY' or tag_type.typename == 'STANDARD+') :

                        # 14072012 If this is registration with a complimentary tag send the complimentary subscription email now
                        # otherwise wait until after subscription
                        email_result = False
                        # 23022013 vary email sent depending on whether tag type is COMPLIMENTARY or STANDARD+
                        if tag_type.typename == 'COMPLIMENTARY':
                            (email_result) = mail_complimentary_confirmation(request, new_user, location, None, None, int(tag_type.typeclass), None, None, None, None, None, None, tag_code=tag_code)
                        elif tag_type.typename == 'STANDARD+':
                            (email_result) = mail_standardplus_confirmation(request, new_user, location, int(tag_type.typeclass), None, tag_code=tag_code)
                        
                        # 14072012 if a user is not already logged in log this new COMPLIMENTARY class user into myRocketTags
                        if request.user.is_anonymous() or not request.user.is_authenticated() :
                            username = reg_fields['email']
                            # 23022013 password is always username/email by default
                            # 23022013 OLD CODE: password = reg_fields['password']
                            password = reg_fields['email']
                            user = auth.authenticate(username=username, password=password)
                            if user is not None and user.is_active:
                                # Correct password, and the user is marked "active"
                                auth.login(request, user)                
                                # Redirect to a success page.
                                return HttpResponseRedirect(links['registration_complete'])
                                
                            else:
                                # 24042012 Show an error page for now - maybe later just redirect
                                return show_registration_screen(request, form, location, query_string_fields, 'Invalid authentication details - password is wrong', {'confirmed' : True, 'authenticated' : False, 'activation_key' : '', })      

            return HttpResponseRedirect(next_page)
        
        else :
            return show_registration_screen(request, form, location, query_string_fields, validation_error_string, more_fields=reg_fields)                            

    else:

        form = RegistrationForm(None, None, auto_id=True)

        extra_fields = { }

        return show_registration_screen(request, form, location, query_string_fields, '', extra_fields)            


def show_registration_states(request) :
    (query_string_fields, next_page) = get_query_string_fields(request)
    
    country_code = ''
    mobile = 0
    if 'country_code' in query_string_fields :
        country_code = query_string_fields['country_code']
    if 'mobile' in query_string_fields :
        mobile = int(query_string_fields['mobile'])
        
    states = get_states(country_code)
    return show_registration_states_screen(request, states, mobile)

def show_registration_cities(request) :
    (query_string_fields, next_page) = get_query_string_fields(request)
    
    country_code = ''
    mobile = 0
    if 'country_code' in query_string_fields :
        country_code = query_string_fields['country_code']
    if 'mobile' in query_string_fields :
        mobile = int(query_string_fields['mobile'])

    cities = get_cities(country_code)
    return show_registration_cities_screen(request, cities, mobile)
