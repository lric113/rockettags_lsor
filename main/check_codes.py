# This module provides methods for checking a generic code
# which can be either a tagcode or a promocode
#

from django.template import Context, loader, RequestContext, Template
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.conf import settings
from rockettags.fragments import get_fragment_with_links, get_fragment_with_codes, get_codes
from rockettags.fragments import query_string, get_value
from rockettags.lists import page_names, page_codes
from rockettags.main.screenelements import get_bottom_items, get_main_top_menu, get_central_fragment 
from rockettags.main.screenelements import get_query_string_fields
from rockettags.tags.models import get_tag_codes, update_primary_tag, get_tag_details, tag_exists, get_tag, register_tag
from rockettags.store import backoffice
from rockettags.links import links
from rockettags.main.models import create_log_entry, get_user
import datetime


# Obtain all possible information about the entity underlying the
# code be it tag or promo product
def get_code_details(code):

    details = None
    if tag_exists(code) :
        details = get_tag_details(code)
    else :
        # if this code is generic get the product details for it
        details = backoffice.get_product_details(code)
    if details is None: details = {}
    return details    

# Check whether or not the underlying object behind code
# is of a type to expire and if so whether or not it has expired
# COMPLIMENTARY or STANDARD+ tags expire as do tags with associated
# promo products or pure promo products
# registered tags can not expire
def has_expired(code, details) :

    tag_status = get_value(details, 'tag_status')
    tag_typename = get_value(details, 'tag_typename')
    product_type = get_value(details, 'product_type')

    if not tag_status is None :

        # if registered don't let this tag expire
        if tag_status > 0 :
            return False
      
        if tag_typename == 'COMPLIMENTARY' :

            tag = get_tag(code)
            tag_typesubclass = get_value(details, 'tag_typesubclass')
            if not tag_typesubclass is None and len(tag_typesubclass) > 0 :
                complimentary_ttl = int(tag_typesubclass)
            else :
                complimentary_ttl = settings.MAX_COMPLIMENTARY_TTL

            now = datetime.datetime.now()
            ttldate = tag.issued_date + datetime.timedelta(days=complimentary_ttl)
            if now > ttldate :
                return True
            else :
                return False

        elif tag_typename == 'PROMO' :

            tag = get_tag(code)
            product_code = tag.product.product_code
            product_details = backoffice.get_product_details(product_code)
            
            start_date = get_value(product_details, 'product_start_date')
            ttl = get_value(product_details, 'product_ttl')
            now = datetime.datetime.now()
            ttldate = start_date + datetime.timedelta(days=ttl)
            if now > ttldate :
                return True
            else :
                return False

    else :

        if not product_type is None and product_type == 'PROMO' :

            start_date = get_value(details, 'product_start_date')
            ttl = get_value(details, 'product_ttl')
            now = datetime.datetime.now()
            ttldate = start_date + datetime.timedelta(days=ttl)
            if now > ttldate :
                return True
            else :
                return False

    return False

