from django_countries.countries import OFFICIAL_COUNTRIES
from django.contrib.localflavor.us.us_states import US_STATES
from django.contrib.localflavor.uk.uk_regions import UK_REGION_CHOICES
from django.contrib.localflavor.au.au_states import STATE_CHOICES
from django.contrib.localflavor.jp.jp_prefectures import JP_PREFECTURES

# Need functions to convert from country code to country name
# and from state code to state name

def get_countries() :
    countries = OFFICIAL_COUNTRIES.copy()
    rev_countries = {}
    list_countries = []
    for code, name in countries.items() :
        rev_countries[name] = code
        list_countries.append(name)
    list_countries.sort()
    countries2 = []
    for name in list_countries :
        countries2.append((rev_countries[name],name))
    return countries2

def get_countries_dict() :
    countries = OFFICIAL_COUNTRIES.copy()
    countries_dict = {}
    for code, name in countries.items() :
        countries_dict[code] = name
    return countries_dict

def get_country_name(country_code):
    countries = get_countries_dict()
    if country_code in countries:
        return countries[country_code]
    else:
        return ''
    

def get_states(country_code) :
    states = []
    if country_code == 'AU' :
        states.append(('', 'select state'))
        for state in STATE_CHOICES :
            states.append(state)
    elif country_code == 'GB' :
        states.append(('', 'select region'))
        for state in UK_REGION_CHOICES :
            states.append(state)
    elif country_code == 'JP' :
        states.append(('', 'select prefecture'))
        for state in JP_PREFECTURES :
            states.append(state)
    elif country_code == 'US' :
        states.append(('', 'select state'))
        for state in US_STATES :
            states.append(state)
    else :
        states.append(('', 'select province'))
        states.append(('OT', 'Other'))
    return states

def get_states_dict(country_code) :

    states = []
    if country_code == 'AU' :
        for state in STATE_CHOICES :
            states.append(state)
    elif country_code == 'GB' :
        for state in UK_REGION_CHOICES :
            states.append(state)
    elif country_code == 'JP' :
        for state in JP_PREFECTURES :
            states.append(state)
    elif country_code == 'US' :
        for state in US_STATES :
            states.append(state)
    else :
        states.append(('OT', 'Other'))

    states_dict = {}
    for state in states :
        states_dict[state[0]] = state[1]
    return states_dict

def get_state_name(country_code, state_code):
    states = get_states_dict(country_code)
    if state_code in states:
        return states[state_code]
    else:
        return ''


def get_cities(country_code) :
    cities = []

    if country_code == 'US' :
        cities.append(('', 'select city'))
        cities.append(('NYC', 'New York'))

    return cities
