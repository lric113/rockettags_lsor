# The main views module for the Main app of rockettags
# Generally this module calls submodules for actual views implementation
#

from django.template import Context, loader, RequestContext, Template
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.conf import settings
from rockettags.fragments import get_fragment_with_links, get_fragment_with_codes, get_codes
from rockettags.fragments import query_string, get_value, get_parameter_from_session
from rockettags.lists import page_names, page_codes
from rockettags.store.views import get_store_fields, get_shipping_address_fields, get_billing_address_fields, \
    verify_address, setup_shipping_fields, setup_billing_fields
from rockettags.main.screenelements import get_bottom_items, get_main_top_menu, get_central_fragment, \
    generate_page_from_fragments
from rockettags.main.screenelements import get_query_string_fields
from rockettags.tags.models import get_tag_codes, primary_tag, update_primary_tag, get_tag_details, tag_exists, \
    get_found_messages_for_user, get_tag, register_tag, get_all_found_messages, get_found_message, \
    get_random_found_messages
from rockettags.main.login_view import login_admin, login_normal, logout, lost_credentials
from rockettags.main.register_view import register, show_registration_states, show_registration_cities, \
    get_registration_fields, validate_registration_input
from rockettags.main.confirm_view import confirm
from rockettags.store import backoffice
from rockettags.links import links
from rockettags.main.dropdowns import get_countries, get_states, get_cities
from rockettags.main.models import get_personal_details, create_log_entry, verify_email, update_address_details, \
    get_address_details, get_verified_users, set_verified_complete, get_user, update_subscription
from rockettags.main.mailers import mail_verify_address_to_user, mail_verify_address_completion_to_user, \
    mail_user_interest, mail_contact_details
from rockettags.main.mailers import mail_tellafriend_signee, mail_tellafriend_signor
from faq.models import Topic, Question
import datetime
import re
from rockettags.tags.find_ancillary import detect_mobile
from rockettags.main.redeem_view import redeem_code
from rockettags.main.check_codes import get_code_details, has_expired
from rockettags.main.subscriptions import extend_subscription_with_promo
from rockettags.main.models import update_users_to_v2_login


# Returns the index page for the website
def index(request):
    #    if detect_mobile(request) :
    #        mobile = None
    #        if 'm' in request.GET :
    #            mobile = request.GET['m']
    #        elif 'mobile' in request.GET:
    #            mobile = request.GET['mobile']
    #        if mobile is None or mobile != 'index':
    #            t = loader.get_template('mobile_front.html')
    #            c = Context({"request" : request, })
    #            return HttpResponse(t.render(c))

    # t = loader.get_template('index.html')
    # d = {"request" : request,
    #     'main_top_menu_fragment' : get_main_top_menu(request)
    #    }
    # d.update(get_bottom_items(request))
    # d.update({'base_url' : settings.BASE_URL, })
    # c = Context(d)
    # return HttpResponse(t.render(c))

    display_central_screen = False
    if 'display_central_screen' in request.GET:
        if request.GET['display_central_screen'] == 'true':
            display_central_screen = True

    extra_fields = {}
    page_name = "all_found_messages_ticker"
    if page_name == "all_found_messages_ticker":
        start_num = 0
        end_num = 10
        do_random = False
        if 'start_num' in request.GET and 'end_num' in request.GET:
            start_num = int(request.GET['start_num'])
            end_num = int(request.GET['end_num'])
        if 'random' in request.GET:
            if request.GET['random'] == 'true':
                do_random = True

        if do_random:
            messages = get_random_found_messages(start_num, end_num)
        else:
            messages = get_all_found_messages(start_num, end_num)
        extra_fields.update({'found_messages': messages, 'start_num': start_num, 'end_num': end_num, })

        # 11042013 Switch off the found messages ticker tape scroller for now
    # (central_fragment, full_page_name) = get_central_fragment(request, 'mytags', page_name, None, extra_fields, [])
    (central_fragment, full_page_name) = ("", "")

    template_file = 'index.html'

    t = loader.get_template('%s' % template_file)
    my_codes = {"request": request,
                'main_top_menu_fragment': get_main_top_menu(request),
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    my_codes.update({'base_url': settings.BASE_URL, })
    my_codes.update({'display_central_screen': display_central_screen, })
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


# 24022013 Point to the new normal login without password check
def login_view(request):
    return login_normal(request)


# 24022013 Point to the new secure login with password check
def login_admin_view(request):
    return login_admin(request)


def logout_view(request):
    return logout(request)


def lost_credentials_view(request):
    return lost_credentials(request)


def register_view(request, location='mytags'):
    return register(request, location)


def show_registration_states_view(request):
    return show_registration_states(request)


def show_registration_cities_view(request):
    return show_registration_cities(request)


def confirm_view(request, location='mytags'):
    return confirm(request, location)


def mytags(request, page_name='front'):
    email = None
    if 'email' in request.GET:
        email = request.GET['email']

    if request.user.is_anonymous() or not request.user.is_authenticated():

        if page_name != "confirm_verify_address_email":
            url = "%s?next=%s/%s" % (links['login'], links['mytags_front'], page_name)
            if not email is None:
                url = "%s&email=%s" % (url, email)

            return HttpResponseRedirect(url)

    store_fields = get_store_fields(request)
    extra_fields = {}
    extra_fields.update({'base_url': settings.BASE_URL, })

    if page_name == 'update_personal_details':
        return update_personal_fields(request)
    elif page_name == 'update_address_details':
        return update_address_fields(request)

    if page_name == 'tag_details':
        if request.method == 'POST':
            if 'code' in request.POST:
                tagcode = request.POST['code']
                update_primary_tag(request.user, tagcode)

    tag_codes = get_tag_codes(request)
    extra_fields.update(tag_codes)

    if page_name == "personal_details":
        personal_details = get_personal_details(request.user)
        countries = get_countries()
        states = get_states('US')
        extra_fields.update({'countries': countries, 'states': states, })
        extra_fields.update(personal_details)

    elif page_name == "address_details":
        shipping_address_details = get_address_details(request.user.email, 1)
        shipping_address_fields = setup_shipping_fields(shipping_address_details)
        billing_address_details = get_address_details(request.user.email, 2)
        billing_address_fields = setup_billing_fields(billing_address_details)
        countries = get_countries()
        states = get_states('US')
        extra_fields.update({'countries': countries, 'states': states, })
        extra_fields.update({'s_countries': countries, 's_states': states, })
        extra_fields.update({'b_countries': countries, 'b_states': states, })
        extra_fields.update(shipping_address_fields)
        extra_fields.update(billing_address_fields)

    elif page_name == "verify_address":
        extra_fields.update({'email': request.user.email, 'verify_status': request.user.profile.activation_key, })
    elif page_name == "send_verify_address_email":
        mail_verify_address_to_user(request, request.user)
    elif page_name == "confirm_verify_address_email":
        if not email is None:
            verify_email(email)
    elif page_name == "found_messages":
        start_num = 0
        end_num = 10
        if 'start_num' in request.GET and 'end_num' in request.GET:
            start_num = int(request.GET['start_num'])
            end_num = int(request.GET['end_num'])

        messages = get_found_messages_for_user(request.user, start_num, end_num)
        extra_fields.update({'found_messages': messages, 'start_num': start_num, 'end_num': end_num, })

    (central_fragment, full_page_name) = get_central_fragment(request, 'mytags', page_name, store_fields, extra_fields,
                                                              [])

    template_file = 'template.html'
    if page_name == 'front' or page_name == 'confirm_verify_address_email':
        template_file = 'template_front.html'
    else:
        template_file = 'template.html'

    t = loader.get_template('mytags/%s' % template_file)
    my_codes = {"request": request,
                'main_top_menu_fragment': get_main_top_menu(request),
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


def static(request, remainder=''):
    static_host = request.META.get('HTTP_HOST', None) or 'localhost'
    static_port = settings.STATIC_SERVER_PORT
    #    return HttpResponseRedirect('http://%s:%s/static/%s' % (static_host, static_port, remainder))
    return HttpResponseRedirect('/')


def update_personal_fields(request):
    (valid, error_string) = validate_registration_input(request, check_password=False)

    if not valid:
        return HttpResponse(error_string)

    registration_fields = get_registration_fields(request)

    current_profile = request.user.profile

    if not registration_fields is None:

        if registration_fields['firstname'] != request.user.first_name:
            request.user.first_name = registration_fields['firstname']
            request.user.save()

        if registration_fields['lastname'] != request.user.last_name:
            request.user.last_name = registration_fields['lastname']
            request.user.save()

        if len(registration_fields['password']) > 0:
            request.user.set_password(registration_fields['password'])
            request.user.save()

        current_profile = request.user.profile
        # 28072012 profile.tag_format is now used to hold whether and how a user can be contacted        
        if registration_fields['contact_method'] != current_profile.tag_format:
            current_profile.tag_format = registration_fields['contact_method']
            current_profile.save()

    return HttpResponseRedirect(links['personal_details'])


def update_address_fields(request):
    shipping_fields = get_shipping_address_fields(request)
    if not shipping_fields is None:
        error_string = verify_address(shipping_fields)
        if not error_string is None:
            return HttpResponse(error_string)

    billing_fields = get_billing_address_fields(request)
    if not billing_fields is None:
        error_string = verify_address(billing_fields)
        if not error_string is None:
            return HttpResponse(error_string)

    if not shipping_fields is None:
        update_address_details(request.user.email, shipping_fields, 1)

    if not billing_fields is None:
        update_address_details(request.user.email, billing_fields, 2)

    return HttpResponseRedirect(links['address_details'])


def display_screen(request, page_name='how_it_works'):
    my_codes = {}
    extra_fields = {}

    if request.user.is_authenticated() and not request.user.is_anonymous():
        if page_name == 'how_it_works':
            next_link = page_codes['store_registration']['next']
            extra_fields.update({'next': next_link, })

    t = loader.get_template('display_screens/template.html')

    main_top_menu_fragment = ''
    if page_name == 'solutions':
        extra_fields.update({'solutions_link': links['solutions'], })
        extra_fields.update({'contactus_link': links['contactus'], })
        main_top_menu_fragment = get_main_top_menu(request, tab4='')
    else:
        main_top_menu_fragment = get_main_top_menu(request)

    (central_fragment, full_page_name) = get_central_fragment(request, 'display_screens', page_name, {}, extra_fields,
                                                              [])
    my_codes = {"request": request,
                'main_top_menu_fragment': main_top_menu_fragment,
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


# Central gateway routine for handling entry of tag and promo codes
# 09022013 can be activated by form or by url call
# routine checks the code to see if valid and not expired
# then calls appropriate handler for it
def redeem_view(request, page_name='front'):
    return redeem_code(request, page_name)


def tellafriend_view(request, page_name='front'):
    main_fields = {}
    extra_fields = {}
    template_name = "template"
    email = None
    email = get_parameter_from_session(request, 'email', "POST")

    if request.user.is_authenticated() and not request.user.is_anonymous():
        template_name = "template_loggedin"
        email = request.user.email

    if not email is None and not re.search('(\@{1,1})', email):
        email = None

    if request.method == 'POST':
        friend1 = get_parameter_from_session(request, 'friend1_email', "POST")
        if not re.search('(\@{1,1})', friend1):
            friend1 = None
        friend2 = get_parameter_from_session(request, 'friend2_email', "POST")
        if not re.search('(\@{1,1})', friend2):
            friend2 = None
        friend3 = get_parameter_from_session(request, 'friend3_email', "POST")
        if not re.search('(\@{1,1})', friend3):
            friend3 = None

        if friend1 is None and friend2 is None and friend3 is None:

            error = "You didn't enter any friends!"

        elif not email is None:

            signor = get_user(email)
            if not signor is None:

                the_primary_tag = primary_tag(signor)
                tag_id = the_primary_tag.code

                if not friend1 is None:
                    result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                    if not result is None:
                        mail_tellafriend_signee(request, signor, friend1, 'main', None, tag_code=tag_id)
                if not friend2 is None:
                    result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                    if not result is None:
                        mail_tellafriend_signee(request, signor, friend2, 'main', None, tag_code=tag_id)
                if not friend3 is None:
                    result = update_subscription(email, settings.TELL_A_FRIEND_EXTENSION)
                    if not result is None:
                        mail_tellafriend_signee(request, signor, friend3, 'main', None, tag_code=tag_id)

                mail_tellafriend_signor(request, signor, 'main', settings.TELL_A_FRIEND_EXTENSION, None,
                                        tag_code=tag_id)

            else:
                error = "signor email is invalid!"

        else:
            error = "Empty signor email!"

    main_fields = locals()

    return generate_page_from_fragments(request, 'tellafriend', page_name, main_fields, extra_fields, [], template_name)


def get_maintenance_fields(request):
    if request.method != 'POST':
        return None

    email = ''
    email2 = ''
    firstname = ''
    lastname = ''

    try:
        email = request.POST['email']
        email2 = request.POST['email2']
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
    except:
        return None

    maintenance_fields = {}
    maintenance_fields['email'] = email
    maintenance_fields['email2'] = email2
    maintenance_fields['firstname'] = firstname
    maintenance_fields['lastname'] = lastname

    return maintenance_fields


def validate_maintenance_fields(maintenance_fields):
    error = None
    if maintenance_fields is None:
        error = "Unspecified error"

    email = maintenance_fields['email']
    email2 = maintenance_fields['email2']

    if email != email2:
        error = "Emails addresses do not match"

    return error


def maintenance_view(request, page_name='front'):
    maintenance_fields = get_maintenance_fields(request)

    details = None
    extra_fields = {}

    if page_name == 'processed':
        request.META['details'] = details
        # name = request.POST['name']

        error_string = validate_maintenance_fields(maintenance_fields)

        if not error_string is None:

            extra_fields.update({'errors': error_string, })
            page_name = 'registration'

        else:

            msg_string = "%s %s (%s) has registered interest in the site" % (
                maintenance_fields['firstname'], maintenance_fields['lastname'], maintenance_fields['email'])
            create_log_entry("MAINTENANCE_REGISTRATION", msg_string)
            mail_user_interest(request, maintenance_fields['email'], maintenance_fields['firstname'],
                               maintenance_fields['lastname'])

            return HttpResponseRedirect(links['front_screen'])

    (central_fragment, full_page_name) = get_central_fragment(request, 'maintenance', page_name, maintenance_fields,
                                                              extra_fields, [])

    template_file = 'template.html'

    t = loader.get_template('maintenance/%s' % template_file)
    my_codes = {"request": request,
                'main_top_menu_fragment': get_main_top_menu(request),
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


def site_log_view(request):
    output_string = ""

    try:
        f = open(settings.SITE_LOG, 'r')
        for line in f:
            output_string = '%s%s\n' % (output_string, line)
        f.close()
    except:
        output_string = ""

    return HttpResponse("<PRE>%s</PRE>" % output_string)


def faq_view(request, page_name='question_list'):
    (query_string_fields, next_page) = get_query_string_fields(request)

    topic_slug = None
    question_slug = None
    if not query_string_fields is None:
        if 'topic' in query_string_fields:
            topic_slug = query_string_fields['topic']
        if 'question' in query_string_fields:
            question_slug = query_string_fields['question']

    my_codes = {}
    extra_fields = {}
    extra_fields.update({'faq_link': links['faq'], })
    extra_fields.update({'contactus_link': links['contactus'], })

    t = loader.get_template('faq/template.html')

    main_top_menu_fragment = ''
    main_top_menu_fragment = get_main_top_menu(request, tab3='')

    topics = Topic.objects.all()
    topic = None
    questions = None
    question = None

    if not topic_slug is None:
        topic = Topic.objects.get(slug=topic_slug)
        questions = topic.questions.all()
        if not question_slug is None:
            question = topic.questions.get(slug=question_slug)

    (central_fragment, full_page_name) = get_central_fragment(request, 'faq', page_name,
                                                              {'topics': topics, 'topic': topic, 'questions': questions,
                                                               'question': question, }, extra_fields, [])
    my_codes = {"request": request,
                'main_top_menu_fragment': main_top_menu_fragment,
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


def question_list_view(request):
    (query_string_fields, next_page) = get_query_string_fields(request)

    topic_slug = None
    question_slug = None
    if not query_string_fields is None:
        if 'topic' in query_string_fields:
            topic_slug = query_string_fields['topic']
        if 'question' in query_string_fields:
            question_slug = query_string_fields['question']

    my_codes = {}
    extra_fields = {}

    t = loader.get_template('faq/question_list_internal.html')

    topics = Topic.objects.all()
    topic = None
    questions = None
    question = None

    if not topic_slug is None:
        topic = Topic.objects.get(slug=topic_slug)
        questions = topic.questions.all()
        if not question_slug is None:
            question = topic.questions.get(slug=question_slug)

    topics_list = []
    for topic_obj in topics:
        topic_rec = {}
        topic_rec['name'] = topic_obj.name
        topic_questions = []
        for question in topic_obj.questions.all():
            question_template = Template(question.answer)
            c = RequestContext(request, {})
            question_answer = question_template.render(c)
            question_text = question.text
            question_details = {}
            question_details['text'] = question_text
            question_details['answer'] = question_answer
            topic_questions.append(question_details)
        topic_rec['questions'] = topic_questions
        topics_list.append(topic_rec)

    c = RequestContext(request, {'topics': topics, 'topic': topic, 'questions': questions, 'question': question,
                                 'topics_list': topics_list, })
    return HttpResponse(t.render(c))


def get_contact_fields(request):
    if request.method != 'POST':
        return None

    mailto = ''
    next_page = ''
    email = ''
    email2 = ''
    firstname = ''
    lastname = ''
    message = ''

    try:
        if 'mailto' in request.POST:
            mailto = request.POST['mailto']
        if 'next' in request.POST:
            next_page = request.POST['next']
        email = request.POST['email']
        if email == 'email': email = ''
        email2 = request.POST['email2']
        if email2 == 'confirm email': email2 = ''
        firstname = request.POST['firstname']
        if firstname == 'first name': firstname = ''
        lastname = request.POST['lastname']
        if lastname == 'last name': lastname = ''
        message = request.POST['message']
        if message == 'Enter your message here': message = ''
    except:
        return None

    contact_fields = {}
    contact_fields['mailto'] = mailto
    contact_fields['next'] = next_page
    contact_fields['email'] = email
    contact_fields['email2'] = email2
    contact_fields['firstname'] = firstname
    contact_fields['lastname'] = lastname
    contact_fields['message'] = message

    return contact_fields


def validate_contact_fields(contact_fields):
    error = None
    if contact_fields is None:
        error = "Unspecified error"

    email = contact_fields['email']
    email2 = contact_fields['email2']

    if email != email2:
        error = "Emails addresses do not match"

    return error


def contact_view(request, page_name='registration'):
    request.META['page_name'] = page_name
    # name = request.POST['name']

    mailto = None
    next_page = None
    (query_string_fields, next_page) = get_query_string_fields(request)
    if not query_string_fields is None:
        if 'mailto' in query_string_fields:
            mailto = query_string_fields['mailto']

    contact_fields = get_contact_fields(request)
    if not contact_fields is None:
        if len(contact_fields['mailto']) > 0:
            mailto = contact_fields['mailto']
        if len(contact_fields['next']) > 0:
            next_page = contact_fields['next']

    if mailto is None:
        mailto = settings.DEFAULT_CONTACT_MAILTO

    details = None
    extra_fields = {}
    extra_fields.update({'mailto': mailto, 'next': next_page, })

    if page_name == 'processed' and request.method == 'POST':
        request.META['details'] = details
        # name = request.POST['name']

        error_string = validate_contact_fields(contact_fields)

        if not error_string is None:

            extra_fields.update({'errors': error_string, })
            page_name = 'registration'

        else:

            msg_string = "%s %s (%s) has contacted this site at mailto %s" % (
                contact_fields['firstname'], contact_fields['lastname'], contact_fields['email'], mailto)
            create_log_entry("CONTACT_REGISTRATION", msg_string)
            mail_contact_details(request, mailto, contact_fields['email'], contact_fields['firstname'],
                                 contact_fields['lastname'], contact_fields['message'])

            # 06072012 Now display a response page first 
            # return HttpResponseRedirect(next_page)

    else:

        page_name = 'registration'

    (central_fragment, full_page_name) = get_central_fragment(request, 'contact', page_name, contact_fields,
                                                              extra_fields, [])

    template_file = 'template.html'

    t = loader.get_template('contact/%s' % template_file)
    my_codes = {"request": request,
                'main_top_menu_fragment': get_main_top_menu(request),
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


def verified_emails_view(request):
    if request.user.is_anonymous() or not request.user.is_authenticated():
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff:
        return HttpResponse('You need to be staff to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if request.method == 'GET':

        email = None
        if 'email' in request.GET:
            email = request.GET['email']

        users_verified_not_completed = []

        users = get_verified_users()

        last_promotion_applied = ''
        for user, been_completed in users:
            if not been_completed:
                users_verified_not_completed.append(user)

        return render_to_response('mytags/verified_emails.html',
                                  {'request': request, 'users': users_verified_not_completed, 'errormsg': '',
                                   'promos': get_valid_promos_dropdown(), }, context_instance=RequestContext(request))



    elif request.method == 'POST':

        email = None
        if 'email' in request.POST:
            email = request.POST['email']
        promo_code = None
        if 'promo_code' in request.POST:
            promo_code = request.POST['promo_code']

        errormsg = ''

        if not email is None and not promo_code is None:
            user = get_user(email)
            details = backoffice.get_product_details(promo_code)

            if not has_expired(promo_code, details):

                if not user is None:

                    (result, error) = extend_subscription_with_promo(user, promo_code, details)

                    if result:

                        subscription_details = backoffice.get_subscription_details(user.email)

                        expiry_date = subscription_details['expiry_date']

                        mail_verify_address_completion_to_user(request, user, promo_code, expiry_date)

                        set_verified_complete(email)

                    else:
                        errormsg = error
                else:
                    errormsg = 'Invalid user %s' % email
            else:
                errormsg = 'This code %s has expired' % promo_code

        users_verified_not_completed = []

        users = get_verified_users()

        last_promotion_applied = ''
        for user, been_completed in users:
            if not been_completed:
                users_verified_not_completed.append(user)

        return render_to_response('mytags/verified_emails.html',
                                  {'request': request, 'users': users_verified_not_completed, 'errormsg': errormsg,
                                   'promos': get_valid_promos_dropdown(), }, context_instance=RequestContext(request))

    return HttpResponse("invalid response")


def get_valid_promos_dropdown():
    (promo_codes, prices) = backoffice.get_products(["PROMO"])

    promos_list = []

    for promo_code in promo_codes:
        details = backoffice.get_product_details(promo_code)

        if not has_expired(promo_code, details):
            promos_list.append((promo_code, details["product_name"]))

    return promos_list


# def stickerbook_update_view(request) :

#    test = 'something'
#    #if 'REMOTE_USER' in request.META:
#    test = request.META['REMOTE_USER']
#    return HttpResponse(test)

def update_subscription_view(request):
    if request.user.is_anonymous() or not request.user.is_authenticated():
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff:
        return HttpResponse('You need to be staff to update a subscription <a href=\"/login\">Login to Rocket Tags</a>')

    email = None
    extension_period = None

    email = get_parameter_from_session(request, 'email')
    extension_period = get_parameter_from_session(request, 'extension_period')
    extension_period = int(extension_period)

    result = False
    if not email is None and not extension_period is None:
        result = update_subscription(email, extension_period)

    (central_fragment, full_page_name) = get_central_fragment(request, 'mytags', 'update_subscription_result', locals(),
                                                              None, [])

    template_file = 'template.html'

    t = loader.get_template('mytags/%s' % template_file)
    my_codes = {"request": request,
                'main_top_menu_fragment': get_main_top_menu(request),
                'central_fragment': central_fragment,
                'page_name': full_page_name,
                }
    my_codes.update(get_bottom_items(request))
    c = RequestContext(request, my_codes)
    return HttpResponse(t.render(c))


def update_user_passwords(request):
    if request.user.is_anonymous() or not request.user.is_authenticated():
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff:
        return HttpResponse('You need to be staff to update a subscription <a href=\"/login\">Login to Rocket Tags</a>')

    num_changed = update_users_to_v2_login()

    return HttpResponse(num_changed)
