import datetime, random, sha
from django.db import models
from django.contrib.auth.models import User
from rockettags.tags.models import get_tag, get_tag_type
from rockettags.store import backoffice
from django.conf import settings

class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # preferences
    tag_code = models.CharField(max_length=8, null=True, blank=True)    
    tag_format = models.CharField(max_length=12, null=True, blank=True) # 28072012 reserved as a flag for whether or not the user wants to be contactable (values are 0 for no, 1 for email)
    tag_colour = models.CharField(max_length=12, null=True, blank=True) # 26072012 used for whether a user has been sent their promo for verifying their email
    subscription_period = models.IntegerField(default=0)

    # activation key - 26072012 used to hold whether or not a user has verified their email address
    # is either True or False. If empty or None is False.
    activation_key = models.CharField(max_length=40, null=True, blank=True)
    key_expires = models.DateTimeField(null=True, blank=True)

    # contact details
    # 28072012 These fields are no longer in use
    address1 = models.CharField(max_length=128, null=True, blank=True)
    address2 = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=64, null=True, blank=True)
    state = models.CharField(max_length=64, null=True, blank=True)
    country = models.CharField(max_length=32, null=True, blank=True)
    postcode = models.CharField(max_length=32, null=True, blank=True)
    phone = models.CharField(max_length=32, null=True, blank=True)
    mobile = models.CharField(max_length=32, null=True, blank=True)

    # Important Dates
    creation_date = models.DateTimeField(default=datetime.datetime.now())
    registration_date = models.DateTimeField(null=True, blank=True)
    expiry_date = models.DateTimeField(null=True, blank=True)

    status = models.PositiveIntegerField(default=0)
    # 14052012 status takes the following values: 0 - unsubscribed
    #                                             1 - subscribed but waiting for tag
    #                                             2 - fully subscribed
    #                                             3 - quarantine

    def __unicode__(self) :
        return u'%s' % self.user.get_full_name()

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])

def check_duplicate_username(request, username) :

    found_user = None
    try :
        found_user = User.objects.get(username=username)
    except :
        found_user = None

    if not found_user is None :
        return (True, 'User with email %s already exists in system' % found_user.email)
    else :
        return (False, '')


def check_duplicate_email(request, email) :
    found_user = None
    try :
        found_user = User.objects.get(email=email)
    except :
        found_user = None

    if not found_user is None :
        return (True, 'User with email %s already exists in system' % email)
    else :
        return (False, '')

# 28072012 do not use this method anymore since the fields are being used for other purposes!!!!
def update_profile(profile, profile_fields) :

    if profile is None : return
    if profile_fields is None : return
    if 'format_type' in profile_fields : profile.tag_format = profile_fields['format_type']
    if 'colour' in profile_fields : profile.tag_colour = profile_fields['colour']
    if 'subscription' in profile_fields : profile.subscription_period = profile_fields['subscription']
    if 'country' in profile_fields : profile.country = profile_fields['country']
    if 'state' in profile_fields : profile.state = profile_fields['state']
    profile.save()
    return
        

def get_personal_details(user) :

    personal_details = {}

    personal_details['firstname'] = user.first_name
    personal_details['lastname'] = user.last_name                                    
    personal_details['email'] = user.email

    personal_details['address1'] = user.profile.address1
    personal_details['address2'] = user.profile.address2
    personal_details['city'] = user.profile.city
    personal_details['state'] = user.profile.state
    personal_details['country'] = user.profile.country
    personal_details['postcode'] = user.profile.postcode
    personal_details['phone'] = user.profile.phone
    personal_details['mobile'] = user.profile.mobile

    personal_details['status'] = user.profile.status

    # 28072012 profile.tag_format is now used to hold whether and how a user can be contacted
    personal_details['contact_method'] = user.profile.tag_format


    return personal_details


# 24042012 register user can now automatically activate a user
# 23022013 as of Rev 2.0 all users are assigned their usernames as passwords (emails) this can be overridden by
# the use_password flag
# 23022013 both COMPLIMENTARY and STANDARD+ tags are automatically subscribed
def register_user(request, register_fields, profile_fields, activate_user=False, tag_code=None, use_password=False) :

    new_user = None

    try :

        # 23022013 OLD CODE: new_user = User.objects.create_user(register_fields['email'], register_fields['email'], register_fields['password'])
        # 23022013 usernames (email fields) are now automatically used as password
        password = register_fields['email']
        if use_password:
            password = register_fields['password']
            
        new_user = User.objects.create_user(register_fields['email'], register_fields['email'], password)

        # 25022013 We don't collect information about the full name anymore at this point
        # new_user.first_name = register_fields['firstname']
        # new_user.last_name = register_fields['lastname']

        if activate_user :
            new_user.is_active = True
        else :
            new_user.is_active = False
        new_user.save()

        create_log_entry('Create User', 'User %s created.' % new_user.email, tablename='User')

    except :
        new_user = None

    if new_user is None :
        # 23022013 OLD CODE: create_log_entry('Create User', 'Problem creating user %s %s.' % (register_fields['firstname'], register_fields['lastname']), subcategory='Error', tablename='User')
        # 23022013 Don't use firstname and lastname since they aren't assigned at registration
        create_log_entry('Create User', 'Problem creating user %s.' % (register_fields['email']), subcategory='Error', tablename='User')
        return (None, 'Problem creating user')

    new_profile = None
    try :
        new_profile = UserProfile(user=new_user)
        new_profile.expiry_date = datetime.datetime.now()
        new_profile.subscription_period = 0        
        new_profile.status = 0 # By default the user is not subscribed

        # 28072012 profile.tag_format is now used to store whether and how a user wants to be contacted - currently it is 0 for not at all, 1 for contact by email
        # 25022013 we don't collect information about contact method anymore
        # so default to switched on
        # new_profile.tag_format = register_fields['contact_method']
        new_profile.tag_format = '1'
        new_profile.save()

        # 28072012 do not use this method anymore since the fields are being used for other purposes!!!!
        #update_profile(new_profile, profile_fields)

        create_log_entry('Create User Profile', 'Profile for User %s created.' % new_user.email, tablename='UserProfile')
        
    except :
        new_profile = None

    if new_profile is None :
        create_log_entry('Create User Profile', 'Problem creating profile for User %s.' % new_user.email, subcategory='Error', tablename='UserProfile')
        return (None, 'Problem creating profile')

    new_tagid = ''

    # 14052012 If a tag code is provided register it with the user
    # If the tag is COMPLIMENTARY automatically mark the user as subscribed otherwise not
    if not tag_code is None :
        new_tag = None
        try :
            new_tag = get_tag(tag_code)
            if new_tag is None :
                create_log_entry('Assign Tag', 'Problem 1 assigning tag %s to User %s.' % (tag_code, new_user.email), subcategory='Error', tablename='Tag')
                return (False, 'Problem assigning tag')

            # If this is a complimentary tag and the user is registering for the first time
            # then set the user's expiry date forward by the requisite number of days automatically
            # Otherwise, the expiry date only gets set once the user has purchased a subscription
            # at this point the expiry date may be extended by a promotional amount
            # so send the relevant tag id on into the store
            # If this is a complimentary tag there is no payment to add to the subscription record
            tag_type = get_tag_type(new_tag)
            typename = tag_type.typename
            numdays = 0
            if len(tag_type.typeclass) > 0 :
                numdays = int(tag_type.typeclass)

            # 23022013 There is now a new tag type like COMPLIMENTARY called STANDARD+
            
            # 23022013 OLD CODE: if typename == 'COMPLIMENTARY' :
            if typename == 'COMPLIMENTARY' or typename == 'STANDARD+' :
                if numdays > 0 :
                    current_profile = new_user.get_profile()
                    current_profile.expiry_date = datetime.datetime.now() + datetime.timedelta(days=numdays)
                    current_profile.subscription_period = numdays
                    current_profile.status = 2 # user is subscribed
                    current_profile.save()
                    subscription_number = backoffice.create_subscription(new_user.email, None, numdays, autorenew=False, accepted_tcs_version=settings.TC_VERSION)
                    backoffice.update_subscription(new_user.email, None, None, None, current_profile.status)

                    # 23072012 Ensure that a COMPLIMENTARY user always has shipping and billing address fields
                    # 23022013 Ensure that a COMPLIMENTARY or STANDARD+ user always has shipping and billing address fields
                    add_address_details(new_user.email, None, 1)
                    add_address_details(new_user.email, None, 2)
            else :
                current_profile = new_user.get_profile()
                current_profile.expiry_date = datetime.datetime.now()
                current_profile.subscription_period = 0
                current_profile.status = 0 # user is not yet suscribed
                current_profile.save()

            # Always register this tag            
            new_tag.status = 1 # 1 is registered
            new_tag.registered_date = datetime.datetime.now()
            new_tag.save()
            new_user.tags.add(new_tag)
            current_profile = new_user.get_profile()
            current_profile.tag_code = new_tag.to_base36()
            current_profile.registration_date = datetime.datetime.now()
            current_profile.save()
            create_log_entry('Assign Tag', 'Assigning tag %s to User %s.' % (tag_code, new_user.email), tablename='Tag')
        except :
            new_tag = None

        if new_tag is None :
            create_log_entry('Assign Tag', 'Problem 2 assigning tag %s to User %s.' % (tag_code, new_user.email), subcategory='Error', tablename='Tag')
            return (None, 'Problem assigning tag')

        new_tagid = ''
        try :
            new_tagid = new_tag.to_base36()
        except :
            new_tagid = ''

        if len(new_tagid) == 0 :
            create_log_entry('Assign Tag', 'Problem 3 assigning tag %s to User %s. Invalid tagid.' % (tag_code, new_user.email), subcategory='Error', tablename='Tag')
            return (None, 'Incorrect tagid')
                                            
    return (new_user, new_tagid)


# 14052012 Test the current status of the user
# If the user is subscribed or in quarantine check the expiry date and update the status
def test_if_subscribed(user) :

    current_profile = user.get_profile()

    current_status = current_profile.status

    if current_status == 2 or current_status == 3 :
        expiry_date = current_profile.expiry_date
        current_date = datetime.datetime.now()

        if expiry_date is None : current_profile.status = 0
        else :
            if current_date > expiry_date :
                if current_date <= expiry_date + datetime.timedelta(days=settings.USER_QUARANTINE_PERIOD) :
                    current_profile.status = 3
                    create_log_entry('Changing subscription status', 'User %s has been changed to quarantine status.' % user.email, tablename='UserProfile')
                else :
                    current_profile.status = 0
                    create_log_entry('Changing subscription status', 'User %s has been changed to not subscribed status.' % user.email, tablename='UserProfile')

        current_status = current_profile.status

    return current_status


def is_subscribed(user) :

    current_profile = user.get_profile()

    current_status = current_profile.status

    if current_status == 0 or current_status == 1 :
        return False
    else :
        return True

def get_subscribed_status(email) :

    user = get_user(email)
    if user is None :
        return None
    
    current_profile = user.get_profile()
    if current_profile is None :
        return None
    
    current_status = current_profile.status

    return current_status


def get_user(email) :
    if email is None or len(email) == 0:
        return None

    user = None
    try:
        user = User.objects.get(email=email)
    except:
        user = None
        
    return user

def setup_activation_key(request, user) :

    # Build the activation key for their account                                                                                                                    
    salt = sha.new(str(random.random())).hexdigest()[:5]
    activation_key = sha.new(salt+user.username).hexdigest()
    key_expires = datetime.datetime.today() + datetime.timedelta(2)

    user_profile = user.get_profile()
    user_profile.activation_key = activation_key
    user_profile.key_expires = key_expires
    user_profile.save()

    create_log_entry('Activating User', 'Activating/registering User %s.' % user.email, tablename='UserProfile')

    return activation_key

def convert_to_base36(val) :
    result = ''
    temp = []

    while val >= 36 :
        div = val / 36
        mod = val % 36
        temp.append(mod)
        val = div
    temp.append(val)

    while len(temp) > 0 :
        digit = temp.pop()
        if digit > 9 :
            result = '%s%s' % (result, chr(digit + 55))
        else :
            result = '%s%s' % (result, digit)
            
    return result

def generate_new_password() :
    maxnumber = pow(2, 32)

    randomnum = random.randint(1, maxnumber)
    random_password = convert_to_base36(randomnum)
    return str(random_password)
    
def verify_email(email) :

    try :
        user = get_user(email)
        current_profile = user.profile
    except :
        user = None
        current_profile = None

    if current_profile is None :
        return False
    
    current_profile.activation_key = "True"
    current_profile.save()

    return True
    
def get_verified_users():

    verified_users = []
    users = User.objects.all()
    for user in users:
        profile = user.profile
        if not profile is None:
            if profile.activation_key == 'True':
                sent_verification_promo = False
                if not profile.tag_colour is None and profile.tag_colour == 'True':
                    sent_verification_promo = True
                verified_users.append((user, sent_verification_promo))
    return verified_users

def set_verified_complete(email):

    user = get_user(email)
    if user is None:
        return False

    profile = user.profile
    if profile is None:
        return False

    profile.tag_colour = 'True'
    profile.save()

    return True

class SiteLog(models.Model):

    # preferences
    tablename = models.CharField(max_length=32, null=True, blank=True)    
    category = models.CharField(max_length=64, null=True, blank=True)
    subcategory = models.CharField(max_length=64, null=True, blank=True)
    message = models.CharField(max_length=256, null=True, blank=True)
    creation_date = models.DateTimeField(default=datetime.datetime.now())

    
def create_log_entry(category, message, subcategory=None, tablename=None) :

    if category is None or len(category) == 0 :
        return None

    if subcategory is None :
        subcategory = ''

    if tablename is None :
        tablename = ''

#    new_entry = SiteLog(tablename=tablename, category=category, subcategory=subcategory, message=message)
#    new_entry.save()

#    return new_entry

    message_list = message.rsplit('\n')
    new_message = ':'.join(message_list)

    new_entry = '%s|%s|%s|%s|%s|' % (str(datetime.datetime.now()), tablename, category, subcategory, new_message)
    f = open(settings.SITE_LOG, 'a')
    try :
        f.write('%s\n' % new_entry)
    finally :
        f.close()

    return new_entry
    

class AddressDetails(models.Model) :

    user = models.ForeignKey(User, null=True, blank=True, related_name='addressdetails', on_delete=models.SET_NULL)

    address_type = models.PositiveIntegerField(default=0)

    address1 = models.CharField(max_length=255, null=True, blank=True)
    address2 = models.CharField(max_length=255, null=True, blank=True)    
    city = models.CharField(max_length=64, null=True, blank=True)
    state = models.CharField(max_length=64, null=True, blank=True)
    postcode = models.CharField(max_length=32, null=True, blank=True)    
    country = models.CharField(max_length=64, null=True, blank=True)
    phone = models.CharField(max_length=64, null=True, blank=True)    
    mobile = models.CharField(max_length=64, null=True, blank=True)

    creation_date = models.DateTimeField(default=datetime.datetime.now())

    extra1 = models.CharField(max_length=128, null=True, blank=True)
    extra2 = models.CharField(max_length=64, null=True, blank=True)
    extra3 = models.CharField(max_length=32, null=True, blank=True)
    status = models.PositiveIntegerField(default=0)
    update_date = models.DateTimeField(null=True, blank=True)


def add_address_details(email, address_details, address_type) :

    user = get_user(email)
    if user is None :
        return False

    new_address = AddressDetails()
    new_address.address_type = address_type

    if not address_details is None:
        new_address.address1 = address_details['address1']
        new_address.address2 = address_details['address2']
        new_address.city = address_details['city']
        new_address.state = address_details['state']
        new_address.postcode = address_details['postal_code']
        new_address.country = address_details['country']
        new_address.phone = address_details['phone']
        new_address.mobile = address_details['mobile']
    
    new_address.update_date = datetime.datetime.now()
    new_address.save()

    user.addressdetails.add(new_address)

    return True
    
def update_address_details(email, address_details, address_type) :

    user = get_user(email)
    if user is None :
        return False

    if user.addressdetails is None or len(user.addressdetails.all()) == 0:
        return False
    
    address = user.addressdetails.all()[address_type-1]
    address.address1 = address_details['address1']
    address.address2 = address_details['address2']
    address.city = address_details['city']
    address.state = address_details['state']
    address.postcode = address_details['postal_code']
    address.country = address_details['country']
    address.phone = address_details['phone']
    address.mobile = address_details['mobile']
    
    address.update_date = datetime.datetime.now()
    address.save()

    return True

def get_address_details(email, address_type) :

    user = get_user(email)
    if user is None :
        return False

    address = None
    address_details = {}

    if user.addressdetails is None or len(user.addressdetails.all()) == 0:
        return None

    address = user.addressdetails.all()[address_type-1]
    if address is None:
        return None

    address_details['address_type'] = address.address_type
    address_details['address1'] = address.address1
    address_details['address2'] = address.address2
    address_details['city'] = address.city
    address_details['state'] = address.state
    address_details['postal_code'] = address.postcode
    address_details['country'] = address.country
    address_details['phone'] = address.phone
    address_details['mobile'] = address.mobile

    return address_details

    
def update_subscription(email, extension_period):

    current_user = get_user(email)
    if current_user is None: return False

    current_profile = current_user.get_profile()
    if current_profile is None: return False

    remaining_period = 0
    if datetime.datetime.now() < current_profile.expiry_date :
      remaining_delta = current_profile.expiry_date - datetime.datetime.now()
      remaining_period = remaining_delta.days + 1

    # This is the total of the subscription period plus any extension
    total_subscription_period = int(extension_period) + remaining_period
                
    current_profile.expiry_date = datetime.datetime.now() + datetime.timedelta(days=total_subscription_period)
    current_profile.subscription_period = total_subscription_period
    current_profile.save()

    # update subscription record status to be the same as the current_profile.status
    backoffice.update_subscription(email, None, None, extension_period, current_profile.status)

    create_log_entry('UPDATE_SUBSCRIPTION', 'Extension to subscription of period %d created for user %s with resulting status %d' % (extension_period, email, current_profile.status))

    return True

def update_users_to_v2_login():

    count = 0
    users = User.objects.all()
    for user in users:

        name = user.username
        email = user.email
        email_elements = email.split('@')
        email_name = email_elements[0]

        if name.lower() == 'root' or email_name.lower() == 'root':
            continue
        
        if name.lower() == 'admin' or email_name.lower() == 'admin':
            continue

        user.set_password(email.lower())
        user.save()
        count = count + 1

    return count



    
    
