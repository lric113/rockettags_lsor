import os
import sys

sys.path.append('/opt/rockettags/webapps')
os.environ['DJANGO_SETTINGS_MODULE'] = 'rockettags.settings'
#os.environ['HTTPS'] = "on"

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
