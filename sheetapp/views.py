from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from transition.models import Tag, UserProfile
from transition.forms import CustomFieldsForm, AddCorporateAccountForm, UploadCorporateTagsForm
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from import_export import resources
from transition.mailers import mail_list_of_edited_tags
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
import pytz
import datetime
import json
import csv
import os


def checkuserisstaff(user):
    """
    Function called by the user_passes_test decorator to check if the user is a Staff member
    :param user:
    :return:
    """
    return user.is_staff


@login_required(login_url='/login')
def main(request):
    """
    Main view
    :param request: Django request object
    :return: HttpResponse
    """
    return render(request, 'sheetapp/main.html', locals())


@login_required(login_url='/login')
def corpmanage(request):
    """
    View to the manage page containing the corporate users tags

    :param request: Django request object
    :return: HttpResponse
    """
    
    # get current user id, current_user.id
    current_user = request.user
    
    # get all tags from the db for the current user
    tags = Tag.objects.all().filter(user_id=current_user.id)

    # get number of tags allocated
    total_allocated_tags = tags.count()

    # get total tags assigned
    total_assigned_tags = Tag.objects.filter(user_id=current_user.id, assigned_to__isnull=False).count()

    # setup the form that allows to rename the custom fields
    my_user_profile = UserProfile.objects.filter(user_id=request.user.id)

    if len(my_user_profile) == 1:
        custom_fields = getcustomfield(my_user_profile[0])

    # locals() will expose them to the template
    return render(request, 'sheetapp/corpmanage.html', locals())


def getcustomfield(user_profile):
    """
    Function that returns the renamed fields of the user or the default values
    :return: dict
    """

    custom_fields_dict = {'field1': "", 'field2': "",
                          'field3': "", 'field4': ""}

    if user_profile:
        # there is a user profile
        try:
            # test to see if the new custom_fields field exists
            custom_fields_json = user_profile.custom_fields
        except Exception:
            # broad exception here
            return custom_fields_dict

        # decode the JSON object in the db, if there is no json object there then set to None
        try:
            custom_fields = json.loads(custom_fields_json)
        except Exception:
            custom_fields = None

        if custom_fields:
            # there are custom fields renamed
            custom_fields_dict = {'field1': custom_fields['field1'], 'field2': custom_fields['field2'],
                                  'field3': custom_fields['field3'], 'field4': custom_fields['field4']}

        return custom_fields_dict


@login_required(login_url='/login')
def renamefields(request):
    """
    View that shows a form that the user can use to rename his custom fields
    :param request: A django request
    :return: HttpResponse
    """

    # setup the form that allows to rename the custom fields
    my_user_profile = UserProfile.objects.filter(user_id=request.user.id)

    if len(my_user_profile) == 1:
        custom_fields = getcustomfield(my_user_profile[0])

    if custom_fields:
        # there are custom fields renamed
        pre_filled_dict = {'field1': custom_fields['field1'], 'field2': custom_fields['field2'],
                           'field3': custom_fields['field3'], 'field4': custom_fields['field4']}
    else:
        # no properly formatted field
        pre_filled_dict = {'field1': "", 'field2': "",
                           'field3': "", 'field4': ""}

    update_fields_form = CustomFieldsForm(pre_filled_dict)

    # locals() will expose them to the template
    return render(request, 'sheetapp/renamefields.html', locals())


@csrf_exempt
@require_http_methods(['GET','POST'])
@login_required(login_url='/login')
def updatetag(request):
    """
    Handler method for updating tag fields.
    This function is set as CSRF exempt for now. What is required is that a CSRF token is included in the POST values
    so that the Django CSRF protection middleware can authorize the call. This can be done. But not now.

    :param request: Django request object
    :return: HttpResponse
    """
 
    # list of fields that are editable
    list_of_editable_fields = ['email_cc', 'assigned_to', 'field1', 'field2', 'field3', 'field4']

    field = request.POST.get('field', None)
    tagid = int(request.POST.get('id', None))
    value = request.POST.get('value', None)

    # check if the field is editable, if not then return a 403 error code
    if field not in list_of_editable_fields:
        return HttpResponse(status=400)

    # check if the value id is an integer
    if type(tagid) is not int:
        return HttpResponse(status=400)

    # fetch tag from db using id and user_id
    tagset = Tag.objects.filter(id=tagid, user_id=request.user.id)

    # check if only one tag is returned
    if len(tagset) != 1:
        return HttpResponse(status=403)
    else:
        tag = tagset[0]

    # lets update the tag now
    if field == "email_cc":
        bool_set = {'true': 1, 'false': 0}
        value = bool_set[value]

    # lets try to update value and save it
    try:
        # update the value
        setattr(tag, field, value)

        # update the edit_time field
        now = datetime.datetime.utcnow()
        now = now.replace(tzinfo=pytz.utc)
        tag.edit_time = now

        # save tag
        tag.save()

    except Exception:
        # TODO: the above Exception is too broad
        return HttpResponse(status=500)

    return HttpResponse(content=value, status=200)


@login_required(login_url='/login')
def downloadmytags(request):
    """
    View that will offer a download of CSV list of the tags the user owns

    :param request: Django request object
    :return: HttpResponse
    """

    class TagResource(resources.ModelResource):
        """
        ModelResource subclass for the django-import-export module
        """
        class Meta:
            model = Tag
            fields = ('code', 'issued_date', 'registered_date', 'edit_time', 'assigned_to', 'email_cc', 'field1', 'field2',
                      'field3', 'field4')

    # get a queryset that contains the tags of the logged in user
    tagset = Tag.objects.filter(user_id=request.user.id)

    # get a set of data based on the above query
    dataset = TagResource().export(tagset)

    # prepare an HttpResponse
    response = HttpResponse(dataset.csv, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=mytags.csv'
    response['Content-Length'] = len(dataset.csv)

    return response


@login_required(login_url='/login')
def emailmyeditedtags(request):
    """
    View that will email a list of tags that were edited today

    :param request: Django request object
    :return: HttpResponse
    """

    mail_list_of_edited_tags(request)

    return redirect('sheetapp.views.corpmanage')


@require_http_methods(['POST'])
@login_required(login_url='/login')
def updatecustomfields(request):
    """
    View that handles the POST request when changing the custom fields. This implementation will be custom.
    :param request: Django request object
    :return:
    """

    # check if values were posted
    if request.method == 'POST':
        # fetch values from the POSTed request
        field1 = request.POST.get('field1', "")
        field2 = request.POST.get('field2', "")
        field3 = request.POST.get('field3', "")
        field4 = request.POST.get('field4', "")

        # strip and truncate
        field1 = field1.strip()[:50]
        field2 = field2.strip()[:50]
        field3 = field3.strip()[:50]
        field4 = field4.strip()[:50]

        # make json
        fields = {'field1': field1, 'field2': field2, 'field3': field3, 'field4': field4}
        json_fields = json.dumps(fields)

        # store json in UserProfile
        my_user_profile = UserProfile.objects.filter(user_id=request.user.id)[0]
        my_user_profile.custom_fields = json_fields
        my_user_profile.save()

        return redirect('sheetapp.views.corpmanage')


@user_passes_test(checkuserisstaff, '/')
@login_required(login_url='/login')
def staffconsole(request):
    """
    View to the staff console page
    :param request: Django request object
    :return: HttpResponse
    """

    add_corp_account_form = AddCorporateAccountForm()
    upload_tags_form = UploadCorporateTagsForm()

    # locals() will expose them to the template
    return render(request, 'sheetapp/staffconsole.html', locals())


@user_passes_test(checkuserisstaff, '/')
@require_http_methods(['POST'])
@login_required(login_url='/login')
def createcorporateaccount(request):
    """
    View that handles the creation of a corporate account
    :param request:
    :return:
    """

    add_corp_account_form = AddCorporateAccountForm(request.POST or None)
    upload_tags_form = UploadCorporateTagsForm()
    add_account_process_messages = {}

    if request.method == 'POST':
        if add_corp_account_form.is_valid():
            # form is valid add the account
            email = request.POST.get('email', "")
            password = request.POST.get('password1', "")
            now = datetime.datetime.utcnow()
            now = now.replace(tzinfo=pytz.utc)
            user = User.objects.create_user(username=email, email=email, password=password)
            UserProfile.objects.get_or_create(user=user, registration_date=now, creation_date=now, is_corporate=True)

            add_account_process_messages['success'] = "The corporate account with email: %s has been added" % email

    return render(request, 'sheetapp/staffconsole.html', locals())


@user_passes_test(checkuserisstaff, '/')
@require_http_methods(['POST'])
@login_required(login_url='/login')
def uploadtags(request):
    """
    View for the upload of tags
    :param request:
    :return:
    """

    add_corp_account_form = AddCorporateAccountForm()
    upload_tags_form = UploadCorporateTagsForm(request.POST or None, request.FILES or None)
    upload_process_messages = {}
    if request.method == 'POST':
        if upload_tags_form.is_valid():
            account = request.POST.get('account', 0)
            numberoftags = request.POST.get('numberoftags', 0)
            csv_file = "/tmp/"+request.FILES['tagsfileupload'].name

            # open file get contents of csv into a list
            with open(csv_file, 'rb') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                tags_list = []
                for row in spamreader:
                    # add tags to list
                    tags_list.append(row)

            # delete the file
            try:
                os.remove(csv_file)
            except Exception:
                # do not do anything right now, if this fails its ok
                pass

            if tags_list[0][0] == "Row#":
                # file contained a header, remove the first line
                tags_list.pop(0)

            # get Tags objects and register them
            for tag_list in tags_list:
                listed_tag = Tag.objects.get(code=tag_list[2])
                now = datetime.datetime.utcnow()
                now = now.replace(tzinfo=pytz.utc)
                listed_tag.registered_date = now
                user = User.objects.get(id=account)
                listed_tag.user = user
                listed_tag.save()

            upload_process_messages['success'] = "Tags registered successfully!"

    return render(request, 'sheetapp/staffconsole.html', locals())
