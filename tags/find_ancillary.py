import re
from rockettags.main.screenelements import show_form_screen



# Move to find_ancillary
def show_found_tag_screen_front(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'tags', 'found_tag_1', include_fields, error_string, more_fields, exclude_fields, template_name='template')

def show_found_tag_screen_details(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'tags', 'found_tag_2', include_fields, error_string, more_fields, exclude_fields, template_name='template')

def show_found_tag_screen_confirm(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'tags', 'found_tag_confirm', include_fields, error_string, more_fields, exclude_fields, template_name='template')

def show_test_tag_screen_confirm(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'tags', 'test_tag_confirm', include_fields, error_string, more_fields, exclude_fields, template_name='template')

def show_want_register_tag(request, form, include_fields, error_string, more_fields={}, exclude_fields=[]) :
    return show_form_screen(request, form, 'tags', 'want_register', include_fields, error_string, more_fields, exclude_fields, template_name='template')

def validate_found_tag_input(request) :

    if request.method != 'POST' :
        return (False, 'Not in POST mode')

    tag_id = ''

    try :
        password = request.POST['tag_id']
    except :
        return (False, 'Not all required fields present')

    if len(tag_id) == 0 :
        return (False, 'RocketTag ID field is empty')

    return (True, '')

def get_found_tag_fields(request) :

    if request.method != 'POST' :
        return None

    tag_id = ''
    email = ''
    item_type = ''    
    test = False
    show_details = False
    prev_message_id = None
    
    try :
        tag_id = request.POST['tag_id']
    except :
        return None
    
    try :
        email = request.POST['email']
    except :
        email = ''   

    try :
        prev_message_id = request.POST['prev_message_id']
    except :
        prev_message_id = None  

    try :
        item_type = request.POST['item_type']
    except :
        item_type = ''

    try :
        test = request.POST['test']
    except :
        test = "false"

    if test == "false" or test == "False" :
        test = False
    elif test == "true" or test == "True" :
        test = True
    else :
        test = False

    try :
        show_details = request.POST['show_details']
    except :
        show_details = "false"

    if show_details == "false" or show_details == "False" :
        show_details = False
    elif show_details == "true" or show_details == "True" :
        show_details = True
    else :
        show_details = False

    found_tag_fields = {}
    found_tag_fields['tag_id'] = tag_id
    found_tag_fields['email'] = email
    found_tag_fields['prev_message_id'] = prev_message_id
    found_tag_fields['item_type'] = item_type
    found_tag_fields['test'] = test
    found_tag_fields['show_details'] = show_details

    return found_tag_fields

def get_found_detail_fields(request) :

    if request.method != 'POST' :
        return None

    if not "details" in request.POST :
        return None

    tag_id = ''
    email = ''
    firstname = ''
    lastname = ''
    address = ''
    phone_number = ''
    country = ''
    state = ''
    city = ''
    contact_method = ''
    #item_type = ''
    item_description = ''
    item_location = ''
    
    try :
        tag_id = request.POST['tag_id']        
        email = request.POST['email']
        firstname = request.POST['firstname']
        if firstname == 'first name' : firstname = ''
        lastname = request.POST['lastname']
        if lastname == 'last name' : lastname = ''
        address = request.POST['address']
        if address == 'address' : address = ''
        phone_number = request.POST['phone_number']
        if phone_number == 'phone number' : phone_number = ''
        country = request.POST['country']
        state = request.POST['state']
        city = request.POST['city']
        if city == 'city' : city = ''
        contact_method = request.POST['contact_method']
        if contact_method == 'preferred method of contact' : contact_method = ''
        #item_type = request.POST['item_type']
        item_description = request.POST['item_description']
        if item_description == 'description' : item_description = ''
        item_location = request.POST['item_location']
        if item_location == 'description' : item_location = ''
    except :
        return None

    found_tag_fields = {}
    found_tag_fields['tag_id'] = tag_id
    found_tag_fields['email'] = email
    found_tag_fields['firstname'] = firstname
    found_tag_fields['lastname'] = lastname
    found_tag_fields['address'] = address
    found_tag_fields['phone_number'] = phone_number
    found_tag_fields['country'] = country
    found_tag_fields['state'] = state
    found_tag_fields['city'] = city
    found_tag_fields['contact_method'] = contact_method
    #found_tag_fields['item_type'] = item_type
    found_tag_fields['item_description'] = item_description
    found_tag_fields['item_location'] = item_location
    
    return found_tag_fields


def get_item_types() :

    item_types = []
    item_types.append(('Other', 'Other'))
    item_types.append(('Bag', 'Bag'))
    item_types.append(('Camera', 'Camera'))
    item_types.append(('CarryBag', 'Carry Bag'))
    item_types.append(('cellphone', 'Cell Phone'))
    item_types.append(('cellphone', 'cellphone'))
    item_types.append(('Charger', 'Charger'))
    item_types.append(('Gameboy', 'Gameboy'))
    item_types.append(('Garmin', 'Garmin'))
    item_types.append(('Glasses ', 'Glasses '))
    item_types.append(('iPad', 'iPad'))
    item_types.append(('iPod', 'iPod'))
    item_types.append(('iPodTouch', 'iPod Touch'))
    item_types.append(('Kindle', 'Kindle'))
    item_types.append(('Luggage', 'Luggage'))
    item_types.append(('Magellan', 'Magellan'))
    item_types.append(('cellphone', 'Mobile Phone'))
    item_types.append(('MoneyClip', 'Money Clip'))
    item_types.append(('MP3Player', 'MP3 Player'))
    item_types.append(('MusicDevice', 'Music Device'))
    item_types.append(('NavigationDevice', 'Navigation Device'))
    item_types.append(('Navman', 'Navman'))
    item_types.append(('Nintendo', 'Nintendo'))
    item_types.append(('Notebook', 'Notebook'))
    item_types.append(('OtherElectronicDevice', 'Other Electronic Device'))
    item_types.append(('PC', 'Personal Computer'))
    item_types.append(('PortableComputer', 'Portable Computer'))
    item_types.append(('ReadingDevice', 'Reading Device'))
    item_types.append(('SchoolBag', 'School Bag'))
    item_types.append(('SportEquipment', 'Sport Equipment'))
    item_types.append(('Sunglasses', 'Sunglasses'))
    item_types.append(('Tablet', 'Tablet Computer'))
    item_types.append(('TomTom', 'TomTom'))
    item_types.append(('VideoCamera', 'Video Camera'))
    item_types.append(('VideoRecorder', 'Video Recorder'))
    item_types.append(('Wallet', 'Wallet'))

    return item_types

def get_contact_methods() :

    contact_methods = []
    contact_methods.append(('email', 'email'))
    contact_methods.append(('phone', 'phone call'))
    contact_methods.append(('mobile', 'text message'))
    #contact_methods.append(('post', 'posted letter'))

    return contact_methods


def detect_mobile(request) :

    reg_b = re.compile(r"iPad|android.+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", re.I|re.M)
    reg_v = re.compile(r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-", re.I|re.M)

    request.mobile = False
    if request.META.has_key('HTTP_USER_AGENT'):
        user_agent = request.META['HTTP_USER_AGENT']
        b = reg_b.search(user_agent)
        v = reg_v.search(user_agent[0:4])
        if b or v:
           request.mobile = True
           return True

    return False
           
    


