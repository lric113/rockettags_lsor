from django.db import models
from django.contrib.auth.models import User
from rockettags.store.models import Product
from datetime import datetime
import re
import random
import bad_words
from django.conf import settings
#from rockettags.main.models import create_log_entry

class TagType(models.Model):
    code = models.PositiveIntegerField(default=0)
    typename = models.CharField(null=False, blank=False, max_length=16)
    typeclass = models.CharField(null=True, blank=True, max_length=16) # reserved for number of days to extend/create subscription
    typesubclass = models.CharField(null=True, blank=True, max_length=16) # reserved for expiry time to live
    
class Tag(models.Model):

    code = models.CharField(null=False, blank=False, unique=True, max_length=8)
    user = models.ForeignKey(User, null=True, blank=True, related_name='tags', on_delete=models.SET_NULL)
    issued_date = models.DateTimeField(default=datetime.now())
    registered_date = models.DateTimeField(null=True, blank=True)
    status = models.PositiveIntegerField(default=0) # 0 - not registered, 1 = registered
    tag_type = models.PositiveIntegerField(default=0) # 0 - GENERATED, 1 = STANDARD, etc.
    product = models.ForeignKey(Product, null=True, blank=True, related_name='tags', on_delete=models.SET_NULL) # used for associating tags with a physical product or PROMOTION/COMPLIMENTARY
   

    def to_base36(self) :
      val = self.code
      result = val
      return result
    to_base36.short_description = 'Tag ID'

    def __unicode__(self) :
        return u'%s' % self.to_base36()

    # Factory Methods for creating Tags
    
    @classmethod
    def create(cls, tagcode=None) :
        if tagcode is None :
            return cls.create_from_random()
        else :
            return cls.create_from_given_code(tagcode)

    @classmethod
    def create_from_random(cls) :
        newtagcode = cls.generate_tagcode()
        newtag = Tag(code=newtagcode)
        newtag.issued_date = datetime.now()        
        return newtag

    @classmethod
    def generate_tagcode(cls) :
        maxnumber = pow(2, 32)

        randomnum = random.randint(1, maxnumber)
        randomtag = cls.convert_to_base36(randomnum)

        while cls.exists(randomtag) or not cls.is_valid(randomtag) :
            randomnum = random.randint(1, maxnumber)
            randomtag = cls.convert_to_base36(randomnum)

        return randomtag

    @classmethod
    def create_from_given_code(cls, newtagcode) :
        # don't accept if the tagcode is not valid or is offensive
        if not cls.is_valid(newtagcode) or cls.has_offensive_word(newtagcode) :
            return None
        # don't accept if the tagid is already assigned
        if cls.exists(newtagcode) :
            return None
        newtag = Tag(code=newtagcode)
        newtag.issued_date = datetime.now()
        return newtag


    # Extension to get method for locating by base36 tagcode
    @classmethod
    def get_from_base36(cls, tagcode) :
        found_tag = cls.objects.get(code=tagcode)
        return found_tag


    # Utility methods for converting between decimal and base36

    @classmethod
    def convert_to_base36(cls, val) :
      result = ''
      temp = []
      while val >= 36 :
       div = val / 36
       mod = val % 36
       temp.append(mod)
       val = div
      temp.append(val)

      while len(temp) > 0 :
       digit = temp.pop()
       if digit > 9 :
        result = '%s%s' % (result, chr(digit + 55))
       else :
        result = '%s%s' % (result, digit)
      return result

    @classmethod
    def convert_from_base36(cls, val) :
      return int(val, 36)


    # Validation methods

    @classmethod
    def is_valid(cls, tagcode) :
      if len(tagcode) > 7 : return False
      elif not re.search('([AEIOU]+)', tagcode) is None : return False
      elif not re.search('([0-9BCDFGHJKLMNPQRSTVWXYZ]{1,8})', tagcode) is None :
          tagvalue = cls.convert_from_base36(tagcode)
          if tagvalue <= pow(2, 32) : return True
      return False

    @classmethod
    def has_offensive_word(cls, tagcode) :
      for word in bad_words.offensive_words :
          pattern = '.*%s.*' % word
          if re.search(pattern, tagcode) :
              return word
      return None

    @classmethod
    def exists(cls, tagcode) :
        try :
            existing_tag = cls.objects.get(code=tagcode)
        except :
            existing_tag = None
        if existing_tag is not None :
            return True        
        return False

    @classmethod
    def create_pool(cls, request, poolsize, tag_type=0) :

        if poolsize == 0 :
            return None

        tags = []
        for i in range(0, poolsize) :

            new_tag = cls.create()
            new_tag.tag_type = tag_type
            new_tag.save() 

            #rockettags.main.models.create_log_entry('Generated tag code', 'Generated new tag code %s; number %d of 15; internal id %d' % (new_tag.to_base36(), i+1, new_tag.id), tablename='Tag')
            
            tags.append(new_tag)

        return tags

    @classmethod
    def obtain_pool(cls, request, poolsize, start_tag_id, end_tag_id) :

        tags = cls.objects.all()[start_tag_id-1:end_tag_id]

        return tags

    
    @classmethod
    def obtain_pool_of_codes(cls, request, poolsize, start_tag_id, end_tag_id) :

        tags = cls.objects.all()[start_tag_id-1:end_tag_id]

        tagcodes = []
        for current_tag in tags :

            #rockettags.main.models.create_log_entry('Obtained tag code', 'Obtained existing tag code %s; number %d of 15; internal id %d' % (current_tag.to_base36(), i+1, current_tag.id), tablename='Tag')
            
            tagcode = current_tag.to_base36()

            tagcodes.append(tagcode)

        return tagcodes

    @classmethod
    def update_pool_tag_type(cls, request, start_tag_id, end_tag_id, new_tag_type) :

        tags = cls.objects.all()[start_tag_id-1:end_tag_id]

        for atag in tags :
            atag.tag_type = new_tag_type
            atag.save()

        return tags

    @classmethod
    def update_tag_type_by_code(cls, request, tag_code, new_tag_type) :

        atag = cls.objects.get(code=tag_code)

        if not atag is None :
            atag.tag_type = new_tag_type
            atag.save()

        return atag


# Move to Tag model
def primary_tag(user) :
    the_primary_tag = None
    if user is None or user.is_anonymous() :
        return None
    
    current_profile = user.get_profile()
    if current_profile is None :
        return None
    
    tag_code = None
    if not current_profile.tag_code is None and len(current_profile.tag_code) > 0 :
        tag_code = current_profile.tag_code
        try :
            the_primary_tag = user.tags.get(code=tag_code)
        except :
            the_primary_tag = None

    if the_primary_tag is None :
        try :
            the_primary_tag = user.tags.all()[0]
        except :
            the_primary_tag = None
                
    return the_primary_tag

# Move to Tag model
def update_primary_tag(user, tagcode) :
    the_primary_tag = None
    if user is None or user.is_anonymous() :
        return None
    
    current_profile = user.get_profile()
    if current_profile is None :
        return None
    
    new_tag = get_tag(tagcode)
    if not new_tag is None :
        current_profile.tag_code = tagcode
        current_profile.save()
    
    return new_tag


def get_tag_codes(request) :

    tag_codes = {}

    mytag = primary_tag(request.user)
         
    if not mytag is None :
        tagid = mytag.to_base36()
    else :
        tagid = ''

    tag_codes['tagid'] = tagid
    tag_codes['tag'] = mytag

    return tag_codes

def get_tag_by_id(tagid) :
    if tagid is None : return None
    stagid=str(tagid)
    if len(stagid) == 0 : return None
    tag = Tag.objects.get(pk=tagid)
    return tag

def get_tag(tagcode) :
    if tagcode is None : return None
    try :
        tag = Tag.objects.get(code=tagcode)
    except :
        tag = None
    return tag

def get_tag_type(tag) :
    if tag is None : return None
    tag_type = TagType.objects.get(code=tag.tag_type)
    return tag_type

def get_last_tag() :
    tags = Tag.objects.all()
    tag = tags[len(tags)-1]
    return tag

def tag_exists(tagcode) :
    tag = get_tag(tagcode)
    if tag is None : return False
    else : return True

def get_tag_details(tagcode) :

    tag_details = {}
    tag = None
    tag = get_tag(tagcode)
    if tag is None : return None

    tag_type = None
    tag_type = get_tag_type(tag)
    if tag_type is None : return None

    tag_details['tag_code'] = tag.to_base36()
    tag_details['tag_id'] = tag.id
    tag_details['tag_status'] = tag.status

    tag_details['tag_typename'] = tag_type.typename
    tag_details['tag_typeclass'] = tag_type.typeclass
    tag_details['tag_typesubclass'] = tag_type.typesubclass

    if not tag.product is None :
        tag_details['tag_product_type'] = tag.product.product_type
        tag_details['tag_product_name'] = tag.product.product_name
        tag_details['tag_product_expiry_date'] = tag.product.expiry_date
        
    return tag_details

def get_tag_user(tagcode) :
    if tagcode is None : return None
    try :
        tag = Tag.objects.get(code=tagcode)
        user = tag.user
    except :
        tag = None
        user = None
    return user

def register_tag(tagcode, user) :

    if not tag_exists(tagcode) :
        return False
    if user is None :
        return False

    tag = get_tag(tagcode)
    if tag.status > 0 :
        return False

    tag.registered_date = datetime.now()
    tag.status = 1
    tag.save()
    user.tags.add(tag)
    user.save()

    return True
    

def get_batch_details(tagcode) :
    current_tag = get_tag(tagcode)
    if current_tag is None : return {'error' : "invalid tagcode", 'current_tag' : None, 'batch_tag_type' : None, 'start_tag' : None, 'end_tag' : None}

    tag_id = current_tag.id

    all_tags = Tag.objects.all()
    num_tags = len(all_tags)

    end_id = settings.TAG_BATCH_START - 1

    while end_id < tag_id and end_id <= num_tags :
        end_id = end_id + 15

    if tag_id <= end_id and end_id <= num_tags :        
        start_id = end_id - 14
    else :
        return {'error' : "couldnt find tag id batch", 'current_tag' : None, 'batch_tag_type' : None, 'start_tag' : None, 'end_tag' : None}
    
    start_tag = get_tag_by_id(start_id)        
    end_tag = get_tag_by_id(end_id)
    current_tag_type = get_tag_type(current_tag)

    if start_tag.tag_type != end_tag.tag_type :
        return {'error' : "mismatching start/end tag types - %s and %s" % (start_tag.tag_type, end_tag.tag_type), 'current_tag' : current_tag, 'batch_tag_type' : current_tag_type, 'start_tag' : start_tag, 'end_tag' : end_tag}
    if start_tag.tag_type != current_tag.tag_type :
        return {'error' : "mismatching batch and current tag types - %s and %s" % (start_tag.tag_type, current_tag.tag_type), 'current_tag' : current_tag, 'batch_tag_type' : current_tag_type, 'start_tag' : start_tag, 'end_tag' : end_tag}

    return {'error' : None, 'current_tag' : current_tag, 'batch_tag_type' : current_tag_type, 'start_tag' : start_tag, 'end_tag' : end_tag}
    

class FoundMessage(models.Model):

    tag = models.ForeignKey(Tag, related_name='messages', null=True, blank=True, on_delete=models.SET_NULL)
    email = models.EmailField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    firstname = models.CharField(max_length=128, blank=True, null=True)
    lastname = models.CharField(max_length=128, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=64, blank=True, null=True)
    state = models.CharField(max_length=64, blank=True, null=True)
    country = models.CharField(max_length=5, blank=True, null=True)
    contact_method = models.CharField(max_length=255, blank=True, null=True)
    item_description = models.CharField(max_length=255, blank=True, null=True)
    item_location = models.CharField(max_length=255, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)
    generated_date = models.DateTimeField(default=datetime.now())
    acted_upon_date = models.DateTimeField(null=True, blank=True)
    
def get_found_messages_for_user(user, start_num=0, end_num=0) :

    messages = []
    
    user_tags = user.tags.all()

    for tag in user_tags :

        current_messages = tag.messages.order_by('-generated_date')

        for message in current_messages :
            messages.append(message)

    if end_num == 0 :
        return messages
    else :
        return messages[start_num:end_num+1]

def get_all_found_messages(start_num=0, end_num=0) :

    messages = []

    messages = FoundMessage.objects.order_by('-generated_date')

    if end_num == 0 :
        return messages
    else :
        return messages[start_num:end_num+1]

def get_found_message(message_id=1) :

    message = None

    message = FoundMessage.objects.get(id=message_id)

    return message

def get_random_message():

    message = None

    messages = FoundMessage.objects.all()

    random_key = random.randint(1, len(messages))

    message = messages[random_key]

    return message

def get_random_found_messages(start_num=0, end_num=0):

    ids = []
    messages = []
    num_to_fetch = 0
    num_available = len(FoundMessage.objects.all())

    if end_num == 0 or end_num < start_num :
        num_to_fetch = num_available
    else:
        num_to_fetch = end_num - start_num + 1
    
    while len(ids) < num_to_fetch:
        message = get_random_message()
        if not message.id in ids:
            ids.append(message.id)
            messages.append(message)

    return messages

def update_tag_types(new_tag_type=206, target_tag_types=[1, 2, 106, 107, 108]):

    changed_tag_codes = []
    tags = Tag.objects.all()
    for tag in tags:
        tagid = tag.id
        code = tag.to_base36()
        status = tag.status
        tagtype = tag.tag_type

        found = False

        if (tagtype in target_tag_types) and (status == 0):
            found = True

        if found:
            tag.tag_type = new_tag_type
            tag.save()
            changed_tag_codes.append(code)

    return changed_tag_codes
    
    
