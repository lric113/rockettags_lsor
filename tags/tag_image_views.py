import os
import mimetypes
import sys

from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from django.contrib import auth
from django.shortcuts import render_to_response
from django.conf import settings

from rockettags.main.models import create_log_entry
from rockettags.tags.models import Tag
from rockettags.tags.models import primary_tag, get_tag_codes
from generate_tag_images import generate_image_file, generate_image_files


# This should be called generate_tag_images
def display_tag_images(request, include_tag_id_field=None) :

    image_type = ''
    num_tags = 0
    do_REST = 0
    start_tag_id = 0
    end_tag_id = 0
    
    if request.method == 'GET' :

        if len(request.GET) == 0 :

            return HttpResponse('What is this request method? - %s' % request.method)

        else :
            
            if 'num_tags' in request.GET :
                num_tags = int(request.GET['num_tags'])
            if 'do_REST' in request.GET :
                do_REST = int(request.GET['do_REST'])
            if 'image_type' in request.GET :
                image_type = request.GET['image_type']
            if 'start_tag_id' in request.GET :
                start_tag_id = int(request.GET['start_tag_id'])
            if 'end_tag_id' in request.GET :
                end_tag_id = int(request.GET['end_tag_id'])

            if start_tag_id == 0 or end_tag_id == 0 or end_tag_id < start_tag_id :
                return HttpResponse('Invalid tag id range')

            num_tags = end_tag_id - start_tag_id + 1
            
    else :
        return HttpResponse('What is this request method? - %s' % request.method)

    if len(image_type) == 0 :
        image_type = 'JPG'

    final_files_directory = 'final'
    if image_type == 'MOO' :
        final_files_directory = 'moo'
    else :
        final_files_directory = 'final'        

    #if end_tag_id == 0 :
    #    create_log_entry('Create tag pool', 'Creating pool of %d tags' % num_tags, tablename='Tag')
    #    tagids = Tag.create_pool(request, num_tags)
    #else :
    create_log_entry('Get tag pool', 'Getting pool of %d tags from %d to %d' % (num_tags, start_tag_id, end_tag_id), tablename='Tag')
    #tagids = Tag.obtain_pool_of_codes(request, num_tags, start_tag_id, end_tag_id)

    # 02012013 Include support for extra tag id field
    tags = Tag.obtain_pool(request, num_tags, start_tag_id, end_tag_id)

    path_to_files = '%s/html/static' % settings.WWW_ROOT
    if sys.platform.startswith('linux'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    elif sys.platform.startswith('win'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    else :
        path_to_files = '%s/html/static' % settings.WWW_ROOT

    path_to_output_files = '%s/output/%s' % (path_to_files, final_files_directory)

    #request.META['tagids'] = tagids

    # 02012013 Include support for extra tag id field
    image_files_string = ""
    of = open('%s/tags.txt' % path_to_output_files, 'w')
    try :
        of.write("%d:%d\n" % (start_tag_id, end_tag_id))
        for tag in tags :
            if not include_tag_id_field is None and include_tag_id_field == 1:
                of.write('%s:%s' % (tag.to_base36(), tag.id))
                of.write('\n')
                image_files_string = "%s%s:%s\n" % (image_files_string, tag.to_base36(), tag.id)
            else:
                of.write(tag.to_base36())
                of.write('\n')
                image_files_string = "%s%s\n" % (image_files_string, tag.to_base36())
    except:
        of.close()

    if do_REST :
        if len(tags) == 0 :
            image_files_string = "<empty>"
        output_string = "%d:%d\n%s" % (start_tag_id, end_tag_id, image_files_string)
        return HttpResponse('%s' % output_string)
    else :
        return HttpResponse('<html><head><body>%s<BR>\n<BR>\n<PRE>%s</PRE><BR><BR><a href=\"/static/output/moo/packs.html\">Wait for results</a></body></html>' % (path_to_output_files, image_files_string))

    #image_dict = {}
    #images_directory = ''
    #for tagid in tagids :

    #    #request.META['tagid'] = tagid
    #    (images_directory, image_files) = generate_images(request, tagid, image_type, final_files_directory=final_files_directory)
    #    image_dict[tagid] = image_files
    #    create_log_entry('Generated tag images', 'Generated images for tag %s' % tagid, tablename='Tag')

    #image_files_string = ''

    #for tagid in tagids :
    #    for image_file in image_dict[tagid] :
    #        image_files_string = '%s%s\n' % (image_files_string, image_file)

    #outfile = open("%s/files.txt" % images_directory, 'w')
    #outfile.write(image_files_string)
    #outfile.close()            
        
    #return HttpResponse('<html><head><body>%s<BR>\n<BR>\n<PRE>%s</PRE></body></html>' % (images_directory, image_files_string))





def generate_images(request, tagid, image_type, final_files_directory=None) :
    if len(tagid) == 0 or not Tag.is_valid(tagid) :
        return 'Invalid tag id'
    if len(image_type) == 0 or not image_type in ['PNG', 'JPG', 'PDF', 'MOO'] :
        return 'Invalid image type'

    input_type = 'PNG'
    result_type = 'PNG'
    if image_type == 'PNG' :
        input_type = 'PNG'
        result_type = 'PNG'
    elif image_type == 'JPG' :
        input_type = 'JPG'
        result_type = 'JPG'
    elif image_type == 'PDF' :
        input_type = 'PNG'
        result_type = 'PDF'
    elif image_type == 'MOO' :
        input_type = 'MOO'
        result_type = 'JPG'

    path_to_files = '%s/html/static' % settings.WWW_ROOT
    if sys.platform.startswith('linux'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    elif sys.platform.startswith('win'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    else :
        path_to_files = '%s/html/static' % settings.WWW_ROOT
        
    filepaths = generate_image_files(request, tagid, input_type, path_to_files=path_to_files, final_files_directory=final_files_directory)

    output_directory = '%s/output/%s' % (path_to_files, final_files_directory)

    return (output_directory, filepaths)

def display_images(request) :

    tagid = ''
    image_type = ''
    result_type = ''

    if request.method == 'GET' :

        if len(request.GET) == 0 :

            # display a form
            existing_tagid = ''
            existing_tag = primary_tag(request.user)
            if not existing_tag is None :
                existing_tagid = existing_tag.to_base36()
            else :
                existing_tagid = ''

            t = loader.get_template('tags/display_image_form.html')
            my_codes = {
                        'tagid' : existing_tagid,
                        }
            c = RequestContext(request, my_codes)
            return HttpResponse(t.render(c))

        else :
            
            if 'tagid' in request.GET :
                tagid = request.GET['tagid']
            if 'image_type' in request.GET :
                image_type = request.GET['image_type']

    elif request.method == 'POST' :

        if 'tagid' in request.POST :
            tagid = request.POST['tagid']
        if 'image_type' in request.POST :
            image_type = request.POST['image_type']

    else :
        return HttpResponse('What is this request method? - %s' % request.method)

    if len(image_type) == 0 :
        image_type = 'JPG'

    final_files_directory = 'final'
    if image_type == 'MOO' :
        final_files_directory = 'moo'
    else :
        final_files_directory = 'final'        
    
    (images_directory, image_files) = generate_images(request, tagid, image_type, final_files_directory=final_files_directory)

    image_files_string = ''
    for x in image_files :
        image_files_string = '%s%s<BR>\n' % (image_files_string, x)
        
    return HttpResponse('<html><head><body>%s</body></html>' % (image_files_string))





def generate_image(request, tagid, colour, image_type, final_files_directory=None) :
    if len(tagid) == 0 or not Tag.is_valid(tagid) :
        return 'Invalid tag id'
    if len(colour) == 0 or not colour in ['black', 'blue', 'green', 'pink', 'red', 'orange'] :
        return 'Invalid colour'
    if len(image_type) == 0 or not image_type in ['PNG', 'JPG', 'PDF', 'MOO'] :
        return 'Invalid image type'

    input_type = 'PNG'
    result_type = 'PNG'
    if image_type == 'PNG' :
        input_type = 'PNG'
        result_type = 'PNG'
    elif image_type == 'JPG' :
        input_type = 'JPG'
        result_type = 'JPG'
    elif image_type == 'PDF' :
        input_type = 'PNG'
        result_type = 'PDF'
    elif image_type == 'MOO' :
        input_type = 'MOO'
        result_type = 'JPG'
    
    if sys.platform.startswith('linux'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    elif sys.platform.startswith('win'):
        path_to_files = '%s/html/static' % settings.WWW_ROOT
    else :
        path_to_files = '%s/html/static' % settings.WWW_ROOT
        
    #debug_string = create_tag_image(path_to_files, tagid, colour, input_type, result_type)
    filepath = generate_image_file(request, tagid, colour, input_type, path_to_files=path_to_files, final_files_directory=final_files_directory)
    #request.META['test'] = debug_string
    #test2 = request.POST['test2']

    image_ext = 'png'
    if result_type == 'PNG' : image_ext = 'png'
    elif result_type == 'JPG' : image_ext = 'jpg'
    elif result_type == 'PDF' : image_ext = 'pdf'

    return 'tag_%s_%s.%s' % (tagid, colour, image_ext)

    
# rename here as display_image_view
def display_image(request) :

    tagid = ''
    colour = ''
    image_type = ''
    result_type = ''

    if request.method == 'GET' :

        if len(request.GET) == 0 :

            # display a form
            existing_tagid = ''
            existing_tag = primary_tag(request.user)
            if not existing_tag is None :
                existing_tagid = existing_tag.to_base36()
            else :
                existing_tagid = ''

            t = loader.get_template('tags/display_image_form.html')
            my_codes = {
                        'tagid' : existing_tagid,
                        }
            c = RequestContext(request, my_codes)
            return HttpResponse(t.render(c))

        else :
            
            if 'tagid' in request.GET :
                tagid = request.GET['tagid']
            if 'colour' in request.GET :
                colour = request.GET['colour']
            if 'image_type' in request.GET :
                image_type = request.GET['image_type']

    elif request.method == 'POST' :

        if 'tagid' in request.POST :
            tagid = request.POST['tagid']
        if 'colour' in request.POST :
            colour = request.POST['colour']
        if 'image_type' in request.POST :
            image_type = request.POST['image_type']

    else :
        return HttpResponse('What is this request method? - %s' % request.method)
    
    image_file = generate_image(request, tagid, colour, image_type, final_files_directory='final')

    if image_type == 'PDF' :
        return HttpResponse('<html><head><meta http-equiv=\"refresh\" content=\"2;url=/tags/download/%s/\"></head><body><a href=\"/static/output/final/%s\">%s</a></body></html>' % (image_file, image_file, image_file))
    else :
        return HttpResponse('<html><head><meta http-equiv=\"refresh\" content=\"2;url=/tags/download/%s/\"></head><body><img src=\"/static/output/final/%s\"></body></html>' % (image_file, image_file))


def download_image(request, filename=''):

    if len(filename) == 0 :
        return HttpResponseRedirect('/')
    
    if sys.platform.startswith('linux'):
        path_to_files = '%s/html/static/output/final' % settings.WWW_ROOT
    elif sys.platform.startswith('win'):
        path_to_files = '%s/html/static/output/final' % settings.WWW_ROOT
    else :
        path_to_files = '%s/html/static/output/final' % settings.WWW_ROOT

    full_path = '%s/%s' % (path_to_files, filename)
    the_file = open(full_path,"rb")
    
    mimetype = mimetypes.guess_type(filename)[0]
    if not mimetype: mimetype = "application/octet-stream"

    response = HttpResponse(the_file.read(), mimetype=mimetype)
    response["Content-Disposition"]= "attachment; filename=%s" % os.path.split(filename)[1]
    return response        


