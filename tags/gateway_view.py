import sys
import os
import datetime

from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.conf import settings

#import recaptcha.client.captcha

#from rockettags.tags.models import Tag
#from rockettags.tags.models import primary_tag, get_tag_codes, get_tag, get_tag_type, get_tag_by_id, get_batch_details
#from rockettags.main.models import UserProfile
from rockettags.main.models import create_log_entry

from rockettags.links import links
from rockettags.fragments import query_string
from rockettags.main.screenelements import get_query_string_fields
from rockettags.tags.find_ancillary import detect_mobile

from rockettags.tags.find_views import find
#from rockettags.tags.find_views import captcha_handler, show_found_a_tag_text
#from rockettags.tags.tag_image_views import display_tag_images, display_images, display_image, download_image

from rockettags.main.redeem_view import validate_code, redeem_code

from django.views.decorators.csrf import csrf_exempt

# Handles the main gateway url for the site '/t/<tagcode>'
# If the tag_id is reserved process it as is
# otherwise validate the id then perform an action based on the result:
# either go to registration for a new unregistered tag
# or go to find a tag for a valid registered tag
# If the tag is not provided or is invalid go to find a tag for now
#
# In each case test if the user has come in on a mobile device and handle accordingly
#
@csrf_exempt
def gateway(request, tag_id=None) :

    # Grab the tag id wherever it is - that is if it is at the end of the command sequence
    # or is provided in the GET dictionary
    if tag_id is None:
        if 'id' in request.GET:
            tag_id = request.GET['id']
        elif 'tag_id' in request.GET:
            tag_id = request.GET['tag_id']
        if not tag_id is None and len(tag_id) == 0:
            tag_id = None
            
    if not tag_id is None and tag_id in settings.RESERVED_TAG_CODES :
        return HttpResponseRedirect(settings.RESERVED_TAG_CODES[tag_id])


    (success, code, details, validate_code_fields, dummy) = validate_code(request, code=tag_id)

    request.META['details'] = details
    request.META['validate_fields'] = validate_code_fields
    #name = request.POST['name']

    if success:
        if details['response_type'] == 1:
            
            if detect_mobile(request) :
                return HttpResponseRedirect('%s?id=%s' % (links['mobile_register'], code))
            else:
                return redeem_code(request, code=code)
            
        elif details['response_type'] == 2:
            
            if detect_mobile(request) :
                return HttpResponseRedirect('%s?id=%s' % (links['mobile_find'], code))
            else:
                return find(request, code)
            
    return find(request, None)    

