from models import Tag, FoundMessage, TagType
from django.contrib import admin

class TagAdmin(admin.ModelAdmin):
    # ...
    list_display = ('id', 'to_base36', 'user', 'issued_date')

class FoundMessageAdmin(admin.ModelAdmin):
    # ...
    list_display = ('id', 'tag', 'email')

class TagTypeAdmin(admin.ModelAdmin):
    # ...
    list_display = ('code', 'typename', 'typeclass')

    
admin.site.register(Tag, TagAdmin)
admin.site.register(FoundMessage, FoundMessageAdmin)
admin.site.register(TagType, TagTypeAdmin)
