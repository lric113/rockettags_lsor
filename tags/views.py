import sys
import os
import datetime

from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from django.contrib import auth
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.conf import settings

#import recaptcha.client.captcha

from rockettags.tags.models import Tag
from rockettags.tags.models import primary_tag, get_tag_codes, get_tag, get_tag_type, get_tag_by_id, get_batch_details, register_tag
from rockettags.main.models import UserProfile, get_user
from rockettags.main.models import create_log_entry

from rockettags.tags.find_views import find, captcha_handler, show_found_a_tag_text
from rockettags.tags.tag_image_views import display_tag_images, display_images, display_image, download_image

from rockettags.links import links
from rockettags.fragments import query_string, get_parameter_from_session
from rockettags.main.screenelements import get_query_string_fields, generate_page_from_fragments
#from rockettags.tags.find_ancillary import detect_mobile
from rockettags.store import backoffice
from rockettags.tags.gateway_view import gateway
from rockettags.main.redeem_view import validate_code

from rockettags.main import mailers

from django.views.decorators.csrf import csrf_exempt

from rockettags.tags.models import update_tag_types

# rename here as display_image_view
def assign_view(request, tagid=None) :

    tagid = 0
    tagcode = ''
    email = ''

    if request.user.is_anonymous() or not request.user.is_authenticated() :
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')
   
    if request.method == 'GET' :

        if len(request.GET) == 0 :

            # display a form
            t = loader.get_template('tags/assign_tag.html')
            my_codes = {
                        }
            c = RequestContext(request, my_codes)
            return HttpResponse(t.render(c))

        else :
            
            if 'id' in request.GET :
                tagid = request.GET['id']
            if 'code' in request.GET :
                tagcode = request.GET['code']
            if 'email' in request.GET :
                email = request.GET['email']

    elif request.method == 'POST' :

        if 'id' in request.POST :
            tagid = request.POST['id']
        if 'code' in request.POST :
            tagcode = request.POST['code']
        if 'email' in request.POST :
            email = request.POST['email']

    else :
        return HttpResponse('What is this request method? - %s' % request.method)
    
    return assign(request, tagid, tagcode, email)



def assign(request,tagid=None, tagcode=None, email=None) :

    stagid = str(tagid)
    if len(stagid) == 0 : tagid = None

    user = None
    if not email is None and len(email) > 0:
        if not request.user.is_staff :
            return HttpResponse('You need to be staff to use assign to a specific email <a href=\"/login\">Login to Rocket Tags</a>')               

        user = None
        try :
            user = User.objects.get(email=email)
        except :
            user = None
        if user is None :
            return HttpResponse('Invalid email address %s for user provided' % email)

    else :
        user = request.user
        
    
    if tagid is None and tagcode is None :            
        return HttpResponse('No tag was provided!')
    else :
        if not tagid is None :
            newtag = get_tag_by_id(tagid)
        else :
            newtag = get_tag(tagcode)
            
        if newtag is None :
            return HttpResponse('Invalid tag!')

        tagcode = newtag.to_base36()
        
        newtag.status = 1 # registered
        newtag.registered_date = datetime.datetime.now()
        user.tags.add(newtag)
        newtag.save()
        current_profile = user.get_profile()
        if current_profile.tag_code is None or len(current_profile.tag_code) == 0 :
            current_profile.tag_code = tagcode
            current_profile.registration_date = datetime.datetime.now()
            current_profile.save()
            # 14052012 If staff mark user as subscribed at this point if they are subscribed but waiting
            if request.user.is_staff and current_profile.status == 1 :
                current_profile.status = 2
                current_profile.save()
                backoffice.update_subscription(user.email, None, None, None, current_profile.status)

        tag_type = get_tag_type(newtag)

        return HttpResponse('new tag (id = %s, code = %s) of type %s with type value %s assigned to %s' % (newtag.id, newtag.to_base36(), tag_type.typename, tag_type.typeclass, user.email))



def register_tag_view(request, page_name='reg_front', code=None, email=None):

    (validated, code, details, validate_code_fields, dummy) = validate_code(request, code)
    if validate_code_fields is None: validate_code_fields = {}
    
    request.META['details'] = details
    request.META['validate_fields'] = validate_code_fields
    #name = request.POST['name']

    if email is None:
        email = get_parameter_from_session(request, 'email')
    if email is None:
        if request.user.is_authenticated() and not request.user.is_anonymous():
            email = request.user.email
            
    validate_code_fields['email'] = email

    if page_name == "reg_front":
        if not email is None and not code is None:        
            page_name = "reg_confirm"

    if page_name == "reg_confirm":

        request.META['validated'] = validated
        #name = request.POST['name']

        if not validated is None and validated is True:
            response_type = None
            if 'response_type' in details: response_type = details['response_type']

            
            
            if response_type is None or response_type == 2:
                
                details['register_tag_success'] = False
                page_name = "reg_processed"
                                
        elif not validated is None and validated is False:
            details['register_tag_success'] = False
            page_name = "reg_processed"
            
        #page_name = "reg_processed"
        
    elif page_name == "reg_processed":

        
        if not validated is None and validated is True:
            response_type = None
            if 'response_type' in details: response_type = details['response_type']

            if response_type == 1:

                if request.user.is_authenticated() and not request.user.is_anonymous() :
                    success = register_tag(code, request.user)

                    if success:
                        mailers.mail_add_a_tag(request, request.user, 'tags', None, tag_code=code)
                    
                elif not email is None:
                    user = get_user(email)
                    if not user is None:
                        success = register_tag(code, user)

                        if success:
                            mailers.mail_add_a_tag(request, user, 'tags', None, tag_code=code)

                    else:
                        success = False
                details['register_tag_success'] = success

            else:
                details['register_tag_success'] = False
        else:
            if details is None: details = {}
            details['register_tag_success'] = False

    return generate_page_from_fragments(request, 'tags', page_name, validate_code_fields, details, [], 'template')



def list_view(request) :

    if request.user.is_anonymous() or not request.user.is_authenticated() :
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff :
        return HttpResponse('You need to be staff to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    tagid = None
    tagcode = None

    if request.method == 'GET' :

        if len(request.GET) == 0 :

            tags = Tag.objects.order_by('id')

            return render_to_response('tags/list_tags.html', locals(), context_instance=RequestContext(request))

        else :
            
            if 'id' in request.GET :
                tagid = request.GET['id']
            if 'code' in request.GET :
                tagcode = request.GET['code']

    elif request.method == 'POST' :

        if 'id' in request.POST :
            tagid = request.POST['id']
        if 'code' in request.POST :
            tagcode = request.POST['code']

    if tagid is None and tagcode is None :
        return HttpResponse('Invalid arguments!')

    tag = None

    if not tagid is None :
        tag = get_tag_by_id(tagid)
    else :
        try :
            tag = get_tag_by_id(tagcode)
        except :
            tag = None
        if tag is None :
            tag = get_tag(tagcode)

    registered = False
    if tag.status == 1 : registered = True

    owner_name = 'None'
    if not tag.user is None :
        owner_name = tag.user.get_full_name()
        
    tag_type = None
    tag_type = get_tag_type(tag)
    if not tag_type.typeclass is None and len(tag_type.typeclass) > 0 :
        tag_type_details = "%s %s (%s days)" % (str(tag_type.code), tag_type.typename, tag_type.typeclass)
    else :
        tag_type_details = "%s %s" % (str(tag_type.code), tag_type.typename) 


    response = "<html><head></head><body>"
    response = "%s<h3>Tag Details</h3>\n\n" % response
    response = "%s<table border=1><br>\n" % response
    response = "%s<tr>\n<td>ID</td><td>%d</td>\n</tr>\n" % (response, tag.id)
    response = "%s<tr>\n<td>Code</td><td>%s</td>\n</tr>\n" % (response, tag.to_base36())
    response = "%s<tr>\n<td>Registered</td><td>%s</td>\n</tr>\n" % (response, str(registered))
    response = "%s<tr>\n<td>Owner</td><td>%s</td>\n</tr>\n" % (response, owner_name)
    response = "%s<tr>\n<td>Tag Type</td><td>%s</td>\n</tr>\n" % (response, tag_type_details)
    response = "%s</table><br><br>\n" % response

    batch_details = get_batch_details(tag.code)

    if not batch_details is None :

        response = "%s<h3>Batch Details</h3>\n\n" % response
        response = "%s<table border=1><br>\n" % response

        if not batch_details['error'] is None :
            response = "%s<tr>\n<td>Error</td><td>%s</td>\n</tr>\n" % (response, batch_details['error'])
        if not batch_details['current_tag'] is None :
            response = "%s<tr>\n<td>Batch Number</td><td>%d to %d</td>\n</tr>\n" % (response, batch_details['start_tag'].id, batch_details['end_tag'].id)
            response = "%s<tr>\n<td>Start code</td><td>%s</td>\n</tr>\n" % (response, batch_details['start_tag'].code)
            response = "%s<tr>\n<td>End code</td><td>%s</td>\n</tr>\n" % (response, batch_details['end_tag'].code)
            if batch_details['error'] is None :
                response = "%s<tr>\n<td>Type</td><td>%s</td>\n</tr>\n" % (response, batch_details['batch_tag_type'].typename)
                if not batch_details['batch_tag_type'].typeclass is None and len(batch_details['batch_tag_type'].typeclass) > 0 :
                    response = "%s<tr>\n<td>Type Class</td><td>%s days</td>\n</tr>\n" % (response, batch_details['batch_tag_type'].typeclass)
                        
        response = "%s</table>\n" % response

    response = "%s</body></html>" % response

    return HttpResponse(response)
            

@csrf_exempt
def gateway_view(request, tag_id=None) :
    return gateway(request, tag_id)


@csrf_exempt
def find_view(request, tag_id=None) :
    return find(request, tag_id)


@csrf_exempt
def captcha_view(request) :
    return captcha_handler(request)

@csrf_exempt
def show_found_a_tag_text_view(request) :
    return show_found_a_tag_text(request)


def generate_new_tags_view(request) :

    do_REST = 0
    if 'do_REST' in request.GET :
        do_REST = int(request.GET['do_REST'])

    # 17092012 introduce passphrase protection for REST urls
    # 02012013 introduce support for additional tag id field included in REST output
    passPhrase = ''
    if 'psp' in request.GET :
        passPhrase = request.GET['psp']

    include_tag_id_field = 0
    if 'include_tag_id_field' in request.GET:
        include_tag_id_field = int(request.GET['include_tag_id_field'])
        
    if do_REST == 1 :
        if len(passPhrase) <= 0 or passPhrase != settings.REST_PASSPHRASE :
            return HttpResponse('Incorrect passphrase!')

    if do_REST == 0 :
        if request.user.is_anonymous() or not request.user.is_authenticated() :
            return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

        if not request.user.is_staff :
            return HttpResponse('You need to be staff to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    num_tags = 0
    tag_type = 0
    
    if request.method == 'GET' :

        if len(request.GET) == 0 :

            return HttpResponse('What is this request method? - %s' % request.method)

        else :
            
            if 'num_tags' in request.GET :
                num_tags = int(request.GET['num_tags'])
            if 'tag_type' in request.GET :
                tag_type = int(request.GET['tag_type'])

            if num_tags <= 0 :
                return HttpResponse('Invalid tag number')
            
    else :
        return HttpResponse('What is this request method? - %s' % request.method)

    create_log_entry('Create tag pool', 'Creating pool of %d tags' % num_tags, tablename='Tag')
    tags = Tag.create_pool(request, num_tags, tag_type)
           
    tagid_string = ""
    
    for tag in tags :
        tagid_string = "%s%d = %s<BR>\n" % (tagid_string, tag.id, tag.to_base36())

    if do_REST == 1 :
        output_string = ""
        first_tag = tags[0]
        first_tag_id = first_tag.id
        last_tag = tags[len(tags)-1]
        last_tag_id = last_tag.id
        output_string = "%d:%d:%d\n" % (first_tag_id, last_tag_id, tag_type)
        for tag in tags :
            if include_tag_id_field == 1:
                output_string = "%s%s:%s\n" % (output_string, tag.to_base36(), tag.id)
            else:
                output_string = "%s%s\n" % (output_string, tag.to_base36())

        return HttpResponse(output_string)

    return HttpResponse(tagid_string)

def update_tag_type_view(request) :

    if request.user.is_anonymous() or not request.user.is_authenticated() :
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff :
        return HttpResponse('You need to be staff to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    start_tag_id = 0
    end_tag_id = 0
    tag_type = 0
    tag_code = None
    
    if request.method == 'GET' :

        if len(request.GET) == 0 :

            return HttpResponse('What is this request method? - %s' % request.method)

        else :

            if 'tag_code' in request.GET :
                tag_code = request.GET['tag_code']
            if 'start_tag_id' in request.GET :
                start_tag_id = int(request.GET['start_tag_id'])
            if 'end_tag_id' in request.GET :
                end_tag_id = int(request.GET['end_tag_id'])
            if 'tag_type' in request.GET :
                tag_type = int(request.GET['tag_type'])

            if tag_code is None :
                if start_tag_id == 0 or end_tag_id == 0 or end_tag_id < start_tag_id :
                    return HttpResponse('Invalid tag id range')
            elif len(tag_code) <= 0 :
                return HttpResponse('Invalid tag code')
            if tag_type < 0 : return HttpResponse('Invalid tag type')
           
    else :
        return HttpResponse('What is this request method? - %s' % request.method)

    if tag_code is None :
        create_log_entry('Update pool tag type', 'Updating pool of %d to %d tags to tag type %d' % (start_tag_id, end_tag_id, tag_type), tablename='Tag')
        tags = Tag.update_pool_tag_type(request, start_tag_id, end_tag_id, tag_type)

        tagid_string = ""
        for tag in tags :
            tagid_string = "%s%d = %s goes to type %d<BR>\n" % (tagid_string, tag.id, tag.to_base36(), tag.tag_type)
    else :
        create_log_entry('Update tag type by code', 'Updating tag %s to tag type %d' % (tag_code, tag_type), tablename='Tag')
        atag = Tag.update_tag_type_by_code(request, tag_code, tag_type)

        tagid_string = "%d = %s goes to type %d<BR>\n" % (atag.id, atag.to_base36(), atag.tag_type)
        
    return HttpResponse(tagid_string)


def generate_tag_images_view(request) :

    do_REST = 0
    if 'do_REST' in request.GET :
        do_REST = int(request.GET['do_REST'])

    # 17092012 introduce passphrase protection for REST urls
    # 02012013 introduce support for additional tag id field included in REST output
    passPhrase = ''
    if 'psp' in request.GET :
        passPhrase = request.GET['psp']

    include_tag_id_field = 0
    if 'include_tag_id_field' in request.GET:
        include_tag_id_field = int(request.GET['include_tag_id_field'])

    if do_REST == 1 :
        if len(passPhrase) <= 0 or passPhrase != settings.REST_PASSPHRASE :
            return HttpResponse('Incorrect passphrase!')

    if do_REST == 0 :
        if request.user.is_anonymous() or not request.user.is_authenticated() :
            return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

        if not request.user.is_staff :
            return HttpResponse('You need to be staff to use this feature <a href=\"/login\">Login to Rocket Tags</a>')
   
    return display_tag_images(request, include_tag_id_field=include_tag_id_field)

def display_images_view(request) :
    return display_images(request)

def display_image_view(request) :
    return display_image(request)

def download_image_view(request, filename='') :
    return download_image(request, filename)


# Leave here for now
def recaptcha_view(request):

    # Initialize to an empty string, not None, so the reCAPTCHA call query string
    # will be correct if there wasn't a captcha error on POST.
    captcha_error = ""
	 
    if request.method == 'POST':
	# Check the form captcha.  If not good, pass the template an error code
	#captcha_response = \
	#recaptcha.client.captcha.submit(request.POST.get("recaptcha_challenge_field", None),
	#request.POST.get("recaptcha_response_field", None),
	#"6Lez8M4SAAAAABk1RmAr_FelTUWo79D4rWrKpZq5",
	#request.META.get("REMOTE_ADDR", None))
	 
	#if not captcha_response.is_valid:
        #    captcha_error = "&error=%s" % captcha_response.error_code
 
	#else :
        return HttpResponseRedirect('/')

    return render_to_response('tags/captcha.html', {'request' : request, 'errors' : captcha_error, }, context_instance=RequestContext(request))

def update_tag_types_view(request):

    if request.user.is_anonymous() or not request.user.is_authenticated() :
        return HttpResponse('You need to be logged in to use this feature <a href=\"/login\">Login to Rocket Tags</a>')

    if not request.user.is_staff :
        return HttpResponse('You need to be staff to update a subscription <a href=\"/login\">Login to Rocket Tags</a>')               

    #name = request.POST['name']

    tags_changed = update_tag_types()

    return HttpResponse(str(tags_changed))
