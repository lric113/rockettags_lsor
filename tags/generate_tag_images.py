from create_tag import create_tag_image
import sys
from os import system
from os import environ
from django.conf import settings

def generate_image_files(request, tagid, image_type, path_to_files=None, final_files_directory=None) :
    if len(tagid) == 0 : #or not Tag.is_valid(tagid) :
        return 'Invalid tag id'
    if len(image_type) == 0 or not image_type in ['PNG', 'JPG', 'PDF', 'MOO'] :
        return 'Invalid image type'

    image_filepaths = []
    
    for colour in ['black', 'blue', 'green', 'pink', 'red', 'orange'] :

        image_filepath = generate_image_file(request, tagid, colour, image_type, path_to_files=path_to_files, final_files_directory=final_files_directory)
        image_filepaths.append(image_filepath)

    return image_filepaths
        

def generate_image_file(request, tagid, colour, image_type, path_to_files=None, final_files_directory=None) :
    if len(tagid) == 0 : #or not Tag.is_valid(tagid) :
        return 'Invalid tag id'
    if len(colour) == 0 or not colour in ['black', 'blue', 'green', 'pink', 'red', 'orange'] :
        return 'Invalid colour'
    if len(image_type) == 0 or not image_type in ['PNG', 'JPG', 'PDF', 'MOO'] :
        return 'Invalid image type'

    input_type = 'PNG'
    result_type = 'PNG'
    if image_type == 'PNG' :
        input_type = 'PNG'
        result_type = 'PNG'
    elif image_type == 'JPG' :
        input_type = 'JPG'
        result_type = 'JPG'
    elif image_type == 'PDF' :
        input_type = 'PNG'
        result_type = 'PDF'
    elif image_type == 'MOO' :
        input_type = 'MOO'
        result_type = 'JPG'

    if path_to_files is None :
        if sys.platform.startswith('linux'):
            path_to_files = '%s/html/static' % settings.WWW_ROOT
        elif sys.platform.startswith('win'):
            path_to_files = '%s/html/static' % settings.WWW_ROOT
        else :
            path_to_files = '%s/html/static' % settings.WWW_ROOT

    path_to_output_files = '%s/output/final' % path_to_files
    if final_files_directory is None :
        path_to_output_files = '%s/output/final' % path_to_files
    else :
        path_to_output_files = '%s/output/%s' % (path_to_files, final_files_directory)
        
    #system('rm %s/*.jpg' % path_to_output_files)
    #system('rm %s/*.png' % path_to_output_files)
    #system('rm %s/*.pdf' % path_to_output_files)
        
    debug_string = create_tag_image(path_to_files, tagid, colour, input_type, result_type, path_to_output_files)
    #request.META['convert_string'] = debug_string
    #assert False
    
    image_ext = 'png'
    if result_type == 'PNG' : image_ext = 'png'
    elif result_type == 'JPG' : image_ext = 'jpg'
    elif result_type == 'PDF' : image_ext = 'pdf'

    return '%s/tag_%s_%s.%s' % (path_to_output_files, tagid, colour, image_ext)

if __name__ == "__main__" :
    tagid = 'ABOUT'
    colour = 'black'
    image_type = "PNG"
    if len(sys.argv) > 1 :
        tagid = sys.argv[1]
        if len(sys.argv) > 2 :
            colour = sys.argv[2]
            if len(sys.argv) > 3 :
                image_type = sys.argv[3]

    filepath = generate_image_file(tagid, colour, image_type, final_files_directory='moo')
    print filepath
    
