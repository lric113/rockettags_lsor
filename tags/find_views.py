import sys
import os
from datetime import datetime

from django.template import Context, loader, RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import Http404
from django.contrib import auth
from django.shortcuts import render_to_response
from django.conf import settings

from forms import FoundTagForm, CaptchaForm
from rockettags.tags.models import Tag, FoundMessage
from rockettags.main.models import UserProfile
from rockettags.main.models import create_log_entry, test_if_subscribed, is_subscribed
from rockettags.fragments import query_string
from rockettags.main.screenelements import get_query_string_fields
from rockettags.main.screenelements import show_found_a_tag_text_screen
from rockettags.main.dropdowns import get_countries, get_states, get_cities

from rockettags.tags.find_ancillary import show_found_tag_screen_front, show_found_tag_screen_details, show_found_tag_screen_confirm, show_test_tag_screen_confirm, show_want_register_tag
from rockettags.tags.find_ancillary import validate_found_tag_input, get_found_tag_fields, get_found_detail_fields
from rockettags.tags.find_ancillary import get_item_types, get_contact_methods

from rockettags.main.mailers import mail_message_to_owner, mail_test_message_to_owner, mail_message_to_finder

from rockettags.links import links

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def find(request, tag_id=None) :

    (query_string_fields, next_page) = get_query_string_fields(request)
    
    if len(next_page) == 0 and request.method == 'POST' :
        if 'next' in request.POST :
            next_page = request.POST['next']
            
    next_page = '%s%s' % (next_page, query_string(request, query_string_fields, ['tag_id']))

    doTest = 'false'
    if not query_string_fields is None and 'doTest' in query_string_fields :
        doTest = query_string_fields['doTest']

    item_types = get_item_types()

    if request.method == 'POST' :
        form = FoundTagForm(None, request.POST, auto_id=True) # A form bound to the POST data
        captcha_form = CaptchaForm(None, None, auto_id=True)

        found_tag_fields = get_found_tag_fields(request)

        # check tag id
        tag_id = found_tag_fields['tag_id']
        if len(tag_id) == 0 :
            return show_found_tag_screen_front(request, form, query_string_fields, 'Empty RocketTag ID', {'form' : form, 'captcha_form' : captcha_form, 'doTest' : doTest, 'item_types' : item_types, })

        # get test flag
        test = found_tag_fields['test']

        # get show_details flag
        show_details = found_tag_fields['show_details']

        # check email
        email = found_tag_fields['email']
        if doTest == 'false' and len(email) == 0 :
            return show_found_tag_screen_front(request, form, query_string_fields, 'Empty Email Address', {'form' : form, 'captcha_form' : captcha_form, 'tag_id' : tag_id, 'doTest' : doTest,'item_types' : item_types, })

        # check item_type
        item_type = found_tag_fields['item_type']

       
        try :
            tag = Tag.objects.get(code=tag_id)
            user = tag.user
        except :
            tag = None
            user = None
            
        if tag is None :
            # Show an error page
            return show_found_tag_screen_front(request, form, query_string_fields, 'Invalid RocketTag ID', {'form' : form,'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types,})

        # 04072012 If this tag is not registered redirect into the redeem promo subsystem
        if tag.status == 0 :
            #return HttpResponseRedirect("%s?id=%s" % (links['redeem_promo'], tag_id))
            return show_want_register_tag(request, form, query_string_fields, '', {'form' : form, 'tag_id' : tag_id, })
        
        # 14052012 Before distinguishing between test a tag and found a tag
        # check that the tag owner exists and that they are currently subscribed or inside quarantine
        if user is None :
            return show_found_tag_screen_front(request, form, query_string_fields, 'This tag is registered but without an owner', {'form' : form,'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types,})

        subscribed_status = is_subscribed(user)
        if not subscribed_status :
            return show_found_tag_screen_front(request, form, query_string_fields, 'The owner of this tag is not currently subscribed', {'form' : form,'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types,})


        # Try to get user for email
        find_user = None
        try :
            find_user = auth.User.objects.get(email=email)
        except :
            find_user = None

        if test :

            # send test email to owner
            mail_test_message_to_owner(request, user, find_user, tag)

            return show_test_tag_screen_confirm(request, form, query_string_fields, '', {'form' : form, 'tag_id' : tag_id, 'email' : user.email, 'tag' : tag, 'user' : user, 'find_user' : find_user})
            

        previous_message_id = found_tag_fields['prev_message_id']
        new_message = None
        if previous_message_id is None :
            new_message = FoundMessage(tag=tag, email=email)
            new_message.item_description = item_type
            contact_method = 'email'
            new_message.contact_method = contact_method                
            new_message.status = 0            
            new_message.save()
        else :
            new_message = FoundMessage.objects.get(id=previous_message_id)

        found_detail_fields = get_found_detail_fields(request)

        if found_detail_fields or not show_details :

            if show_details :
                #if len(found_detail_fields['item_type']) == 0 :
                if len(item_type) == 0 :
                    if len(found_detail_fields['item_description']) == 0 :
                        countries = get_countries()
                        states = get_states('US')
                        cities = get_cities('US')
                        item_types = get_item_types()
                        contact_methods = get_contact_methods()

                        input_codes = {'form' : form,
                                       'tag_id' : tag_id,
                                       'email' : email,
                                       'prev_message_id' : new_message.id,
                                       'item_type' : item_type,
                                       'tag' : tag,
                                       'user' : user,
                                       'find_user' : find_user,
                                       'countries' : countries,
                                       'states' : states,
                                       'cities' : cities,
                                       'contact_methods' : contact_methods,
                                       'item_types' : item_types, }

                        return show_found_tag_screen_details(request, form, query_string_fields, 'Item type is a required field', input_codes)
                    

            #new_message = FoundMessage(tag=tag, email=email)

            contact_method = 'email'

            if show_details :
                new_message.phone = found_detail_fields['phone_number']
                new_message.firstname = found_detail_fields['firstname']
                new_message.lastname = found_detail_fields['lastname']
                new_message.address = found_detail_fields['address']
                new_message.city = found_detail_fields['city']
                new_message.state = found_detail_fields['state']
                new_message.country = found_detail_fields['country']
                contact_method = found_detail_fields['contact_method']
                if len(contact_method) == 0 : contact_method = 'email'

                if len(item_type) > 0 :
                    description = item_type
                    if len(found_detail_fields['item_description']) > 0 :
                        description = "%s - %s" % (description, found_detail_fields['item_description'])                
                    new_message.item_description = description
                    
                else :
                    new_message.item_description = found_detail_fields['item_description']
                       
                new_message.item_location = found_detail_fields['item_location']
            else :
                new_message.item_description = item_type
                contact_method = 'email'

            new_message.contact_method = contact_method
                
            new_message.status = 1
            
            new_message.save()

            if show_details :
                create_log_entry('Create Found Messsage', 'Item %s has been found by %s.' % (new_message.item_description, new_message.email), tablename='FoundMessage')
            else :
                create_log_entry('Create Found Messsage', 'An item with tag code %s has been found by %s.' % (new_message.tag.to_base36(), new_message.email), tablename='FoundMessage')
                
            # send emails to owner and to the finder               
            if contact_method == 'email' :
                mail_message_to_owner(request, user, find_user, new_message)

            # 15062012 If the finder has given an email send a thank you email to them
            if len(email) > 0 :
                mail_message_to_finder(request, email, find_user, new_message)

            return show_found_tag_screen_confirm(request, form, query_string_fields, '', {'form' : form, 'tag_id' : tag_id, 'email' : email, 'tag' : tag, 'user' : user, 'find_user' : find_user})

        else :

            countries = get_countries()
            states = get_states('US')
            cities = get_cities('US')
            item_types = get_item_types()
            contact_methods = get_contact_methods()

            input_codes = {'form' : form,
                           'tag_id' : tag_id,
                           'email' : email,
                           'prev_message_id' : new_message.id,
                           'item_type' : item_type,
                           'tag' : tag,
                           'user' : user,
                           'find_user' : find_user,
                           'countries' : countries,
                           'states' : states,
                           'cities' : cities,
                           'contact_methods' : contact_methods,
                           'item_types' : item_types, }

            return show_found_tag_screen_details(request, form, query_string_fields, '', input_codes)
       
    else :
        form = FoundTagForm(None, None, auto_id=True) # A form not bound
        captcha_form = CaptchaForm(None, None, auto_id=True)

        if tag_id == None or len(tag_id) == 0 :
            if not query_string_fields is None and 'tag_id' in query_string_fields :
                tag_id = query_string_fields['tag_id']
    
        if tag_id is None or len(tag_id) == 0 :
            return show_found_tag_screen_front(request, form, query_string_fields, '', {'form' : form, 'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types,})

        try :
            tag = Tag.objects.get(code=tag_id)
            user = tag.user
        except :
            tag = None
            user = None
            
        if tag is None :
            # Show an error page
            return show_found_tag_screen_front(request, form, query_string_fields, 'Invalid RocketTag ID', {'form' : form, 'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types,})            

        return show_found_tag_screen_front(request, form, query_string_fields, '', {'form' : form, 'tag_id' : tag_id, 'tag' : tag, 'user' : user, 'captcha_form' : captcha_form, 'doTest' : doTest,'item_types' : item_types, })
        
@csrf_exempt
def captcha_handler(request):

    # Initialize to an empty string, not None, so the reCAPTCHA call query string
    # will be correct if there wasn't a captcha error on POST.
    captcha_error = ""
    #return HttpResponseRedirect("/")

    #method = request.POST['method']
    #return HttpResponse(method)
	 
    if request.POST:

        #if "method" in request.POST :
        #return HttpResponse('{"method" : "%s"}' % request.POST["method"])

        form = CaptchaForm(request.POST)

        ##name = request.POST['name']
        
        # Validate the form: the captcha field will automatically 
        # check the input
        human = False
        if form.is_valid():
            human = True

        if human :
                return HttpResponse('YES')
        else :
                return HttpResponse('NO')
                
            
    else:

        #name = request.GET['name']
        get = request.GET

        if 'captcha_1' in request.GET :
            form = CaptchaForm(request.GET)

            human = False
            if form.is_valid() :
                human = True

            if human :
                return HttpResponse('YES')
            
        else :
            form = CaptchaForm()
    return render_to_response('tags/captcha2.html',locals(), context_instance=RequestContext(request))

@csrf_exempt
def show_found_a_tag_text(request) :

    (query_string_fields, next_page) = get_query_string_fields(request)
    
    test = False
    if "test" in query_string_fields :
        test = query_string_fields['test']
        if test == "false" or test == "False" :
            test = False
        elif test == "true" or test == "True" :
            test = True
        else :
            test = False

    return show_found_a_tag_text_screen(request, test)

