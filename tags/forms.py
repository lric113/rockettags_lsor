from django.utils.translation import ugettext_lazy as _
from django import forms
from captcha.fields import CaptchaField

class FoundTagForm(forms.Form) :
    tag_id = forms.CharField(max_length=8, required=False)    
    
    # At some point do custom validation
    def is_valid(self) :
        return True

class CaptchaForm(forms.Form) :
    captcha = CaptchaField()

