import pyqrcode
from PIL import Image
import sys
from os import system


colours = {}
colours['black'] = 'rgb(0,0,0)'
colours['green'] = 'rgb(24,196,200)'
colours['blue'] = 'rgb(91,130,251)'
colours['pink'] = 'rgb(255,0,162)'
colours['orange'] = 'rgb(246,115,43)'
colours['purple'] = 'rgb(134,94,154)'

def create_tag_image(path_to_files, tagid, colour, image_type, result_type) :

    BASE_URL = "http://rockettags.com/t/"
    URL = '%s%s' % (BASE_URL, tagid)

    BASE_TEMPLATE_NAME = 'tag_template'
    TEMPLATE_NAME = '%s_%s.png' % (BASE_TEMPLATE_NAME, colour)
    if image_type == 'JPG' :
        TEMPLATE_NAME = '%s_%s.jpg' % (BASE_TEMPLATE_NAME, colour)    
    else :
        TEMPLATE_NAME = '%s_%s.png' % (BASE_TEMPLATE_NAME, colour)
        
    template_image = Image.open('%s/templates/%s' % (path_to_files, TEMPLATE_NAME))
    template_image_copy = template_image.copy()

    qr_image = pyqrcode.MakeQRImage(URL, rounding = 2, fg = colours[colour], block_in_pixels = 5, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)

    paste_position = (47, 53)
    if image_type == 'JPG' :
        paste_position = (38, 44)
    else :
        paste_position = (47, 53)
        
    template_image_copy.paste(qr_image, paste_position)

    OUTPUT_NAME = 'tag_%s_int.png' % tagid
    if image_type == 'JPG' :
        OUTPUT_NAME = 'tag_%s_int.jpg' % tagid
    else :
        OUTPUT_NAME = 'tag_%s_int.png' % tagid
        
    template_image_copy.save('%s/output/%s' % (path_to_files, OUTPUT_NAME), quality=95)

    EXTENSION = 'png'
    if image_type == 'JPG' :
        EXTENSION = 'jpg'
    else :
        EXTENSION = 'png'

    START_TEXT = (80,31)
    if image_type == 'JPG' :
    #    START_TEXT = (72,23)
        START_TEXT = (82,23)
    else :
    #    START_TEXT = (80,31)
        START_TEXT = (90,31)    

    RESULT_EXTENSION = EXTENSION
    if result_type == 'JPG' :
        RESULT_EXTENSION = 'jpg'
    elif result_type == 'PDF' :
        RESULT_EXTENSION = 'pdf'
    else :
        RESULT_EXTENSION = EXTENSION
        
    convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 16 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_int.%s %s/output/tag_%s.%s' % (path_to_files, colours[colour], colours[colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, EXTENSION, path_to_files, tagid, RESULT_EXTENSION)

    system(convert_cmd)

    return convert_cmd


if __name__ == "__main__" :
    tagid = 'ABOUT'
    colour = 'black'
    image_type = "PNG"
    result_type = "PNG"
    if len(sys.argv) > 1 :
        tagid = sys.argv[1]
        if len(sys.argv) > 2 :
            colour = sys.argv[2]
            if len(sys.argv) > 3 :
                image_type = sys.argv[3]
                if len(sys.argv) > 4 :
                    result_type = sys.argv[4]

    create_tag_image('/nginx/html/static', tagid, colour, image_type, result_type)
