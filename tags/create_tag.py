import pyqrcode
from PIL import Image
import sys
from os import system
import sys

# ***************************************************************************************
#  24052012 This version of create_tag is now only for downloading a digital image !!!!
# ***************************************************************************************

#colours = {}
#colours['black'] = 'rgb(0,0,0)'
#colours['green'] = 'rgb(24,196,200)'
#colours['blue'] = 'rgb(91,130,251)'
#colours['pink'] = 'rgb(255,0,162)'
#colours['orange'] = 'rgb(246,115,43)'
#colours['purple'] = 'rgb(134,94,154)'

colours = {}
colours['black'] = 'rgb(0,0,0)'
colours['green'] = 'rgb(102,255,0)'
colours['blue'] = 'rgb(0,153,255)'
colours['pink'] = 'rgb(255,0,204)'
#colours['orange'] = 'rgb(255,102,51)'
colours['orange'] = 'rgb(255,102,24)'
colours['red'] = 'rgb(255,0,0)'

colours['softblack'] = 'rgb(153,153,153)'


def create_tag_image(path_to_files, tagid, colour, image_type, result_type, path_to_output=None) :

    BASE_URL = "http://rockettags.com/t/"
    URL = '%s%s' % (BASE_URL, tagid)
    BASE_TEMPLATE_NAME = 'tag_template'

    if path_to_output is None :
        path_to_output = '%s/output' % path_to_files
    
    #  24052012 if image_type == 'MOO' :
    BASE_TEMPLATE_NAME = 'moo_template'
    #else :
    #    BASE_TEMPLATE_NAME = 'tag_template'        
    
    TEMPLATE_NAME = '%s_%s.png' % (BASE_TEMPLATE_NAME, colour)
    if image_type == 'MOO' :
        TEMPLATE_NAME = '%s_%s.jpg' % (BASE_TEMPLATE_NAME, colour)    
    elif image_type == 'JPG' :
        TEMPLATE_NAME = '%s_%s.jpg' % (BASE_TEMPLATE_NAME, colour)    
    else :
        TEMPLATE_NAME = '%s_%s.png' % (BASE_TEMPLATE_NAME, colour)
        
    template_image = Image.open('%s/templates/%s' % (path_to_files, TEMPLATE_NAME))
    template_image_copy = template_image.copy()

    if image_type == 'MOO' :
        # 28042012 qr_image = pyqrcode.MakeQRImage(URL, rounding = 6, fg = colours[colour], block_in_pixels = 15, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
        #qr_image = pyqrcode.MakeQRImage(URL, rounding = 3, fg = colours[colour], block_in_pixels = 4, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
        qr_image = pyqrcode.MakeQRImage(URL, rounding = 6, fg = colours[colour], block_in_pixels = 15, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
    elif image_type == 'JPG' :
        qr_image = pyqrcode.MakeQRImage(URL, rounding = 6, fg = colours[colour], block_in_pixels = 15, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
    else :
# 24052012        qr_image = pyqrcode.MakeQRImage(URL, rounding = 2, fg = colours[colour], block_in_pixels = 5, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
        qr_image = pyqrcode.MakeQRImage(URL, rounding = 6, fg = colours[colour], block_in_pixels = 15, border_in_blocks = 0, bg = "white", br = False, extra_reduction = 0)
        
    paste_position = (47, 53)
    if image_type == 'MOO' :
        # 28042012 paste_position = (163, 161)
        paste_position = (163, 161)
    elif image_type == 'JPG' :
        #paste_position = (38, 44)
        # 24052012 paste_position = (37, 46)
        paste_position = (163, 161)
    else :
        #paste_position = (47, 53)
        # 24052012 paste_position = (36, 46)
        paste_position = (163, 161)
        
    template_image_copy.paste(qr_image, paste_position)

    OUTPUT_NAME = 'tag_%s_%s_int.png' % (tagid, colour)
    if image_type == 'MOO' :
        OUTPUT_NAME = 'tag_%s_%s_int.jpg' % (tagid, colour)
    elif image_type == 'JPG' :
        OUTPUT_NAME = 'tag_%s_%s_int.jpg' % (tagid, colour)
    else :
        OUTPUT_NAME = 'tag_%s_%s_int.png' % (tagid, colour)
        
    template_image_copy.save('%s/output/%s' % (path_to_files, OUTPUT_NAME), quality=95)

    EXTENSION = 'png'
    if image_type == 'MOO' :
        EXTENSION = 'jpg'
    elif image_type == 'JPG' :
        EXTENSION = 'jpg'
    else :
        EXTENSION = 'png'

    START_TEXT = (80,31)
    text_colour = colour
    if image_type == 'MOO' :
        #START_TEXT = (330, 108)
        # 19042012_135000 START_TEXT = (334, 108)
        # 28042012_122800 START_TEXT = (270, 108) (295,108)
        # 18052012_104800 START_TEXT = (290, 108) (315, 108)
        if len(tagid) >= 7 :
            START_TEXT = (275, 108)
        else :
            START_TEXT = (290, 108)
        #text_colour = 'softblack'
    elif image_type == 'JPG' :
    #    START_TEXT = (72,23)
#     24052012    START_TEXT = (82,23)
        if len(tagid) >= 7 :
            START_TEXT = (275, 108)
        else :
            START_TEXT = (290, 108)
    else :
    #    START_TEXT = (80,31)
    #    START_TEXT = (90,31)
#  24052012       START_TEXT = (82,23)    
        if len(tagid) >= 7 :
            START_TEXT = (275, 108)
        else :
            START_TEXT = (290, 108)

    RESULT_EXTENSION = EXTENSION
    if result_type == 'MOO' :
        RESULT_EXTENSION = 'jpg'
    elif result_type == 'JPG' :
        RESULT_EXTENSION = 'jpg'
    elif result_type == 'PDF' :
        RESULT_EXTENSION = 'pdf'
    else :
        RESULT_EXTENSION = EXTENSION

    rotate_back_cmd = None
    convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 28 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
    if image_type == 'MOO' :
        # 19042012_135000 font size was 28 - Font was Nunito-Light
        # 28042012_121500 font size was 50 and font was Nunito-Regular
        # 18052012_103600 font size was 44 and font was vaground        
        if sys.platform.startswith('linux'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill \'%s\' -stroke \'%s\' -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        elif sys.platform.startswith('win'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        rotate_back_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 270 %s/tag_%s_%s.%s %s/tag_%s_%s.%s' % (path_to_files, path_to_output, tagid, colour, RESULT_EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)

    elif image_type == 'JPG' :
#  24052012       if sys.platform.startswith('linux'):
#            convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 16 -fill \'%s\' -stroke \'%s\' -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[colour], colours[colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
#        elif sys.platform.startswith('win'):
#            convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 16 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[colour], colours[colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        if sys.platform.startswith('linux'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill \'%s\' -stroke \'%s\' -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        elif sys.platform.startswith('win'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        rotate_back_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 270 %s/tag_%s_%s.%s %s/tag_%s_%s.%s' % (path_to_files, path_to_output, tagid, colour, RESULT_EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)

    else :
#   24052012      if sys.platform.startswith('linux'):
#            convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 16 -fill \'%s\' -stroke \'%s\' -draw \"text %s \'%s\'\" %s/output/tag_%s_int.%s %s/tag_%s.%s' % (path_to_files, colours[colour], colours[colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
#        elif sys.platform.startswith('win'):
#            convert_cmd = 'convert -font %s/fonts/Nunito-Light.ttf -pointsize 16 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[colour], colours[colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)

        if sys.platform.startswith('linux'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill \'%s\' -stroke \'%s\' -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        elif sys.platform.startswith('win'):
            convert_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 90 -pointsize 56 -fill %s -stroke %s -draw \"text %s \'%s\'\" %s/output/tag_%s_%s_int.%s %s/tag_%s_%s.%s' % (path_to_files, colours[text_colour], colours[text_colour], str(START_TEXT)[1:-1], tagid, path_to_files, tagid, colour, EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
        rotate_back_cmd = 'convert -font %s/fonts/vaground.ttf -rotate 270 %s/tag_%s_%s.%s %s/tag_%s_%s.%s' % (path_to_files, path_to_output, tagid, colour, RESULT_EXTENSION, path_to_output, tagid, colour, RESULT_EXTENSION)
    
    system(convert_cmd)
    #  24052012 if image_type == 'MOO' and not rotate_back_cmd is None :
    if not rotate_back_cmd is None :
        system(rotate_back_cmd)
    return convert_cmd

if __name__ == "__main__" :
    tagid = 'ABOUT'
    colour = 'black'
    image_type = "PNG"
    result_type = "PNG"
    if len(sys.argv) > 1 :
        tagid = sys.argv[1]
        if len(sys.argv) > 2 :
            colour = sys.argv[2]
            if len(sys.argv) > 3 :
                image_type = sys.argv[3]
                if len(sys.argv) > 4 :
                    result_type = sys.argv[4]

    create_tag_image('.', tagid, colour, image_type, result_type)
    
